var _init = $.ui.dialog.prototype._init;

$.ui.dialog.prototype._init = 
function() 
{
	_init.apply(this, arguments);
	
	var dialog_element = this;
	var dialog_id = this.uiDialogTitlebar.next().attr('id');
			
	this.uiDialogTitlebar.append('<a href="#" id="' + dialog_id + '-minbutton" class="ui-dialog-titlebar-minimize ui-corner-all">'
									+ '<span class="ui-icon ui-icon-minusthick"></span></a>');
			
	$('#dialog_window_minimized_container').append( '<div class="dialog_window_minimized ui-widget ui-state-default ui-corner-all" id="'
													+ dialog_id + '_minimized">' + this.uiDialogTitlebar.find('.ui-dialog-title').text()
													+ '<span class="ui-icon ui-icon-newwin"></div>');
				
	$('#' + dialog_id + '-minbutton').hover
	(
		function() 
		{
			$(this).addClass('ui-state-hover');
		}, 
		
		function() 
		{
			$(this).removeClass('ui-state-hover');
		}
	).click
	(
		function() 
		{
			dialog_element.close();
			$('#' + dialog_id + '_minimized').show();
		}
	);
			
	$('#' + dialog_id + '_minimized').click(function() {
		$(this).hide();
		dialog_element.open();
	});
};
	
$(document).ready(function() 
{		
	$('#fecha').datepicker();
	$('#acction').button().click(
		function() 
		{
			alert("Boton ACCTION fue pulsado");
		}
	);		

	$('#create_button').button().click(function() 
	{
		var create_dialog = $('#dialog_window_1');
		var create_button = $(this);
		if( create_dialog.dialog('isOpen') ) 
		{
			create_button.button('option', 'label', 'Create a new Window');
			create_dialog.dialog('close');
		} else 
		{
			create_button.button('option', 'label', 'Close Window');
			create_dialog.dialog('open');
		}
	});

	
	$('#dialog_window_1').dialog(
	{
		width: 'auto',
		height: 'auto',
		autoOpen : false,
		buttons: [
			{
				text: 'Create',
				click: function() 
				{
					var div_count = $('.dialog_window').length + 1;
					var div_id = 'dialog_window_' + div_count;
					var div_title = $('#new_window_title').val();
					var div_content = $('#new_window_content').val();
					var buttons = new Array();
					if( $('#alertbutton').is(':checked') ) 
					{
						buttons.push(
						{
							text: 'ALERT',
							click: function() 
							{
								alert('ALERTING from Dialog Widnow: ' + div_title);
							}
						});
					}
							
					if( $('#closebutton').is(':checked') ) 
					{
						buttons.push(
						{
							text: 'CLOSE',
							click: function() 
							{
								$('#' + div_id).dialog('close');
							}
						});
					}
					
					$('body').append('<div class="dialog_window" id="' + div_id + '">' + div_content + '</div>');
		
					var dialog = $('#' + div_id).dialog(
					{
						width: 'auto',
						height: 'auto',
						title : div_title,
						autoOpen : true,
						buttons: buttons
					});
				}
			}
		]
	});	
	$('#buttonlist').buttonset();
				
});
