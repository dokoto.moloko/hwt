var oTable;
$(document).ready( function () {
	
	$("#grid tbody").delegate("tr", "click", function(e) 
	{
        if ( $(this).hasClass('row_selected') ) {
            $(this).removeClass('row_selected');
        }
        else {
            oTable.$('tr.row_selected').removeClass('row_selected');
            $(this).addClass('row_selected');
        }
	});

	oTable = $('#grid').dataTable( );
	
} );	
