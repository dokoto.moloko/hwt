
// TODO ESTO ES PARTE DE LA FUNCIONALIDAD DE MAXIMIZACION-MINIMIZACION DE DOCUMENTOS
// I -----------------------------------------------------------------------------------
var mini = false;
var map;
var _init = $.ui.dialog.prototype._init;
$.ui.dialog.prototype._init = function() 
{
	//Run the original initialization code
   	_init.apply(this, arguments);
	
   	//set some variables for use later
   	var dialog_element = this;
   	var dialog_id = this.uiDialogTitlebar.next().attr('id');
	
   	//append our minimize icon
   	this.uiDialogTitlebar.append('<a href="#" id="' + dialog_id +
   	'-minbutton" class="ui-dialog-titlebar-minimize ui-corner-all">'+
   	'<span class="ui-icon ui-icon-minusthick"></span></a>');
	
   	//append our minimized state
   	$('#dialog_window_minimized_container').append(
      	'<div class="dialog_window_minimized ui-widget ui-state-default ui-corner-all" id="' +
      	dialog_id + '_minimized">' + this.uiDialogTitlebar.find('.ui-dialog-title').text() +
      	'<span class="ui-icon ui-icon-newwin"></div>');
	
   	//create a hover event for the minimize button so that it looks good
   	$('#' + dialog_id + '-minbutton').hover(function() {
      	$(this).addClass('ui-state-hover');
   	}, function() {
      	$(this).removeClass('ui-state-hover');
   	}).click(function() {
		
	mini = true;
      	//add a click event as well to do our "minimalization" of the window
      	dialog_element.close();
      	$('#' + dialog_id + '_minimized').show();
   	});
	
   	//create another click event that maximizes our minimized window
   	$('#' + dialog_id + '_minimized').click(function() {
      	$(this).hide();
      	dialog_element.open();
   	});
};
// F -----------------------------------------------------------------------------------

function initDoc(doc)
{
	// Aqui podemos hacer la peticion ajax al servicio del contenido del doc
	// cargarlo y depues visulizarlo
	$(doc).dialog
	(
		{
			width: 'auto',
   			height: 'auto'
		}
	);

}

// INICIALIZACION
var oTable;
$(document).ready
(		
	function() 
	{
		$("#grid_2 tbody").delegate("tr", "click", function(e) 
		{
			if ( $(this).hasClass('row_selected') ) {
				$(this).removeClass('row_selected');
			}
			else {
				oTable.$('tr.row_selected').removeClass('row_selected');
				$(this).addClass('row_selected');
			}
		});	
		
		// Grid
		oTable =  $('#grid_2').dataTable( {		
			"bServerSide": false,	
			"iDisplayLength": 10,
			"sAjaxSource": "file:///D:/DATOS/PERSONALES/DESARROLLOS/C++/HEOWT/tests/HTML/json/grid_2.txt",
			"fnServerData": function ( sSource, aoData, fnCallback ) 
			{
				$.getJSON( sSource, aoData, function (json) 
				{ 
					// Do whatever additional processing you want on the callback, then tell DataTables 
					fnCallback(json)
				} );
			}
		} );	

		$('#select').button().click
		(
			function() 
			{
				var anSelected = oTable.$('tr.row_selected');
				var oSettings = oTable.fnSettings();
				alert("Fila en Pagina: " + anSelected[0].rowIndex + " Numero de Paginas mostradas : " + oSettings._iDisplayStart + " Fila en Global : " + (oSettings._iDisplayStart+anSelected[0].rowIndex));
			}
		);		
		
		// Se ocultan las pantallas o documentos que no son el principal
		$('#document_id_1').hide();
		$('#document_id_3').hide();
        $('#document_id_4').hide();
		$('#dialog-confirm').hide();

		// Se inicializan los Widgets
		$('#buttonlist').buttonset();
		$('#buttonlist2').buttonset();
		$('#fecha').datepicker();
		$('#fecha2').datepicker();
		$('#fecha').width(20);
		$('#mitextarea').attr('maxlength', 10);
		
		
		// Se inicializan sus valores por defecto
		$('#new_window_title').val('Default value for input');
		$('#fecha').val('');
		
		 $('#document_id_1').bind('dialogclose',
		function(event)
		{				
			if (mini == true)
			{
				alert('Mimize');
			}
			else if(mini == false)
			{
				$( "#dialog-confirm" ).dialog({
				resizable: false,
				width: 'auto',
				height: 'auto',
				modal: true,
				buttons: {
				    "Continue": function() {
					$( this ).dialog( "close" );
					$.post
					(
						"acction.do", { uuid: "1376764", event: "close", turnON: "false", endTransmision: "true" },
						function(data)
						{
							alert("Applications closed.");
						}
					);
				    },
				    Cancel: function() {
					$( this ).dialog( "close" );
				    }
				}
				});                			
			}
			mini = false;			
		});
		
		// Se fija la funcionalidad de los eventos o CallBacks
		$('#acction').button().click
		(
			function() 
			{
				//var v = $("#speedA").children(":first").attr('selected', 'selected');
				//alert($('#alertbutton').attr('checked') ? "on" : "off");
                alert ($('#mitextarea').val());
				$.ajax
				({
					type: "POST",
					url: "acction.do", 
					async: false,
					data: 
						{
							uuid: "1376764", 
							event: "CLICK", 
							
							// Aqui todos los valores de los imput segun el Scope		
							uuid: "1233423",
							value: $('#1233423').val(),
							
							
							endTransmision: "true" 
						}
				}).done
				(
				function(data)
					{
						/*
						JAVASCRIPT PART 
						http://stackoverflow.com/questions/5712282/jquery-and-dynamic-javascript-execute
						http://api.jquery.com/jQuery.getScript/
						http://stackoverflow.com/questions/7017241/append-and-call-dynamic-javascript-function-with-jquery
						
						CALL BACK REQUEST TO SERVER ==> DYNAMIC.JS
						$.getScript("dynamic.js", function(){ exec_post_actions(); });
						<<== SERVER RESPOND
							function exec_post_actions()
							{
								// Ocultar todas las pantallas menos la que tenga que ir al top
								// Eliminar el contenido de la WINDOW o WINDOWS dependiendo del scope
								$('#6523542560').empty();
								$('#6523542561').empty();
								// Insertar el contenido de la WINDOWS o WINDOWS dependiendo del scope
								$('#6523542560').append('<p>Test</p>');
								$('#6523542561').append('<p>Test</p>');
								// Carga los valores de los campos
								$('#6523542562').val('Default value for input');
								$('#6523542563').val('02/03/2012');
								// Mandar al top la pantalla idicada
								$("#6523542560").dialog("moveToTop");
							}
						<<== SERVER RESPOND
						
						*/ 
					}
				);
			}
		);
		
        $('#inp1').keypress(function()
        {
            map['imp1'] = $('#imp1').val();
        });
        
		$('#acction2').button().click
		(
			function() 
			{
				$("#document_id_1").dialog("moveToTop")
				/*
				METODOS BASICOS CON DIALOG :
					"close"     : Close or hide the dialog.
					"destroy"   : Permanently disable the dialog. The destroy method for a dialog works in a slightly 
								  different way than it does for the other widgets we've seen so far. Instead of just 
								  returning the underlying HTML to its original state, the dialog's destroy method also hides it.
					"disable"   : Temporarily disable the dialog.
					"enable"    : Enable the dialog if it has been disabled.
					"isOpen"    : Determine whether a dialog is open or not.
					"moveToTop" : Move the specified dialog to the top of the stack.
					"open"      : Open the dialog.
					"option"    : Get or set any configurable option after the dialog has been initialized.
				*/
			}
		);
		

		// Se carga la pantalla principal
		$('#document_id_2').dialog
		(
			{
				width: 'auto',
   				height: 'auto'
			}
		);

	}
);
