#ifndef _TYPES_HPP
#define _TYPES_HPP


#ifdef _MSC_VER
	typedef __int16 int16_t;
	typedef unsigned __int16 uint16_t;
	typedef __int32 int32_t;
	typedef unsigned __int32 uint32_t;
	typedef __int64 int64_t;
	typedef unsigned __int64 uint64_t;	
	#define _CRT_SECURE_NO_WARNINGS
	//#include <windows.h>	
	#include "atlbase.h"
	#include "atlstr.h"
	#include "comutil.h"
	#include "Strsafe.h"
        #include <direct.h>
        #define GetCurrentDir _getcwd	
#else
	#include <stdint.h>
        #include <dirent.h>
        #include <sys/stat.h>
        #include <sys/types.h>
        #include <unistd.h>
        #define GetCurrentDir getcwd
#endif
#include <cstdio>
#include <cstring>

#include <string>
#include <map>
#include <vector>
#include <sstream>
#include <iostream>
#include "emsg.hpp"

namespace hwt
{
	/*-------------------------------------------------------------------------------------------------
	 * DEFINES
	 *------------------------------------------------------------------------------------------------*/

	#define LONG(value) static_cast<long>(value)
	#define FLOAT(value) static_cast<float>(value)
	// CONST CHAR* TO VOID*
	#define CCP_TO_VP(var) static_cast<void*>(const_cast<char*>(var))
	// VOID* TO CONST CHAR*
	#define VP_TO_CCP(var) static_cast<char*>(var)
        #define VECTOR(TYPE) std::vector<TYPE>

	/*-------------------------------------------------------------------------------------------------
	 * TYPES
	 *------------------------------------------------------------------------------------------------*/

	typedef uint32_t uInt_t;
	typedef std::string Str_t;
        typedef char* CStr_t;
	typedef char Char_t;
	typedef std::stringstream Buffer_t;
	
	typedef struct
	{
		Str_t           value;
		int		type;
    
	} value_t;
	typedef struct											
	{
		Str_t		name;
                Str_t		ruta;
		Str_t		tipo;
	} file_t;
	typedef std::vector<file_t>     Vfile_t;        

	typedef std::map<Str_t, value_t> params_t;
	class session;
	typedef void (*ptrParams_t)(const params_t&, session& ss);

	/*-------------------------------------------------------------------------------------------------
	 * CONST VARS
	 *------------------------------------------------------------------------------------------------*/

	const char types[][10] = {"Int", "Long", "Double", "Float", "Char", "String"};
	enum types_t			 {Int, Long, Double, Float, Char, String};
	const char events[][20] = {"onClick", "onMouseOver"};
	enum events_t			 {onClick, onMouseOver};     
}

#endif // _TYPES_HPP

