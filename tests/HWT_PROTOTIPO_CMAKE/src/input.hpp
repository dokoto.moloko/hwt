#ifndef _INPUT_HPP
#define _INPUT_HPP

#include "memReport.hpp"
#include "types.hpp"
#include "widget.hpp"

namespace hwt
{
	class input : public widget
	{
		private:
			input(const input& i )  :	widget(	i.Labeltx, i.Width, i.Height ) {}
			input& operator=(const input&) { return *this; }
		
		public:
			input(	Str_t labeltx = "", uInt_t width = 200, uInt_t height = 100  );
			~input();
	};
}

#endif //_INPUT_HPP

