#ifndef _BUTTON_HPP
#define _BUTTON_HPP

#include "memReport.hpp"
#include "types.hpp"
#include "widget.hpp"

namespace hwt
{
	class button : public widget
	{
		private:
			button(const button& b )  :	widget(	b.Labeltx, b.Width, b.Height  ) {}
			button& operator=(const button&) { return *this; }
	
		public:
			button(	Str_t labeltx = "", uInt_t width = 200, uInt_t height = 100  );


			~button();
	};
}

#endif //_BUTTON_HPP

