#ifndef _DEPLOYAPP_HPP
#define _DEPLOYAPP_HPP

#include "memReport.hpp"
#include "types.hpp"
#include "httpServer.hpp"
#include "session.hpp"


namespace hwt
{
	class deploy
	{
                private:
                    typedef struct MHD_Daemon	Daemon_t;
                    typedef struct
                    {	
			int					connectiontype;
			Str_t					answerstring;				
			struct MHD_PostProcessor*		postprocessor;
			std::map<Str_t, Str_t>                  post_key_value;                        
                    }				Connection_info_struct_t;
                    
		private:
			deploy(const deploy& d) : Session(d.Session) {}
			deploy& operator=(const deploy&) { return *this; }
                
                private:
                    static const uInt_t        POST_BUFFER_SIZE = 512;
                    static const Char_t*       txPOST;
                    static const Char_t*       txGET;                    
                    enum {GET, POST};
                    
		private:
			session&        Session;
                        Daemon_t*       Daemon;

		private:
                        void run                        ();
                        int  on_client_connect          (void* cls, const sockaddr* addr, socklen_t addrlen);
                        int  answer_to_connection       (void* cls, struct MHD_Connection* connection, const char* url, 
                                                         const char* method, const char* version, const char* upload_data, 
                                                         size_t* upload_data_size, void** con_cls);  
                        void request_completed          (void* , struct MHD_Connection* , void** con_cls, enum MHD_RequestTerminationCode );                         
                        int  iterate_post		(void *coninfo_cls, enum MHD_ValueKind kind, const char *key,
							 const char *filename, const char *content_type, const char *transfer_encoding,
							 const char *data, uint64_t off, size_t size);
                        void page_dispatcher            (const Str_t& page_request, Str_t& page_return, Str_t& mimetype);
                        int  send_page			(struct MHD_Connection *connection, const Str_t& page, const Str_t& mimetype);
                        

		public:
			deploy(session& ss);
			~deploy();
			void Release();
	};

}


#endif //_DEPLOYAPP_HPP

