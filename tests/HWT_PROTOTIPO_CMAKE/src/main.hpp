#ifndef _MAIN_HPP
#define _MAIN_HPP

#include "memReport.hpp"
#include <iostream>
#include "hwt.hpp"


void handleClick(const hwt::params_t& params, hwt::session& Session);
void listParams(const hwt::params_t params);


#endif // _MAIN_HPP

