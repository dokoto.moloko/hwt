#include "utils.hpp"

namespace hwt
{
namespace utl
{	
	value_t ConvToStr(int value)
	{
		value_t ret = { ConvToStr_T<int>(value), Int };  
		return ret; 
	}

	value_t ConvToStr(long value)
	{ 
		value_t ret = { ConvToStr_T<long>(value), Long };  
		return ret; 
	}

	value_t ConvToStr(double value)
	{ 
		value_t ret = { ConvToStr_T<double>(value), Double };  
		return ret; 
	}

	value_t ConvToStr(float value)
	{ 
		value_t ret = { ConvToStr_T<float>(value), Float };  
		return ret; 
	}

	value_t ConvToStr(char value)
	{ 
		value_t ret = { ConvToStr_T<char>(value), Char };  
		return ret; 
	}

	value_t ConvToStr(Str_t value)
	{ 
		value_t ret = { ConvToStr_T<Str_t>(value), String };  
		return ret; 
	}	
}
}
