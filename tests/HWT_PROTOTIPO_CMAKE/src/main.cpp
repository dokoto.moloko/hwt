#include "main.hpp"

/*
	QUEDA	
	- Definir ParseGrid
	- Crear la clase webServer
*/

int main(void)
{
	try
	{
		hwt::session App;	
		App.AddDocument(800, 600);	                                
		
		App.Document(0).Header.Title = "Aplicacion de Test : HTML WIDGET TOOLKIT FOR C++ v0.1";

        App.Document(0).Body.AddLine();
        App.Document(0).Body.ModLine(0).AddLabel("Nombre", 200);		
		App.Document(0).Body.ModLine(0).AddInput("input01", 500);
		App.Document(0).Body.ModLine(0).AddButton("botton02");
        
        App.Document(0).Body.AddLine();
        App.Document(0).Body.ModLine(1).AddLabel("Nombre_2", 200);		
		App.Document(0).Body.ModLine(1).AddInput("input01_2", 500);
		App.Document(0).Body.ModLine(1).AddButton("botton02_2");

        App.Document(0).Body.AddLine();
        App.Document(0).Body.ModLine(2).AddLabel("Nombre_3", 200);		
		App.Document(0).Body.ModLine(2).AddInput("input01_3", 500);
		App.Document(0).Body.ModLine(2).AddButton("botton02_3");      
        
        App.Document (0).Body.DelLine (1);
		
		App.Document(0).Body.ModLine(0).ModButton(0).AddEvent(&handleClick, hwt::events[hwt::onClick]);		
		App.Document(0).Body.ModLine(4).ModButton(0).AddEvent(&handleClick, hwt::events[hwt::onClick]);

		//App.Deploy();
        
        
        ReportMemStatus();
	}
	catch (std::exception& e)
	{
		std::cout << e.what() << std::endl;
        ReportMemStatus();
	}
	
	std::cin.get();
}

void listParams(const hwt::params_t params)
{
	for(hwt::params_t::const_iterator cit = params.begin(); cit != params.end(); cit++)
		std::cout << "[" << &cit << "] PARAMETER : " << cit->first << " TYPE : " << hwt::types[cit->second.type]
		<< " VALUE : " << cit->second.value << std::endl;
}

void handleClick(const hwt::params_t& params, hwt::session& Session)
{
	Session.Document(0).Header.Title = "Se cambia el Titulo para regenerar el documento 0.";
	listParams(params);	
}

/*
	hwt::params_t params;	
	params["param1"] = hwt::Load(10);
	params["param2"] = hwt::Load("Paco");	
	params["param3"] = hwt::Load(LONG(5123456));
	params["param4"] = hwt::Load('a');
	params["param5"] = hwt::Load(2.45);
	params["param6"] = hwt::Load(FLOAT(2.3456745));
	hwt::Click(&handleClick, params);
*/






