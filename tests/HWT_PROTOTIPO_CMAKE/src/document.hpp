#ifndef _DOCUMENT_HPP
#define _DOCUMENT_HPP

#include "memReport.hpp"
#include "types.hpp"
#include "header.hpp"
#include "body.hpp"

namespace hwt
{
	class document
	{
		private:
			document(const document& d) :
							Width(d.Width),
							Height(d.Height),
							Priority(d.Priority)
							{}
			document& operator=(const document&) { return *this; }

		private:
			static uInt_t count;

		public:
			uInt_t Width, Height, Priority;
		
		public:
			header Header;
			body Body;

		public:
			document(uInt_t width = 600, uInt_t height = 480) :
				Width(width),
				Height(height),
				Priority(count++)
			{}
			 ~document() {}


	};
}

#endif //_DOCUMENT_HPP

