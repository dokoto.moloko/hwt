#ifndef _EMSG_HPP
#define _EMSG_HPP

#include "memReport.hpp"
#include <string>
#include <stdexcept>

namespace hwt
{
	class emsg : public std::runtime_error
	{
		public:
			emsg(const std::string& msg = "Exception Happened");

	};
}

#endif // _EMSG_HPP


