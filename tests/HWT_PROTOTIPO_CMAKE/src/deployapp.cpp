#include "deployapp.hpp"

namespace hwt
{    

    const Char_t*      hwt::deploy::txPOST = "POST";
    const Char_t*      hwt::deploy::txGET = "GET";
    deploy::deploy(session& ss) :
		Session(ss)
	{
	}
	
	deploy::~deploy()
	{
	}


    void deploy::run ()
    {
      struct MHD_Daemon * d;
      d = MHD_start_daemon(MHD_USE_THREAD_PER_CONNECTION, 
                                static_cast<uint16_t>(Session.Port), 
                                reinterpret_cast<MHD_AcceptPolicyCallback>(&hwt::deploy::on_client_connect), 
                                (void*)this, 
                                reinterpret_cast<MHD_AccessHandlerCallback>(&hwt::deploy::answer_to_connection), 
                               (void*)this, 
                                MHD_OPTION_NOTIFY_COMPLETED, 
                                reinterpret_cast<MHD_AccessHandlerCallback>(&hwt::deploy::request_completed), 
                               (void*)this, 
                                MHD_OPTION_END);
    }
    
    int deploy::on_client_connect(void* , const sockaddr* addr, socklen_t )
    {		
		return MHD_YES;
    }    
        
    int deploy::answer_to_connection(void* , struct MHD_Connection* connection, const char* url, const char* method, 
                                     const char* version, const char* upload_data, size_t* upload_data_size, void** con_cls)
    {
#ifdef DEBUG
		std::printf("%s %s %s\n", version, method, url);
#endif        
		Str_t smethod(method);    
		if (*con_cls == NULL)
		{
            Connection_info_struct_t* con_info = new (VAR2STR(Connection_info_struct_t)) Connection_info_struct_t;        
			if (con_info == NULL) return MHD_NO;
			con_info->answerstring.clear();

			if (smethod.compare(txPOST) == 0)
			{
				con_info->postprocessor = MHD_create_post_processor(connection, POST_BUFFER_SIZE, reinterpret_cast<MHD_PostDataIterator>(&hwt::deploy::iterate_post), (void *) con_info);
				if (con_info->postprocessor == NULL)
				{
#ifdef DEBUG                    
					std::printf("[ERROR-LOG][deploy::answer_to_connection] POST Request not datas to process.\n");
#endif                    
					delete con_info;
					return MHD_NO;
				}
				con_info->connectiontype = POST;
			}
			else if (smethod.compare(txGET) == 0)	
            {
				con_info->connectiontype = GET;			
            }

			*con_cls = (void *) con_info;

			return MHD_YES;
		}

		if (smethod.compare(txGET) == 0)
		{
			Str_t page, mimetype;
			deploy::page_dispatcher(Str_t(url), page, mimetype);
			return send_page (connection, page, mimetype);
		}

		if (smethod.compare(txPOST) == 0)
		{
			Connection_info_struct_t* con_info = reinterpret_cast<Connection_info_struct_t*>(*con_cls);

			if (*upload_data_size != 0)
			{
				MHD_post_process (con_info->postprocessor, upload_data, *upload_data_size);
				*upload_data_size = 0;
	            
				return MHD_YES;
			}
			else if (!con_info->post_key_value.empty())
			{     				
				
			}
		}
		Str_t page, mimetype;
		deploy::page_dispatcher("", page, mimetype);
		return send_page (connection, page, mimetype); 	
    }    

	int deploy::send_page (struct MHD_Connection* connection, const Str_t& page, const Str_t& mimetype)
	{
		int ret = 0;
		struct MHD_Response* response = NULL;
		
		response = MHD_create_response_from_data(page.size(), (void*) page.c_str(), MHD_NO, MHD_YES);
		if (!response) return MHD_NO;
		MHD_add_response_header (response, "Content-Type", mimetype.c_str());
		ret = MHD_queue_response (connection, MHD_HTTP_OK, response);
		MHD_destroy_response (response);

		return ret;    
	}    
    
    void deploy::request_completed(void* , struct MHD_Connection* , void** con_cls, enum MHD_RequestTerminationCode )
    {
        Connection_info_struct_t* con_info = reinterpret_cast<Connection_info_struct_t*>(*con_cls);		

		if (con_info->connectiontype == POST && con_info != NULL)
		{
			MHD_destroy_post_processor(con_info->postprocessor);
			con_info->answerstring.clear();            
		}

		delete con_info;
		*con_cls = NULL;
    }    
    
	int deploy::iterate_post ( void* coninfo_cls, enum MHD_ValueKind , const char* key, const char* , const char* ,
								const char* , const char* data, uint64_t , size_t )
	{
        
		Connection_info_struct_t* con_info = reinterpret_cast<Connection_info_struct_t*>(coninfo_cls);
		static size_t num_of_param = 0;
		num_of_param++;
		std::printf( "POST REQUEST [ %s, %s ]\n", key, data);

		
		return (0) ? MHD_NO : MHD_YES;		
	}    
    
    void deploy::page_dispatcher(const Str_t& page_request, Str_t& page_return, Str_t& mimetype)
	{				

	}    
    
	void deploy::Release(void)
	{
		/*
		params_t param;
		Session.Document(0).Body.Button(0).executeEvent("ljklkj", param, Session);
		Session.Document(0).Body.Button(0).GridCell_x;
		*/	
	}

}


