#ifndef _LINE_HPP
#define	_LINE_HPP

#include "memReport.hpp"
#include "types.hpp"
#include "button.hpp"
#include "input.hpp"
#include "label.hpp"

namespace hwt
{
    class line 
    {
	private:
                line(const line&) {}
		line& operator=(const line&) { return *this; }        
                
	private:
		template<class vWidget_GT> void DeallocateWidgets(vWidget_GT vWidget)
		{
			for(typename vWidget_GT::iterator it = vWidget.begin(); it != vWidget.end(); it++) if (*it) delete *it;
		}                
       
	private:
		std::vector<button*>    vButton;
		std::vector<input*>     vInput;
		std::vector<label*>     vLabel;                
                
        public:
                line(void);            
                ~line(void);
                
	public:		
                void            AddButton(Str_t labeltx = "", uInt_t width = 200, uInt_t height = 100 );
                button&         ModButton(uInt_t index);
		const button&   ModButton(uInt_t index) const;                
                void            DelButton(uInt_t index);
		
		void            AddInput(Str_t labeltx = "", uInt_t width = 200, uInt_t height = 100 );
                input&          ModInput(uInt_t index);
		const input&    ModInput(uInt_t index) const;
                void            DelInput(uInt_t index);
                
		void            AddLabel(Str_t labeltx = "", uInt_t width = 200, uInt_t height = 100 );			
		label&          ModLabel(uInt_t index);
		const label&    ModLabel(uInt_t index) const;                                                
                void            DelLabel(uInt_t index);

    };
}
#endif	// _LINE_HPP

