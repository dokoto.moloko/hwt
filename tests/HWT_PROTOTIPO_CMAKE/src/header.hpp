#ifndef _HEADER_HPP
#define _HEADER_HPP

#include "memReport.hpp"
#include "types.hpp"

namespace hwt
{
	/*
	
	USAR CONSTANTES ESTATICAS PARA OCUPAR LA MEMORIA UNA SOLA VEZ Y RESERVAR POR CADA INTANCIA DEL OBJ

	*/

	class header
	{
		private:
			header(const header& h) : Title(h.Title) {}
			header& operator=(const header&) { return *this; }

		public:
			Str_t Title;

		public:
			header(Str_t title = "New Web C++ Application") :
			  Title(title) {}
			  ~header() {}
				
	};
}

#endif //_HEADER_HPP

