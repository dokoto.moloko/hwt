#include "line.hpp"

namespace hwt
{
    line::line(void) 
    { 
    }
    
    line::~line(void) 
    { 
#ifdef DEBUG
		std::cout << "[line::~line] It release : " << vButton.size() << " pointers to Button class." << std::endl;
#endif
		DeallocateWidgets(vButton);
#ifdef DEBUG
		std::cout << "[line::~line] It release : " << vLabel.size() << " pointers to Label class." << std::endl;
#endif
		DeallocateWidgets(vLabel);
#ifdef DEBUG
		std::cout << "[line::~line] It release : " << vInput.size() << " pointers to Input class." << std::endl;
#endif
		DeallocateWidgets(vInput);      
    }

    button& line::ModButton(uInt_t index) 
	{ 
		if (index >= vButton.size()) throw emsg("[ERROR][body::Button] Index out of range.");
		return *vButton[index]; 
	}

	input& line::ModInput(uInt_t index) 
	{ 
		if (index >= vInput.size()) throw emsg("[ERROR][body::Input] Index out of range.");
		return *vInput[index]; 
	}

	label& line::ModLabel(uInt_t index) 
	{ 
		if (index >= vLabel.size()) throw emsg("[ERROR][body::Label] Index out of range.");
		return *vLabel[index]; 
	}

	const button& line::ModButton(uInt_t index) const
	{ 
		if (index >= vButton.size()) throw emsg("[ERROR][body::Button const] Index out of range.");
		return *vButton[index]; 
	}

	const input& line::ModInput(uInt_t index) const
	{ 
		if (index >= vInput.size()) throw emsg("[ERROR][body::Input const] Index out of range.");
		return *vInput[index]; 
	}

	const label& line::ModLabel(uInt_t index) const
	{ 
		if (index >= vLabel.size()) throw emsg("[ERROR][body::Label const] Index out of range.");
		return *vLabel[index]; 
	}

	void line::AddButton(Str_t labeltx, uInt_t width, uInt_t height ) 
	{ 
		vButton.push_back( new (VAR2STR(button)) button(labeltx, width, height) ); 
	}

	void line::AddInput(Str_t labeltx, uInt_t width, uInt_t height) 
	{ 
		vInput.push_back( new (VAR2STR(input)) input(labeltx, width, height) ); 
	}

	void line::AddLabel(Str_t labeltx, uInt_t width, uInt_t height) 
	{ 
		vLabel.push_back( new (VAR2STR(label)) label(labeltx, width, height) ); 
	}
    
}
