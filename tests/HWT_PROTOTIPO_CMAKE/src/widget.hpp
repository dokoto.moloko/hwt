#ifndef _WIDGET_HPP
#define _WIDGET_HPP

#include "memReport.hpp"
#include "types.hpp"

namespace hwt
{
	class session;
	class deploy;
	class widget
	{
		private:
			widget(const widget& w) : Uuid(reinterpret_cast<uInt_t>(&w)) {}
			widget& operator=(const widget&) { return *this; }

		private: 
			void executeEvent(const Char_t* EventName, const params_t& params, session& ss)  
									{ 
        									const ptrParams_t ptrF = EventPool[EventName];
										(*ptrF)(params, ss); 
									}

			void executeEvent(const Char_t* EventName, const params_t& params, session& ss) const 
									{ 																					
										(*(EventPool.find(EventName)->second)) (params, ss);
									}
		public:
			const uInt_t Uuid;
			Str_t Labeltx;
			uInt_t Width, Height;		
			friend class session;
			friend class deploy;
			

		private:
			std::map<Str_t, ptrParams_t>		EventPool;
				

		public: // Constructor
			widget(Str_t labeltx, uInt_t width, uInt_t height ) :	Uuid(reinterpret_cast<uInt_t>(this)),
										Labeltx(labeltx),
										Width(width),
										Height(height){}												
														 
			virtual ~widget() {}

		public: // Events
			virtual void AddEvent(const ptrParams_t ptrF, const char* eventName) { EventPool[eventName] = ptrF; }
	};

}

#endif //_WIDGET_HPP

