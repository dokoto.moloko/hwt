#include "session.hpp"

namespace hwt
{

	session::~session()
	{
#ifdef DEBUG
		std::cout << "[session::~session] It release : " << vDocument.size() << " pointers to Document class." << std::endl;
#endif
		for(std::vector<document*>::iterator it = vDocument.begin(); it != vDocument.end(); it++) if (*it) delete *it;
#ifdef DEBUG
		std::cout << "[session::~session] It release DeployEngine pointers to Document class." << std::endl;
#endif
		if (DeployEngine) delete DeployEngine;
	}

	document& session::Document(uInt_t index) 
	{ 
		if (index >= vDocument.size()) throw emsg("[ERROR][session::Document] Index out of range.");
		return *vDocument[index]; 
	}

	const document&	session::Document(uInt_t index) const
	{
		if (index >= vDocument.size()) throw emsg("[ERROR][session::Document] Index out of range.");
		return *vDocument[index];
	}

	void session::AddDocument(uInt_t width, uInt_t height) 
	{ 
		vDocument.push_back(new (VAR2STR(document)) document(width, height)); 
	}

	uInt_t session::NumOfDocuments(void) const
	{
		return (uInt_t)vDocument.size();
	}

	void session::Deploy(void)
	{
		DeployEngine = new (VAR2STR(deploy)) deploy(*this);
		DeployEngine->Release();
	}

}

