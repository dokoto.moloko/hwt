#ifndef _LABEL_HPP
#define _LABEL_HPP

#include "memReport.hpp"
#include "types.hpp"
#include "widget.hpp"

namespace hwt
{
	class label : public widget
	{
		private:
			label(const label& l )  :	widget(	l.Labeltx, l.Width, l.Height ) {}
			label& operator=(const label&) { return *this; }
		
		public:
			label(	Str_t labeltx = "", uInt_t width = 200, uInt_t height = 100  );
			~label();
	};
}

#endif //_LABEL_HPP


