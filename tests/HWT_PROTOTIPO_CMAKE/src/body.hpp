#ifndef _BODY_HPP
#define _BODY_HPP

#include "memReport.hpp"
#include "types.hpp"
#include "line.hpp"

namespace hwt
{	
    class body
    {                    
        private:
                body(const body&) {}
                body& operator=(const body&) { return *this; }

        private:
            std::vector<line*>  vLine;
            
        public:
                body();
                ~body();
                
        public:
            void                AddLine(void);
            line&               ModLine(uInt_t index);
            const line&         ModLine(uInt_t index) const;
            void                DelLine(uInt_t index);
    };
}

#endif //_BODY_HPP


