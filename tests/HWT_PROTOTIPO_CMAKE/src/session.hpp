#ifndef _SESSION_HPP
#define _SESSION_HPP

#include "memReport.hpp"
#include "types.hpp"
#include "document.hpp"
#include "deployapp.hpp"

namespace hwt
{
	class deploy;
	class session
	{
		private:
			session(const session& s) : Uuid(reinterpret_cast<uInt_t>(&s)),
										Port(s.Port),
										TimeOutSeg(s.TimeOutSeg) {}
			session& operator=(const session&) { return *this; }

		public:
			friend class deploy;
		public:
			const uInt_t Uuid;
			const uInt_t Port;
			const uInt_t TimeOutSeg;
			deploy* DeployEngine;

		private:
			std::vector<document*> vDocument;

		public:
			session(uInt_t port = 4444, uInt_t timeoutseg = 10 ):	Uuid(reinterpret_cast<uInt_t>(this)),
																	Port(port),
																	TimeOutSeg(timeoutseg),
																	DeployEngine(NULL) {}		
			~session();

		public:
			void				AddDocument(uInt_t width = 600, uInt_t height = 480);
			document&			Document(uInt_t index);
			const document&		Document(uInt_t index) const;
			uInt_t				NumOfDocuments(void) const;
			void				Deploy(void);
	};
}

#endif //_SESSION_HPP

