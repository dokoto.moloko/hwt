#include "emsg.hpp"

namespace hwt
{
	emsg::emsg(const std::string& msg) : 
		std::runtime_error(msg)

		{
		}
}

