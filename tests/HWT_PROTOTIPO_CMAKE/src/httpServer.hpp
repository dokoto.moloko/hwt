#ifndef _HTTPSERVER_HPP
#define _HTTPSERVER_HPP

#include "memReport.hpp"
#ifdef _MSC_VER
	#define MHD_PLATFORM_H    
	#include <winsock2.h> // socket related definitions
	#include <ws2tcpip.h> // socket related definitions

	typedef SSIZE_T ssize_t;
	typedef UINT64 uint64_t; //convey Windows type to unix type
	typedef UINT16 uint16_t; //convey Windows type to unix type
#else
	#include <sys/types.h>
	#include <sys/select.h>
	#include <sys/socket.h>
	#include <arpa/inet.h>
	#include <stdarg.h>
#endif

extern "C" 
{	        
	#include "microhttpd.h"    
}

#endif // _HTTPSERVER_HPP

