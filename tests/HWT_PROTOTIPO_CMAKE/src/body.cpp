#include "body.hpp"

namespace hwt
{
    body::body() { }
	body::~body() { }    

    void body::AddLine ()
    {      
      vLine.push_back(new (VAR2STR(line)) line);
    }
    
    void body::DelLine (uInt_t index)
    {
      if (index >= vLine.size()) throw emsg("[ERROR][body::DelLine] Index out of range.");
      line* auxLine = vLine[index];
      if (auxLine) delete auxLine;
      vLine.erase (vLine.begin () + index);
    }
    
    line& body::ModLine (uInt_t index)
    {
      if (index >= vLine.size()) throw emsg("[ERROR][body::ModLine] Index out of range.");
      return *vLine[index];
    }
    
    const line& body::ModLine (uInt_t index) const
    {
      if (index >= vLine.size()) throw emsg("[ERROR][body::ModLine const] Index out of range.");
      return *vLine[index];
    }
}


