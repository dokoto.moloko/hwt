#ifndef _UTILS_HPP
#define _UTILS_HPP

#include "memReport.hpp"
#include "types.hpp"


namespace hwt
{	
namespace utl
{	
	template <class type_tp> 
        Str_t           ConvToStr_T             (type_tp value) { Buffer_t ss; ss << value; return ss.str(); }
	value_t         ConvToStr               (int value);
	value_t         ConvToStr               (long value);
	value_t         ConvToStr               (double value);
	value_t         ConvToStr               (float value);
	value_t         ConvToStr               (char value);
	value_t         ConvToStr               (Str_t value);	
	bool            Split			(const Char_t& sep, const Char_t* raw, std::vector<Str_t> split_values);
	Str_t		CurrentDir		(void);
	size_t		NumOfDirLevels		(void);
        size_t		NumOfDirLevels		(const Char_t* path);
	Char_t		GetDirSep		(void);

	/* DIRLEVEL(CInt_t& Level, Str_t& Dir)
	* Si tenemos el Directorio C:\uno\dos\tres\cuatro y queremos obtener la ruta hasta C:\uno\dos
	* le pasamos (NºNIVELES - 2), es decir, dos niveles arriba
	* Ej:
	* Str_t& Dir;
	* DirLevel(NumOfDirLevels() - 2, Dir);
	*/
	Str_t		DirLevel		(const uInt_t& Level, const Str_t& path);		
	Str_t		DirLevel		(const uInt_t& Level);
	Str_t		FixWinPath		(const Str_t& path);	
	Str_t		ExtractEXTENSION	(const Str_t& path);
	bool		ListFiles		(const Char_t* path, const Char_t tipo, Vfile_t& files);		
#ifdef _MSC_VER
	bool		ListFilesWIN            (CoCStr_t path, Vfile_t& files);
#else
        bool		ListFilesUNIX		(const Char_t* path, Vfile_t& files);
#endif	        


}
}

#endif //_UTILS_HPP
