#ifndef CONTAINER_HPP
#define	CONTAINER_HPP

#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <string>

typedef struct 
{
	bool released;
	const char* name;
	size_t size;
	const char* filename;
	unsigned line;
} Traze_t;

typedef struct
{
	ptrdiff_t first;
	Traze_t second;
} Content_t;

class Container
{
	private:
		Content_t* ptr;
		size_t size;
		size_t block;
		size_t mult;
		size_t unid;
	public:
		Container() : ptr(NULL), size(0), block(100), mult(1), unid(sizeof(Content_t)) 
		{
			ptr = (Content_t*) malloc(unid * block);
			memset(ptr, 0, (unid * block));
		}

		~Container()
		{
			if (ptr)
			{
				free(ptr);
				ptr = NULL;
			}
		}
		void Add(ptrdiff_t first, Traze_t second)
		{
			if (size == ((block * mult) - 5) )
			{
				mult++;
				ptr = (Content_t*) realloc(ptr, unid * (block * mult));
				memset(ptr+block, 0, unid * (block * mult));
				block *= mult;
			}
			else
			{
				Content_t* p = ptr+size;
				p->first = first;
				p->second.released = second.released;
				p->second.name = second.name;
				p->second.size = second.size;
				p->second.filename = second.filename;
				p->second.line = second.line;
				size++;
			}
		}
	
		Content_t* Search(ptrdiff_t first)
		{
			Content_t* p = NULL;
			for(size_t i = 0; i < size; i++)
			{				
				p = ptr+i;
				if (p->first == first && p->second.released == false)
					return p;
			}
			return NULL;
		}

		void SetRelease(ptrdiff_t first, bool opc)
		{
			Content_t* p = NULL;
			p = Search(first);
			if (p != NULL)
				p->second.released = opc;
		}

                const char* bool2str(bool b) { return (b)? "TRUE":"FALSE"; }
		void ReportAllocStatus()
		{
			Content_t* p = NULL;			
			bool withLeaks = false;
			for(size_t i = 0; i < size; i++)
			{
				p = ptr+i;
				if  (p->second.released) continue;
				withLeaks = true;
				std::printf(
"MEMORY LEAKS REPORT\n\n\
ID: %ld VAR NAME: %s\n\
--------------------------------------------------------------\n\
RELEASE         : %s\n\
BYTES ALLOCATED : %d\n\
LINE            : %d\n\
FILENAME        : %s\n\
--------------------------------------------------------------\n\n", 
					(long)p->first, p->second.name, bool2str(p->second.released),
					p->second.size, p->second.line, p->second.filename);
			}
			if (withLeaks == false) std::printf("GOOD !!! NO MEMORY LEAKS TO REPORT.\n");
		}		
};

#endif	/* CONTAINER_HPP */

