#include "utils.hpp"



namespace hwt
{
namespace utl
{	
	template <class type_tp> 
    FUNCTION PRIVATE DEFINITION static Str_t ConvToStr_T
		(
			type_tp value
		) 
	{ 
		Buffer_t ss; ss << value; return ss.str(); 
	}

	FUNCTION PUBLIC DEFINITION value_t ConvToStr
		(
			int value
		)
	{
		value_t ret = { ConvToStr_T<int>(value), hwt::INT };  
		return ret; 
	}

	FUNCTION PUBLIC DEFINITION value_t ConvToStr
		(
			long value
		)
	{ 
		value_t ret = { ConvToStr_T<long>(value), hwt::LONG };  
		return ret; 
	}

	FUNCTION PUBLIC DEFINITION value_t ConvToStr
		(
			double value
		)
	{ 
		value_t ret = { ConvToStr_T<double>(value), hwt::DOUBLE };  
		return ret; 
	}

	FUNCTION PUBLIC DEFINITION value_t ConvToStr
		(
			float value
		)
	{ 
		value_t ret = { ConvToStr_T<float>(value), hwt::FLOAT };  
		return ret; 
	}

	FUNCTION PUBLIC DEFINITION value_t ConvToStr
		(
			char value
		)
	{ 
		value_t ret = { ConvToStr_T<char>(value), hwt::CHAR };  
		return ret; 
	}

	FUNCTION PUBLIC DEFINITION value_t ConvToStr
		(
			Str_t value
		)
	{ 
		value_t ret = { ConvToStr_T<Str_t>(value), hwt::STRING };  
		return ret; 
	}	

	FUNCTION PUBLIC DEFINITION bool Split
		(
			const Char_t& sep, 
			const Char_t* raw, 
			std::vector<Str_t> split_values
		)
		{
			Str_t line(raw);
			size_t pos = 0;
			while((pos = line.find_first_of(sep)) != std::string::npos)
			{
                if (pos > 0)
                {
                    Str_t extr = line.substr(0, pos);
                    if (extr.size() > 0)
                        split_values.push_back(extr);
                }
				line = line.substr(pos+1, line.size());
			}
            if (line.size() > 0)
                split_values.push_back(line);
			if (split_values.size() == 0) return false;

			return true;
		}

		FUNCTION PUBLIC DEFINITION Str_t CurrentDir
			(
				void
			)
		{
			char rawDir[FILENAME_MAX];
			GetCurrentDir(rawDir, FILENAME_MAX);			
			return std::string(rawDir);
		}

		FUNCTION PUBLIC DEFINITION size_t NumOfDirLevels
			(
				void
			)
		{
			std::vector<Str_t> dirs;
			Split(GetDirSep(), CurrentDir().c_str(), dirs);
			return dirs.size();
		}
        
        FUNCTION PUBLIC DEFINITION size_t NumOfDirLevels
			(
				const Char_t* path
			)
		{
			std::vector<Str_t> dirs;
			Split(GetDirSep(), path, dirs);
			return dirs.size();
		}
        

		FUNCTION PUBLIC DEFINITION Str_t DirLevel
			(
				const uInt_t& Level
			)
		{
			std::vector<Str_t> dirs;
			Str_t Dir;
			Split(GetDirSep(), CurrentDir().c_str(), dirs);
			if (Level > 0 && Level < dirs.size())			
			{
				Dir.clear();
				size_t i = 0;
				for(std::vector<Str_t>::iterator it = dirs.begin(); i < Level && it != dirs.end(); it++, i++)				
				{
					if (it == dirs.begin())
                    {
                        if (GetDirSep() == '/')
                            Dir.append(1, '/');
						Dir.append(*it);
                    }
					else
						Dir.append(GetDirSep() + *it);				
				}
			}
			return Dir;
		}
	
		FUNCTION PUBLIC DEFINITION Str_t DirLevel
			(
				const uInt_t& Level, 
				const Str_t& path
			)
		{
			std::vector<Str_t> dirs;
			Str_t Dir;
			Split(GetDirSep(), path.c_str(), dirs);
#           ifndef WIN
            if (dirs.size() == 1)
                return Str_t("/");
#           endif
			if (Level > 0 && Level < dirs.size())
			{
				Dir.clear();
				size_t i = 0;
                
				for(std::vector<Str_t>::iterator it = dirs.begin(); i < Level && it != dirs.end(); it++, i++)				
				{
					if (it == dirs.begin())
                    {
                        if (GetDirSep() == '/')
                            Dir.append(1, '/');
						Dir.append(*it);
                    }
					else
						Dir.append(GetDirSep() + *it);				
				}
			}
			return Dir;
		}

		FUNCTION PUBLIC DEFINITION Char_t GetDirSep
			(
				void
			)
		{			
			Str_t like_unix = CurrentDir().substr(0, 1);
			Str_t like_win  = CurrentDir().substr(1, 2);

			if ( like_unix.compare("/") == 0 )
				return '/';
			else if ( like_win.compare(":\\") == 0 ) 
				return '\\';
			
			return '#';

		}

		FUNCTION PUBLIC DEFINITION Str_t FixWinPath
			(
				const Str_t& path
			)
		{
			size_t len = path.size();
			if (len == 0) return "";
			Str_t n_path;
			n_path.resize(len+1);
			char* c_str = new char[len+1];		
			memset(c_str, 0, len+1);
			strncpy(c_str, path.c_str(), len);
			size_t x = 0;
			for(size_t i = 0; i < len; i++)
			{
				if (*(c_str+i) == '\\' && *(c_str+i+1) != '\\')
				{
					n_path.resize(n_path.size()+1);
					n_path[x++] = '\\';
				}
				n_path[x++] = *(c_str+i);
			}
			delete[] c_str;
			c_str = NULL;

			return n_path;
		}

		FUNCTION PUBLIC DEFINITION Str_t ExtractEXTENSION
			(
				const Str_t& path
			)
		{
			return path.substr(path.find_last_of(".")+1, path.size());
		}

		FUNCTION PUBLIC DEFINITION bool ListFiles
			(
				const Char_t* path, 
				const Char_t tipo, 
				Vfile_t& files
			)
		{
			Str_t spath;
			if (tipo == 'R')
            {
                size_t level = NumOfDirLevels(path);
                if (level - 1 == 0)
                    level = 1;
                else if (level - 1 < 0)
                    goto list;
                else
                    level--;
				spath = DirLevel(level, path);
            }
        list:
			#ifdef _MSC_VER
                return ListFilesWIN((spath.empty())?path:spath.c_str(), files);
            #else
                return ListFilesUNIX((spath.empty())?path:spath.c_str(), files);
			#endif
		}
        
#ifdef _MSC_VER

		FUNCTION PUBLIC DEFINITION bool ListFilesWIN
			(
				const Char_t* path, 
				Vfile_t& files
			)
		{
			HANDLE hFind;
			WIN32_FIND_DATA data;
			
			// I - char* TO wchar_t* <== IN(path)
			size_t newsize = strlen(path) + 20;
			wchar_t * wcstring = new wchar_t[newsize];
			size_t convertedChars = 0;
			mbstowcs_s(&convertedChars, wcstring, newsize, path, _TRUNCATE);
			if ( convertedChars == 0 ) return false;
			newsize = convertedChars = 0;
			// F - char* TO wchar_t* ==> OUT(wcstring)

			StringCchCat(wcstring, MAX_PATH, TEXT("\\*"));
			hFind = FindFirstFile(wcstring, &data);
			if (hFind != INVALID_HANDLE_VALUE) 
			{
				char *nstring = NULL;
				size_t origsize = 0;
				newsize = 0;
				file_t hfile;
				do 
				{
					// I - wchar_t* TO char* <== IN(data.cFileName)
					origsize = wcslen(data.cFileName) + 20;						
					newsize = origsize * 2;								
					nstring = new char[newsize];
					wcstombs_s(&convertedChars, nstring, newsize, data.cFileName, _TRUNCATE);
					if ( convertedChars == 0 ) return false;
					// F - wchar_t* TO char* ==> OUT(nstring)
					
					if (data.dwFileAttributes & FILE_ATTRIBUTE_ARCHIVE)
						hfile.tipo = "F";
					else if (data.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
						hfile.tipo = "D";
					hfile.name = nstring;
					if (hfile.name.compare(".") == 0)
						goto clear;
                    if (hfile.name.compare("..") != 0 && hfile.name[0] == '.')
                        goto clear;
					hfile.ruta = path;
					if (hfile.name.compare("..") != 0)
					{
						Char_t lastc = hfile.ruta[hfile.ruta.size()-1];
						if (lastc != utl::GetDirSep())
							hfile.ruta.append(1, utl::GetDirSep());
						hfile.ruta.append(hfile.name);
						lastc = '\0';
						lastc = hfile.ruta[hfile.ruta.size()-1];
						if (lastc != utl::GetDirSep() && hfile.tipo.compare("D") == 0)
							hfile.ruta.append(1, utl::GetDirSep());
					}
                    else if (hfile.name.compare("..") == 0)
                    {
                        hfile.tipo = "R";
                    }
					files.push_back(hfile);
					clear:
						if (nstring) { delete[] nstring; nstring = NULL; }
						convertedChars = newsize = origsize = 0;
				} while (FindNextFile(hFind, &data));
				if (nstring) { delete[] nstring; nstring = NULL; }
			} else return false;
							
			//  LIMPIAR RECURSOS
			FindClose(hFind);
			if (wcstring) { delete[] wcstring; wcstring = NULL; }
			
			return true;
		}
        
#else // LINUX & MAC
        
        FUNCTION PUBLIC DEFINITION bool ListFilesUNIX
			(
				const Char_t* path, 
				Vfile_t& files
			)
        {
            DIR *pdir;
            struct dirent *pent;
            struct stat st;

            pdir=opendir(path);
            if (!pdir) return false;
            
            file_t hfile;
            std::string lpath;
            while ((pent=readdir(pdir)))
            {
                lpath.clear();
                lpath = path;
                if (lpath[lpath.size()-1] != hwt::utl::GetDirSep())
                    lpath.append(1, hwt::utl::GetDirSep());
                lpath.append(pent->d_name);
                memset(&st, 0, sizeof(struct stat));
                lstat(lpath.c_str(), &st);
                if(S_ISDIR(st.st_mode))
                    hfile.tipo = "D";
                else
                    hfile.tipo = "F";
                hfile.name = pent->d_name;
                if (hfile.name.compare(".") == 0)
                    continue;
                if (hfile.name.compare("..") != 0 && hfile.name[0] == '.')
                    continue;
                hfile.ruta = path;
                if (hfile.name.compare("..") != 0)
                {
                    Char_t lastc = hfile.ruta[hfile.ruta.size()-1];
                    if (lastc != hwt::utl::GetDirSep())
                        hfile.ruta.append(1, hwt::utl::GetDirSep());
                    hfile.ruta.append(hfile.name);
                    lastc = '\0';
                    lastc = hfile.ruta[hfile.ruta.size()-1];
                    if (lastc != hwt::utl::GetDirSep() && hfile.tipo.compare("D") == 0)
                        hfile.ruta.append(1, hwt::utl::GetDirSep());
                }
                else if (hfile.name.compare("..") == 0)
                {
                    hfile.tipo = "R";
                }
                files.push_back(hfile);
                
            }

            closedir(pdir);
            
            return true;
        }
#endif


	FUNCTION PUBLIC DEFINITION void DoFormatting
		(
			Str_t& sF, 
			const Char_t* sformat, 
			va_list marker
		)
	{
		Char_t* s, ch=0;
		int n, i=0, m;
		long l;
		double d;
		Str_t sf = sformat;
		Buffer_t ss;

		m = sf.length();
		while (i<m)
		{
			ch = sf.at(i);
			if (ch == '%')
			{
				i++;
				if (i<m)
				{
					ch = sf.at(i);
					switch(ch)
					{
						case 's': { s = va_arg(marker, char*);  ss << s;         } break;
						case 'c': { n = va_arg(marker, int);    ss << (char)n;   } break;
						case 'd': { n = va_arg(marker, int);    ss << (int)n;    } break;
						case 'l': { l = va_arg(marker, long);   ss << (long)l;   } break;
						case 'f': { d = va_arg(marker, double); ss << (float)d;  } break;
						case 'e': { d = va_arg(marker, double); ss << (double)d; } break;
						case 'X':
						case 'x':
							{
								if (++i<m)
								{
									ss << std::hex << std::setiosflags (std::ios_base::showbase);
									if (ch == 'X') ss << std::setiosflags (std::ios_base::uppercase);
									char ch2 = sf.at(i);
									if (ch2 == 'c') { n = va_arg(marker, int);  ss << std::hex << (char)n; }
									else if (ch2 == 'd') { n = va_arg(marker, int); ss << std::hex << (int)n; }
									else if (ch2 == 'l') { l = va_arg(marker, long);    ss << std::hex << (long)l; }
									else ss << '%' << ch << ch2;
									ss << std::resetiosflags (std::ios_base::showbase | std::ios_base::uppercase) << std::dec;
								}
							} break;
						case '%': { ss << '%'; } break;
						default:
						{
							ss << "%" << ch;
							//i = m; //get out of loop
						}
					}
				}
			}
			else ss << ch;
			i++;
		}
		va_end(marker);
		sF = ss.str();
	}

	FUNCTION PUBLIC DEFINITION void Stringf
		(
			Str_t& stgt, 
			const Char_t* sformat, 
			... 
		)
	{
		va_list marker;
		va_start(marker, sformat);
		DoFormatting(stgt, sformat, marker);
	}

	FUNCTION PUBLIC DEFINITION Str_t Stringf
		(
			const Char_t* sformat, 
			... 
		)
	{
		Str_t stgt;
		va_list marker;
		va_start(marker, sformat);
		DoFormatting(stgt, sformat, marker);

		return stgt;
	}

	FUNCTION PUBLIC DEFINITION bool ReadFullTxFile	
		(
			const Char_t* path, 
			Str_t& file
		)
	{
		return ReadFullTxFile(Str_t(path), file);
	}

	FUNCTION PUBLIC DEFINITION bool ReadFullBinFile
		(
			const Char_t* path, 
			Str_t& file
		)
	{
		return ReadFullBinFile(Str_t(path), file);
	}

	FUNCTION PUBLIC DEFINITION bool ReadFullTxFile	
		(
			const Str_t& path, 
			Str_t& file
		)
	{
		std::ifstream fileInput;
		fileInput.open( utl::FixWinPath(path).c_str() );
		if (fileInput.is_open())
		{            
			Str_t line;
			fileInput.seekg(0, std::ios::beg);
			Buffer_t ss;
			ss << fileInput.rdbuf();
			file = ss.str();
			fileInput.close();
			if (file.size() == 0) return false;

		} else return false;

		return true;
	}

	FUNCTION PUBLIC DEFINITION bool ReadFullBinFile
		(
			const Str_t& path, 
			Str_t& file
		)
	{
		std::ifstream BinFile (utl::FixWinPath(path).c_str(), std::ios::in | std::ios::binary | std::ios::ate);
		std::ifstream::pos_type fileSize = 0;
		Char_t* fileContents = NULL;
		if (BinFile.is_open())
		{
			fileSize = BinFile.tellg();
			fileContents = new char[fileSize];
			BinFile.seekg(0, std::ios::beg);
			if (!BinFile.read(fileContents, static_cast<std::streamsize>(fileSize) )) 
			{
				delete[] fileContents;
				fileContents = NULL;
				return false;
			}
			BinFile.close();
			file.append(fileContents, static_cast<size_t>(fileSize) );
			delete[] fileContents;
			fileContents = NULL;		

		} else return false;

		return true;    		
	}
}
}
