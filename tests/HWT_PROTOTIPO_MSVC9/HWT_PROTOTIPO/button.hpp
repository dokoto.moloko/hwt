#ifndef _BUTTON_HPP
#define _BUTTON_HPP

#include "types.hpp"
#include "widget.hpp"
#include "utils.hpp"
#include "webTags.hpp"

namespace hwt
{
	class button : public widget
	{
		private:
			COPY_CONSTRUCTOR PRIVATE PRIVATE button
				(
					const button& b 
				)  
				:	widget(	b.Labeltx, b.Width, b.Height, BUTTON ) {}

			OPERATOR PRIVATE DECLARATION button& operator=
				(
					const button&
				) { return *this; }
	
		public:
			CONSTRUCTOR PUBLIC DECLARATION button
				(	
					Str_t labeltx = "", 
					uInt_t width = 200, 
					uInt_t height = 100  
				);

			DESTRUCTOR PUBLIC DECLARATION ~button
				(
				);

			METHOD PUBLIC DECLARATION Str_t toHtml
				(
					void
				);
	};
}

#endif //_BUTTON_HPP

