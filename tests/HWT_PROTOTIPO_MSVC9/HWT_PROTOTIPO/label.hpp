#ifndef _LABEL_HPP
#define _LABEL_HPP

#include "types.hpp"
#include "widget.hpp"

namespace hwt
{
	class label : public widget
	{
		private:
			COPY_CONSTRUCTOR PRIVATE DEFINITION label
				(
					const label& l 
				)  :	widget(	l.Labeltx, l.Width, l.Height, LABEL ) {}

			OPERATOR PRIVATE DEFINITION label& operator=
				(
					const label&
				) { return *this; }
		
		public:
			CONSTRUCTOR PUBLIC DECLARATION label
				(	
					Str_t labeltx = "", 
					uInt_t width = 200, 
					uInt_t height = 100  
				);

			DESTRUCTOR PUBLIC DECLARATION ~label
				(
				);

			METHOD PUBLIC DECLARATION Str_t toHtml
				(
					void
				);
	};
}

#endif //_LABEL_HPP


