#include "main.hpp"

/*

	COSAS POR HACER:

	1 - Hay que crear tipos de input predefinidos, es decir, que no solo sea input
		al declarorlo declara un label + input + button y ponerles el atributo que 
		indique si el label va en la misma linea o en una anterior.
	2 - Crear el Input tipo Fecha.
	3 - Cambiar nombre del proyecto a HWEOT(HTML WIDGET EVENT ORIENTED TOOLKIT)
*/

int main(void)
{
	try
	{
		hwt::Str_t tt = hwt::utl::CurrentDir();
		hwt::session App;				
		
		App.Header.Title = "Aplicacion de Test : HTML WIDGET TOOLKIT FOR C++ v0.1";

		App.AddDocument("Window 1", "Pantalla 1 de Test", 800, 600);	                                

        App.Document(0).Body.AddLine();
        App.Document(0).Body.ModLine(0).AddLabel("Nombre", 200);		
		App.Document(0).Body.ModLine(0).AddInput("input01", 500);
		App.Document(0).Body.ModLine(0).ModInput(0).SetValue("Default Content");
		App.Document(0).Body.ModLine(0).AddButton("botton02");			
        
        App.Document(0).Body.AddLine();
        App.Document(0).Body.ModLine(1).AddLabel("Nombre_2", 200);		
		App.Document(0).Body.ModLine(1).AddInput("input01_2", 500);
		App.Document(0).Body.ModLine(1).ModInput(1).SetValue(34.56);
		App.Document(0).Body.ModLine(1).AddButton("botton02_2");

        App.Document(0).Body.AddLine();
        App.Document(0).Body.ModLine(2).AddLabel("Nombre_3", 200);		
		App.Document(0).Body.ModLine(2).AddInput("input01_3", 500);
		App.Document(0).Body.ModLine(2).ModInput(2).SetValue("12/12/2012");
		App.Document(0).Body.ModLine(2).AddButton("botton02_3");      
        
        App.Document (0).Body.DelLine (1);
		
		App.Document(0).Body.ModLine(0).ModButton(0).AddEvent(&handleClick, hwt::events[hwt::ONCLICK]);		
		// Check Error
		//App.Document(4).Body.ModLine(2).AddLabel("Nombre_3", 200);
		//App.Document(0).Body.ModLine(8).AddInput("input01_2", 500);
		//App.Document(0).Body.ModLine(4).ModButton(0).AddEvent(&handleClick, hwt::events[hwt::ONCLICK]);

		App.DoDeploy();
        
#ifdef DEBUG
        ReportMemStatus();
#endif
	}
	catch (std::exception& e)
	{
		std::cout << e.what() << std::endl;
#ifdef DEBUG
        ReportMemStatus();
#endif
	}
	
	std::cin.get();
}

void listParams(const hwt::params_t params)
{
	for(hwt::params_t::const_iterator cit = params.begin(); cit != params.end(); cit++)
		std::cout << "[" << &cit << "] PARAMETER : " << cit->first << " TYPE : " << hwt::types[cit->second.type]
		<< " VALUE : " << cit->second.value << std::endl;
}

void handleClick(const hwt::params_t& params, hwt::session& Session)
{
	Session.Document(0).Title = "Se cambia el Titulo para regenerar el documento 0.";
	listParams(params);	
}

/*
	hwt::params_t params;	
	params["param1"] = hwt::Load(10);
	params["param2"] = hwt::Load("Paco");	
	params["param3"] = hwt::Load(LONG(5123456));
	params["param4"] = hwt::Load('a');
	params["param5"] = hwt::Load(2.45);
	params["param6"] = hwt::Load(FLOAT(2.3456745));
	hwt::Click(&handleClick, params);
*/






