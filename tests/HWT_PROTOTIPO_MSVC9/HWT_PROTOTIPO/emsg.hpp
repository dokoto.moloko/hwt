#ifndef _EMSG_HPP
#define _EMSG_HPP

#include <string>
#include <stdexcept>
#include "layout_defs.hpp"


namespace hwt
{
	class emsg : public std::runtime_error
	{
		public:
			CONSTRUCTOR PUBLIC DECLARATION emsg
				( 
					const std::string& msg = "Exception Happened"
				);

	};
}

#endif // _EMSG_HPP


