#include "deployapp.hpp"

namespace hwt
{    
	namespace deployapp
	{				
		/*---------------------------------------------------------------------------------------------------------------------------------------
		 * TIPOS LOCALES A UNIDAD
		 *--------------------------------------------------------------------------------------------------------------------------------------*/
		struct ConnInf_tag
		{	
			Int_t							connectiontype;
			Str_t							answerstring;				
			struct MHD_PostProcessor*		postprocessor;
			std::map<Str_t, Str_t>          post_key_value;                        		
		};
		
		typedef struct ConnInf_tag					Connection_info_struct_t;
		typedef std::map<Str_t, Str_t>				Protocolo_t;

		/*---------------------------------------------------------------------------------------------------------------------------------------
		 * VARIABLES LOCALES A UNIDAD
		 *--------------------------------------------------------------------------------------------------------------------------------------*/		
		static const Char_t*						txPOST					= "POST";
		static const Char_t*						txGET					= "GET";
		static const Char_t*						txJAR					= ".jar";
		static const Char_t*						txHTML					= ".html";
		static const Char_t*						txHTM					= ".htm";
		static const Char_t*						txCSS					= ".css";
		static const Char_t*						txJS					= ".js";	
		static const uInt_t							POST_BUFFER_SIZE		= 512;
		static const uInt_t							MAX_NAME_SIZE			= 20;
		static const uInt_t							MAX_ANSWER_SIZE			= 512;
		static const Char_t*						jquery_js_path			= "";
		static const Char_t*						jquery_css_path			= "";
		static const Char_t*						jquery_ui_js_path		= "";
		static const Char_t*						custom_js_path			= "";
		static const Char_t*						custom_css_path			= "";

		enum										{GET, POST};
		enum										{JQUERY_JS, JQUERY_CSS, JQUERY_UI_JS, CUSTOM_JS, CUSTOM_CSS};

		static std::map<int, Str_t*>				request_files;
		static struct MHD_Daemon*					daemon;
		static session*								Session;		
		Protocolo_t									protocolo;
		std::map<Str_t, Str_t>						mimetypes;
		static Str_t								jquery_js;
		static Str_t								jquery_css;
		static Str_t								jquery_ui_js;
		static Str_t								custom_js;
		static Str_t								custom_css;
			
		/*---------------------------------------------------------------------------------------------------------------------------------------
		 * FUNCTIONS DECLARATIONS OF PRIVATE FUNCTIONS
		 *--------------------------------------------------------------------------------------------------------------------------------------*/
		FUNCTION PRIVATE DECLARATION static int send_page 
			(
				struct MHD_Connection* connection, 
				const Str_t& page, 
				const Str_t& mimetype
			);

		FUNCTION PRIVATE DECLARATION static int answer_to_connection
			(	
				void *cls,
				struct MHD_Connection * connection,
				const char *url,
				const char *method,
				const char *version,
				const char *upload_data,
				size_t *upload_data_size,
				void **con_cls
			);

		FUNCTION PRIVATE DECLARATION static int on_client_connect
			(
				void* cls, 
				const sockaddr * addr, 
				socklen_t addrlen 
			);

		FUNCTION PRIVATE DECLARATION static void request_completed
			(
				void* , 
				struct MHD_Connection* , 
				void** con_cls, 
				enum MHD_RequestTerminationCode 
			);

		FUNCTION PRIVATE DECLARATION static int iterate_post 
			(	
				void *cls,
				enum MHD_ValueKind kind,
				const char *key,
				const char *filename,
				const char *content_type,
				const char *transfer_encoding,
				const char *data, 
				uint64_t off, 
				size_t size 
			);

		FUNCTION PRIVATE DECLARATION static void page_dispatcher
			(
				const Str_t& page_request, 
				Str_t& page_return, 
				Str_t& mimetype
			);

		/*---------------------------------------------------------------------------------------------------------------------------------------
		 * PUBLIC FUNCTIONS DEFINITIONS 
		 *--------------------------------------------------------------------------------------------------------------------------------------*/
		FUNCTION PUBLIC DEFINITION void Init
			(
				session* ss
			)			
		{
			Session = ss;
			
			mimetypes["jar"]	= "application/zip";
			mimetypes["html"]	= "text/html";
			mimetypes["css"]	= "text/css";
			mimetypes["js"]		= "text/javascript";
			mimetypes["jpeg"]	= "image/jpeg";
			mimetypes["gif"]	= "image/gif";

			if (utl::ReadFullTxFile(jquery_js_path, jquery_js) == false)
				throw emsg("[ERROR][deployapp::Init] Loading file : " + Str_t(jquery_js_path));
			request_files[JQUERY_JS] = &jquery_js;

			if (utl::ReadFullTxFile(jquery_css_path, jquery_css) == false)
				throw emsg("[ERROR][deployapp::Init] Loading file : " + Str_t(jquery_css_path));
			request_files[JQUERY_CSS] = &jquery_css;

			if (utl::ReadFullTxFile(jquery_ui_js_path, jquery_ui_js) == false)
				throw emsg("[ERROR][deployapp::Init] Loading file : " + Str_t(jquery_ui_js_path));
			request_files[JQUERY_UI_JS] = &jquery_ui_js;

			if (utl::ReadFullTxFile(custom_js_path, custom_js) == false)
				throw emsg("[ERROR][deployapp::Init] Loading file : " + Str_t(custom_js_path));
			request_files[CUSTOM_JS] = &custom_js;

			if (utl::ReadFullTxFile(custom_css_path, custom_css) == false)
				throw emsg("[ERROR][deployapp::Init] Loading file : " + Str_t(custom_css_path));
			request_files[CUSTOM_CSS] = &custom_css;
		}
		
		FUNCTION PUBLIC DEFINITION void Destroy
			(
				void
			)
		{
		}


		FUNCTION PUBLIC DEFINITION void Run 
			(
				void
			)
		{		  
			daemon = MHD_start_daemon (	MHD_USE_THREAD_PER_CONNECTION, static_cast<uint16_t>(Session->Port), 
										on_client_connect,	NULL, 
										answer_to_connection, NULL, 
										MHD_OPTION_NOTIFY_COMPLETED,
										request_completed,	NULL, 
										MHD_OPTION_END);
		}
	    
		FUNCTION PUBLIC DEFINITION void Publish
			(
				void
			)
		{
			/*
			params_t param;
			Session.Document(0).Body.Button(0).executeEvent("ljklkj", param, Session);
			Session.Document(0).Body.Button(0).GridCell_x;
			*/	
		}



		/*---------------------------------------------------------------------------------------------------------------------------------------
		 * PRIVATE FUNCTIONS DEFINITIONS 
		 *--------------------------------------------------------------------------------------------------------------------------------------*/

		FUNCTION PRIVATE DEFINITION static int on_client_connect
			(
				void* cls, 
				const sockaddr * addr, 
				socklen_t addrlen 
			)
		{		
			return MHD_YES;
		}    
	        


		FUNCTION PRIVATE DEFINITION static int answer_to_connection
			(
				void *cls, 
				struct MHD_Connection * connection, 
				const char *url, 
				const char *method,
				const char *version, 
				const char *upload_data, 
				size_t *upload_data_size, 
				void **con_cls
			)
		{
			#ifdef DEBUG
				std::printf("%s %s %s\n", version, method, url);
			#endif        
			Str_t smethod(method);    
			if (*con_cls == NULL)
			{
				Connection_info_struct_t* con_info = new (VAR2STR(Connection_info_struct_t)) Connection_info_struct_t;        
				if (con_info == NULL) return MHD_NO;
				con_info->answerstring.clear();

				if (smethod.compare(txPOST) == 0)
				{
					con_info->postprocessor = MHD_create_post_processor(connection, POST_BUFFER_SIZE, iterate_post, (void *) con_info);
					if (con_info->postprocessor == NULL)
					{
						#ifdef DEBUG                    
							std::printf("[ERROR-LOG][deploy::answer_to_connection] POST Request not datas to process.\n");
						#endif                    
						delete con_info;
						return MHD_NO;
					}
					con_info->connectiontype = POST;
				}
				else if (smethod.compare(txGET) == 0)	
				{
					con_info->connectiontype = GET;			
				}

				*con_cls = (void *) con_info;

				return MHD_YES;
			}

			if (smethod.compare(txGET) == 0)
			{
				Str_t page, mimetype;
				page_dispatcher(Str_t(url), page, mimetype);
				return send_page (connection, page, mimetype);
			}

			if (smethod.compare(txPOST) == 0)
			{
				Connection_info_struct_t* con_info = reinterpret_cast<Connection_info_struct_t*>(*con_cls);

				if (*upload_data_size != 0)
				{
					MHD_post_process (con_info->postprocessor, upload_data, *upload_data_size);
					*upload_data_size = 0;
		            
					return MHD_YES;
				}
				else if (!con_info->post_key_value.empty())
				{     				
					
				}
			}
			Str_t page, mimetype;
			deployapp::page_dispatcher("", page, mimetype);
			return send_page (connection, page, mimetype); 	
		}    

		FUNCTION PRIVATE DEFINITION static int send_page 
			(
				struct MHD_Connection* connection, 
				const Str_t& page, 
				const Str_t& mimetype
			)
		{
			int ret = 0;
			struct MHD_Response* response = NULL;
			
			response = MHD_create_response_from_data(page.size(), (void*) page.c_str(), MHD_NO, MHD_YES);
			if (!response) return MHD_NO;
			MHD_add_response_header (response, "Content-Type", mimetype.c_str());
			ret = MHD_queue_response (connection, MHD_HTTP_OK, response);
			MHD_destroy_response (response);

			return ret;    
		}    
	    
		FUNCTION PRIVATE DEFINITION static void request_completed
			(
				void* , 
				struct MHD_Connection* , 
				void** con_cls, 
				enum MHD_RequestTerminationCode 
			)
		{
			Connection_info_struct_t* con_info = reinterpret_cast<Connection_info_struct_t*>(*con_cls);		

			if (con_info->connectiontype == POST && con_info != NULL)
			{
				MHD_destroy_post_processor(con_info->postprocessor);
				con_info->answerstring.clear();            
			}

			delete con_info;
			*con_cls = NULL;
		}    

		FUNCTION PRIVATE DEFINITION static int iterate_post 
			(	
				void *cls, 
				enum MHD_ValueKind kind, 
				const char *key, 
				const char *filename, 
				const char *content_type,
				const char *transfer_encoding, 
				const char *data,  
				uint64_t off, size_t size 
			)
		{
	        
			Connection_info_struct_t* con_info = reinterpret_cast<Connection_info_struct_t*>(cls);
			static size_t num_of_param = 0;
			num_of_param++;
			#ifdef DEBUG
				std::printf( "POST REQUEST [ %s, %s ]\n", key, data);
			#endif
			
			return (0) ? MHD_NO : MHD_YES;		
		}    
	    
		FUNCTION PRIVATE DEFINITION static void page_dispatcher
			(
				const Str_t& page_request, 
				Str_t& page_return, 
				Str_t& mimetype
			)
		{				

		}    

	}
}


