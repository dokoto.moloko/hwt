#include "label.hpp"

namespace hwt
{
	CONSTRUCTOR PUBLIC DEFINITION label::label
		(
			Str_t labeltx, 
			uInt_t width, 
			uInt_t height 
		) 
		
	:	widget(labeltx, width, height, LABEL )

	{
	}

	DESTRUCTOR PUBLIC DEFINITION label::~label
		(
			void
		)
	{
	}

	METHOD PUBLIC DEFINITION Str_t label::toHtml
		(
			void
		)
	{		
		return this->Labeltx;					
	}
}


