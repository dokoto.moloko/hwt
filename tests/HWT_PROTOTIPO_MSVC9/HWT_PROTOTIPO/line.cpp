#include "line.hpp"

namespace hwt
{
    CONSTRUCTOR PUBLIC DEFINITION line::line
		(
			void
		) 
    { 
    }
    
    DESTRUCTOR PUBLIC DEFINITION line::~line
		(
			void
		) 
    { 
		#ifdef DEBUG
			std::cout << "[line::~line] It release : " << vButton.size() << " pointers to Button class." << std::endl;
		#endif
		DeallocateWidgets(vButton);
		#ifdef DEBUG
			std::cout << "[line::~line] It release : " << vLabel.size() << " pointers to Label class." << std::endl;
		#endif
		DeallocateWidgets(vLabel);
		#ifdef DEBUG
			std::cout << "[line::~line] It release : " << vInput.size() << " pointers to Input class." << std::endl;
		#endif
		DeallocateWidgets(vInput);      
    }

    METHOD PUBLIC DEFINITION button& line::ModButton
		(
			uInt_t index
		) 
	{ 
		if (index >= vButton.size()) throw emsg("[ERROR][body::Button] Index out of range.");
		return *vButton[index]; 
	}

	METHOD PUBLIC DEFINITION input& line::ModInput
		(
			uInt_t index
		) 
	{ 
		if (index >= vInput.size()) throw emsg("[ERROR][body::Input] Index out of range.");
		return *vInput[index]; 
	}

	METHOD PUBLIC DEFINITION label& line::ModLabel
		(
			uInt_t index
		) 
	{ 
		if (index >= vLabel.size()) throw emsg("[ERROR][body::Label] Index out of range.");
		return *vLabel[index]; 
	}

	METHOD PUBLIC DEFINITION const button& line::ModButton
		(
			uInt_t index
		) const
	{ 
		if (index >= vButton.size()) throw emsg("[ERROR][body::Button const] Index out of range.");
		return *vButton[index]; 
	}

	METHOD PUBLIC DEFINITION const input& line::ModInput
		(
			uInt_t index
		) const
	{ 
		if (index >= vInput.size()) throw emsg("[ERROR][body::Input const] Index out of range.");
		return *vInput[index]; 
	}

	METHOD PUBLIC DEFINITION const label& line::ModLabel
		(
			uInt_t index
		) const
	{ 
		if (index >= vLabel.size()) throw emsg("[ERROR][body::Label const] Index out of range.");
		return *vLabel[index]; 
	}

	METHOD PUBLIC DEFINITION void line::AddButton
		(
			Str_t labeltx, 
			uInt_t width, 
			uInt_t height 
		) 
	{ 
		button* b = NULL;
		b = new (VAR2STR(button)) button(labeltx, width, height);
		if (b != NULL)
		{
			vButton.push_back( b ); 
			Widgets[b->Uuid] = b;
		}
		else
			throw emsg("[ERROR][line::AddButton] Allocate Memory error.");
	}

	METHOD PUBLIC DEFINITION void line::AddInput
		(
			Str_t labeltx, 
			uInt_t width, 
			uInt_t height
		) 
	{ 
		vInput.push_back( new (VAR2STR(input)) input(labeltx, width, height) ); 
	}

	METHOD PUBLIC DEFINITION void line::AddLabel
		(
			Str_t labeltx, 
			uInt_t width, 
			uInt_t height
		) 
	{ 
		vLabel.push_back( new (VAR2STR(label)) label(labeltx, width, height) ); 
	}
    
}
