#include "webTags.hpp"

namespace hwt
{
namespace chtml
{


const Char_t* html_open = "<html>\n";		
const Char_t* html_close = "</html>\n";
const Char_t* td_id_open= "<td id =\"%s\">\n";
const Char_t* div_close = "</div>\n";
const Char_t* table_close = "</table>\n";
const Char_t* td_open = "<td>\n";
const Char_t* tr_open = "<tr>\n";
const Char_t* tr_close = "</tr>\n";
const Char_t* td_close = "</td>\n";
const Char_t* head_open = "<head>\n";
const Char_t* head_close = "</head>\n";
const Char_t* body_open = "<body>\n";
const Char_t* body_close = "</body>\n";



const Char_t* error_page = "\
<html>\n\
	<head>\n\
		<title>webserver derived class</title>\n\
		<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\n\
	</head>\n\
	<body  bgcolor=\"White\">\n\
		<h4>Upps! algo ha salido mal, revisa los logs.</h4><br><br>\n\
		<h1>Salud ;)</h1>\n\
	</body>\n\
</html>\n\
";

/*
 * <title>HWT PROTO - SESSION TITLE o APPLICATION TITLE</title>
 */
extern const Char_t* title = "<title>%s</title>\n";

/*
 * <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd"
 */

const Char_t* header_html = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01//EN\" \"http://www.w3.org/TR/html4/strict.dtd\"\n";

/*
 * <meta meta name="App-server" content="text/html;" http-equiv="content-type" charset="utf-8" />
 */
const Char_t* meta_header_html = "<meta meta name=\"App-server\"  content=\"text/html;\" http-equiv=\"content-type\" charset=\"utf-8\" />\n";

/*
 * <link type="text/css" href="../../src/js/jquery-ui-1.9.0.custom/jquery-ui-1.9.0.custom/css/smoothness/jquery-ui-1.9.0.custom.css" rel="Stylesheet" /> 
 * <link type="text/css" href="css/document.css" rel="Stylesheet" /> 	
 * <script type="text/javascript" src="../../src/js/jquery-ui-1.9.0.custom/jquery-ui-1.9.0.custom/js/jquery-1.8.2.js"></script>
 * <script type="text/javascript" src="../../src/js/jquery-ui-1.9.0.custom/jquery-ui-1.9.0.custom/js/jquery-ui-1.9.0.custom.min.js"></script>
 * <script type="text/javascript" src="js/utils.js"></script>
 */
const Char_t* dependencies = "\
<link type=\"text/css\" href=\"../../src/js/jquery-ui-1.9.0.custom/jquery-ui-1.9.0.custom/css/smoothness/jquery-ui-1.9.0.custom.css\" rel=\"Stylesheet\" />\n\
<link type=\"text/css\" href=\"css/document.css\" rel=\"Stylesheet\" />\n\
<script type=\"text/javascript\" src=\"../../src/js/jquery-ui-1.9.0.custom/jquery-ui-1.9.0.custom/js/jquery-1.8.2.js\"></script>\n\
<script type=\"text/javascript\" src=\"../../src/js/jquery-ui-1.9.0.custom/jquery-ui-1.9.0.custom/js/jquery-ui-1.9.0.custom.min.js\"></script>\n\
<script type=\"text/javascript\" src=\"js/utils.js\"></script>\n\
";

/*
 * <div id=\"dialog_window_minimized_container\"></div>
 */
const Char_t* container_minimiz = "<div id=\"dialog_window_minimized_container\"></div>\n";

/*
 * <div id="document_id_2" class="dialog_window" title="Title of Document 2">
 */
const Char_t* doc_div ="<div id=\"%d\" class=\"%s\" title=\"%s\">\n";

/*
 * <h4>DOCUMENT DESCRIPTION 2</h4>
 */
const Char_t* doc_h4="<h4>%s</h4>\n";

/*
 * <table class="dialog_form">
 */
const Char_t* table_open = "<table class=\"dialog_form\">\n";

/*
 * <input type="text" id="new_window_title2" />
 */
const Char_t* input = "<input type=\"text\" id=\"%d\" />\n";

/*
 * <button id="acction2">acction</button>
 */
const Char_t* button = "<button id=\"%d\">%s</button>\n";

/*
 * <input type="checkbox" id="sino2" /><label for="sino">SI/NO</label> 
 */
const Char_t* checkbox = "<input type=\"checkbox\" id=\"%d\" /><label for=\"%d\">%s</label>\n";

/*
 * <title>HWT PROTO - SESSION TITLE o APPLICATION TITLE</title> 
 */
const Char_t* session_title = "<title>%s</title>";
}

namespace cjs
{
}

namespace ccss
{
}
}


