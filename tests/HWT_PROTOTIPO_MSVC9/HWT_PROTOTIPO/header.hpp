#ifndef _HEADER_HPP
#define _HEADER_HPP

#include "types.hpp"
#include "webTags.hpp"
#include "utils.hpp"

namespace hwt
{
	/*	
	USAR CONSTANTES ESTATICAS PARA OCUPAR LA MEMORIA UNA SOLA VEZ Y RESERVAR POR CADA INTANCIA DEL OBJ
	*/

	class header
	{
		private:
			COPY_CONSTRUCTOR PRIVATE DEFINITION header
				(
					const header& h
				) : Title(h.Title) {}

			OPERATOR PRIVATE DEFINITION header& operator=
				(
					const header&
				) { return *this; }

		public:
			Str_t Title;

		public:
			CONSTRUCTOR PUBLIC DEFINITION header
				(
					Str_t title = "New Web C++ Application"
				) :
					Title(title) {}

			DESTRUCTOR PUBLIC DEFINITION ~header
				(
				) {}

			METHOD PUBLIC DECLARATION Str_t toHtml
				(
					void
				);

			METHOD PUBLIC DECLARATION Str_t toHtmlWithHeader
				(
					void
				);
				
	};
}

#endif //_HEADER_HPP

