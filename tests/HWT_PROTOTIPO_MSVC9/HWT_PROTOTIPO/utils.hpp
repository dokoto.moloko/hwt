#ifndef _UTILS_HPP
#define _UTILS_HPP

#include "types.hpp"


namespace hwt
{	
namespace utl
{		
	FUNCTION PUBLIC DECLARATION value_t ConvToStr
		(
			int value
		);

	FUNCTION PUBLIC DECLARATION value_t ConvToStr
		(
			long value
		);

	FUNCTION PUBLIC DECLARATION value_t ConvToStr
		(
			double value
		);

	FUNCTION PUBLIC DECLARATION value_t ConvToStr
		(
			float value
		);

	FUNCTION PUBLIC DECLARATION value_t ConvToStr
		(
			char value
		);

	FUNCTION PUBLIC DECLARATION value_t ConvToStr
		(
			Str_t value
		);	

	FUNCTION PUBLIC DECLARATION bool Split
		(
			const Char_t& sep, 
			const Char_t* raw, 
			std::vector<Str_t> split_values
		);

	FUNCTION PUBLIC DECLARATION Str_t CurrentDir
		(
			void
		);

	FUNCTION PUBLIC DECLARATION size_t NumOfDirLevels
		(
			void
		);

    FUNCTION PUBLIC DECLARATION size_t NumOfDirLevels
		(
			const Char_t* path
		);

	FUNCTION PUBLIC DECLARATION Char_t GetDirSep
		(
			void
		);

	FUNCTION PUBLIC DECLARATION void DoFormatting
		(
			Str_t& sF, 
			const Char_t* sformat, 
			va_list marker
		);

	FUNCTION PUBLIC DECLARATION void Stringf
		(
			Str_t& stgt, 
			const Char_t* sformat, 
			... 
		);

	FUNCTION PUBLIC DECLARATION Str_t Stringf
		(
			const Char_t* sformat, 
			... 
		);

	FUNCTION PUBLIC DECLARATION bool ReadFullTxFile
		(
			const Str_t& path, 
			Str_t& file
		);

	FUNCTION PUBLIC DECLARATION bool ReadFullBinFile
		(
			const Str_t& path, 
			Str_t& file
		);

	FUNCTION PUBLIC DECLARATION bool ReadFullTxFile
		(
			const Char_t* path, 
			Str_t& file
		);

	FUNCTION PUBLIC DECLARATION bool ReadFullBinFile
		(
			const Char_t* path, 
			Str_t& file
		);

	/* DIRLEVEL(CInt_t& Level, Str_t& Dir)
	* Si tenemos el Directorio C:\uno\dos\tres\cuatro y queremos obtener la ruta hasta C:\uno\dos
	* le pasamos (N�NIVELES - 2), es decir, dos niveles arriba
	* Ej:
	* Str_t& Dir;
	* DirLevel(NumOfDirLevels() - 2, Dir);
	*/
	FUNCTION PUBLIC DECLARATION Str_t DirLevel
		(
			const uInt_t& Level, 
			const Str_t& path
		);		

	FUNCTION PUBLIC DECLARATION Str_t DirLevel
		(
			const uInt_t& Level
		);

	FUNCTION PUBLIC DECLARATION Str_t FixWinPath
		(
			const Str_t& path
		);	

	FUNCTION PUBLIC DECLARATION Str_t ExtractEXTENSION
		(
			const Str_t& path
		);

	FUNCTION PUBLIC DECLARATION bool ListFiles
		(
			const Char_t* path, 
			const Char_t tipo, 
			Vfile_t& files
		);		

#ifdef _MSC_VER

	FUNCTION PUBLIC DECLARATION bool ListFilesWIN
		(
			const Char_t* path, 
			Vfile_t& files
		);

#else

    FUNCTION PUBLIC DECLARATION bool ListFilesUNIX
		(
			const Char_t* path, 
			Vfile_t& files
		);

#endif	        


}
}

#endif //_UTILS_HPP
