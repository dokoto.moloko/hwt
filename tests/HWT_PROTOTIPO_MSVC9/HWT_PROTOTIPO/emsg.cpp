#include "emsg.hpp"

namespace hwt
{
	CONSTRUCTOR PUBLIC DEFINITION emsg::emsg
		(
			const std::string& msg
		) : 
			std::runtime_error(msg)

		{
		}
}

