#ifndef _INPUT_HPP
#define _INPUT_HPP

#include "types.hpp"
#include "widget.hpp"
#include "utils.hpp"
#include "webTags.hpp"

namespace hwt
{
	class input : public widget
	{
		private:
			value_t Value;					

		private:
			COPY_CONSTRUCTOR PRIVATE DEFINITION input
				(
					const input& i 
				)  :	widget(	i.Labeltx, i.Width, i.Height, INPUT ) {}

			OPERATOR PRIVATE DEFINITION input& operator=
				(
					const input&
				) { return *this; }

			METHOD PRIVATE DECLARATION types_t checkType
				(
					void
				);
		
		public:
			CONSTRUCTOR PUBLIC DECLARATION input
				(	
					Str_t labeltx = "", 
					uInt_t width = 200, 
					uInt_t height = 100
				);

			DESTRUCTOR PUBLIC DECLARATION ~input
				(
				);

			METHOD PUBLIC DECLARATION Str_t toHtml
				(
					void
				);

			METHOD PUBLIC DEFINITION void SetValue
				(
					int v
				) { Value = hwt::utl::ConvToStr(v); }

			METHOD PUBLIC DEFINITION void SetValue
				(
					char v
				) { Value = hwt::utl::ConvToStr(v); }

			METHOD PUBLIC DEFINITION void SetValue
				(
					Str_t v
				) { Value = hwt::utl::ConvToStr(v); }

			METHOD PUBLIC DEFINITION void SetValue
				(
					float v
				) { Value = hwt::utl::ConvToStr(FLOAT(v)); }

			METHOD PUBLIC DEFINITION void SetValue
				(
					double v
				) { Value = hwt::utl::ConvToStr(v); }

			METHOD PUBLIC DEFINITION void SetValue
				(
					long v
				) { Value = hwt::utl::ConvToStr(LONG(v)); }
			
	};
}

#endif //_INPUT_HPP

