#include "deploycl.hpp"

namespace hwt
{
	CONSTRUCTOR PUBLIC DECLARATION deploy::deploy
		(
			session* ss
		)
		: Session(ss)
	{
		
		hwt::deployapp::Init(Session);
	}

	DESTRUCTOR PUBLIC DECLARATION deploy::~deploy
		(
			void
		)
	{
		hwt::deployapp::Destroy();
	}
}

