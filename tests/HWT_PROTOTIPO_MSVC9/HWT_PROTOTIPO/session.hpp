#ifndef _SESSION_HPP
#define _SESSION_HPP

#include "types.hpp"
#include "document.hpp"
#include "header.hpp"
#include "webTags.hpp"
#include "deploycl.hpp"

namespace hwt
{
	class session
	{
		private:
			COPY_CONSTRUCTOR PRIVATE DEFINITION session
				(
					const session& s
				) : Uuid(reinterpret_cast<uInt_t>(&s)),
					Port(s.Port),
					TimeOutSeg(s.TimeOutSeg) {}

			OPERATOR PRIVATE DEFINITION session& operator=
				(
					const session&
				) { return *this; }

		public:

			FRIEND FUNCTION PUBLIC DECLARATION friend void Init
				(
					session& ss
				);

		public:
			header Header;
			const uInt_t Uuid;
			const uInt_t Port;
			const uInt_t TimeOutSeg;

		private:
			std::vector<document*> vDocument;

		public:
			CONSTRUCTOR PUBLIC DEFINITION session
				(
					uInt_t port = 4444, 
					uInt_t timeoutseg = 10 
				):	Uuid(reinterpret_cast<uInt_t>(this)),
					Port(port),
					TimeOutSeg(timeoutseg) {}		

			DESTRUCTOR PUBLIC DECLARATION ~session
				(
					void
				);

		public: 			
			METHOD PUBLIC DECLARATION void AddDocument
				(
					Str_t title = "", 
					Str_t description = "", 
					uInt_t width = 600, 
					uInt_t height = 480, 
					bool visible = true
				);

			METHOD PUBLIC DECLARATION document& Document
				(
					uInt_t index
				);

			METHOD PUBLIC DECLARATION const document& Document
				(
					uInt_t index
				) const;

			METHOD PUBLIC DECLARATION uInt_t NumOfDocuments
				(
					void
				) const;

			METHOD PUBLIC DECLARATION void DoDeploy
				(
					void
				);

			METHOD PUBLIC DECLARATION Str_t toHtml
				(
					void
				);
	};
}

#endif //_SESSION_HPP

