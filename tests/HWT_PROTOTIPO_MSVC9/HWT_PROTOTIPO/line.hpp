#ifndef _LINE_HPP
#define	_LINE_HPP

#include "types.hpp"
#include "button.hpp"
#include "input.hpp"
#include "label.hpp"

namespace hwt
{
	class document;
    class line 
    {
	private:
        COPY_CONSTRUCTOR PRIVATE DEFINITION line
			(
				const line&
			) {}

		OPERATOR PRIVATE DEFINITION line& operator=
			(
				const line&
			) { return *this; }        
                
	private:
		template<class vWidget_GT> 
		METHOD PRIVATE DEFINITION void DeallocateWidgets
			(
				vWidget_GT vWidget
			)
			{
				for(typename vWidget_GT::iterator it = vWidget.begin(); it != vWidget.end(); it++) if (*it) delete *it;
			}                
       	
	private:
		std::map<uInt_t, hwt::widget*>				Widgets;
		std::vector<button*>						vButton;
		std::vector<input*>							vInput;
		std::vector<label*>							vLabel;                
                
    public:
		friend class document;
                
	public:		
		CONSTRUCTOR PUBLIC DECLARATION line
			(
				void
			); 

		DESTRUCTOR PUBLIC DECLARATION ~line
			(
				void
			); 

        METHOD PUBLIC DECLARATION void AddButton
			(
				Str_t labeltx = "", 
				uInt_t width = 200, 
				uInt_t height = 100 
			);

        METHOD PUBLIC DECLARATION button& ModButton
			(
				uInt_t index
			);

		METHOD PUBLIC DECLARATION const button& ModButton
			(
				uInt_t index
			) const;                

        METHOD PUBLIC DECLARATION void DelButton
			(
				uInt_t index
			);
		
		METHOD PUBLIC DECLARATION void AddInput
			(
				Str_t labeltx = "", 
				uInt_t width = 200, 
				uInt_t height = 100
			);

        METHOD PUBLIC DECLARATION input& ModInput
			(
				uInt_t index
			);

		METHOD PUBLIC DECLARATION const input& ModInput
			(
				uInt_t index
			) const;

        METHOD PUBLIC DECLARATION void DelInput
			(
				uInt_t index
			);
                
		METHOD PUBLIC DECLARATION void AddLabel
			(
				Str_t labeltx = "", 
				uInt_t width = 200, 
				uInt_t height = 100 
			);			

		METHOD PUBLIC DECLARATION label& ModLabel
			(
				uInt_t index
			);

		METHOD PUBLIC DECLARATION const label& ModLabel
			(
				uInt_t index
			) const;                                                

        METHOD PUBLIC DECLARATION void DelLabel
			(
				uInt_t index
			);

    };
}
#endif	// _LINE_HPP

