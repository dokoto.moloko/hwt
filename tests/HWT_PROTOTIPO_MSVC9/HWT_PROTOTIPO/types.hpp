#ifndef _TYPES_HPP
#define _TYPES_HPP

#ifdef _MSC_VER
	#pragma warning(disable : 4995)
	#pragma warning(disable : 4996)
	#define MHD_PLATFORM_H    
    #include <winsock2.h> // socket related definitions
    #include <ws2tcpip.h> // socket related definitions
	#define _CRT_SECURE_NO_WARNINGS	
	#include "atlbase.h"
	#include "atlstr.h"
	#include "comutil.h"
	#include "Strsafe.h"
    #include <direct.h>
    
	typedef SSIZE_T ssize_t;
	typedef __int16 int16_t;
	typedef unsigned __int16 uint16_t;
	typedef __int32 int32_t;
	typedef unsigned __int32 uint32_t;
	typedef __int64 int64_t;
	typedef unsigned __int64 uint64_t;	

	#define GetCurrentDir _getcwd	
#else
	#include <stdint.h>
    #include <sys/types.h>
    #include <sys/select.h>
    #include <sys/socket.h>
    #include <sys/stat.h> 
	#include <unistd.h>
	#include <dirent.h>
	#define GetCurrentDir getcwd
#endif

#include <cstdio>
#include <cstring>
#include <cstdarg>

#include <string>
#include <map>
#include <vector>
#include <sstream>
#include <iostream>
#include <fstream>
#include <iomanip>
#include "memReport.hpp"
#include "emsg.hpp"
#include "layout_defs.hpp"


namespace hwt
{
	/*-------------------------------------------------------------------------------------------------
	 * MACROS
	 *------------------------------------------------------------------------------------------------*/
	#define LONG(value) static_cast<long>(value)
	#define FLOAT(value) static_cast<float>(value)
	// CONST CHAR* TO VOID*
	#define CCP_TO_VP(var) static_cast<void*>(const_cast<char*>(var))
	// VOID* TO CONST CHAR*
	#define VP_TO_CCP(var) static_cast<char*>(var)

	/* GCC C-LANG MACROS  */
	#if defined(__i386__)
		typedef uint32_t uInt_t;
		typedef int32_t	Int_t;
	#elif defined(__x86_64__)
		typedef uint64_t uInt_t;
		typedef int64_t	Int_t;

	/* MSVC++ MACROS  */
	#elif defined(_M_IX86)
		typedef uint32_t uInt_t;
		typedef int32_t	Int_t;
	#elif defined(_M_X64)
		typedef uint64_t uInt_t;
		typedef int65_t	Int_t;

	/* ARQUITECTURA DESCONOCIDA  */
	#else
		# error Unsupported architecture
	#endif	


	/*-------------------------------------------------------------------------------------------------
	 * GLOBAL TYPE DEF
	 *------------------------------------------------------------------------------------------------*/
	typedef std::string			Str_t;    
	typedef char				Char_t;
	typedef std::stringstream	Buffer_t;
	
	typedef struct
	{
		Str_t           value;
		int				type;
    
	} value_t;
	
	typedef struct											
	{
		Str_t			name;
        Str_t			ruta;
		Str_t			tipo;

	} file_t;

	typedef std::vector<file_t>     Vfile_t;        
	typedef std::map<Str_t, value_t> params_t;

	class session; typedef void (*ptrParams_t)(const params_t&, session& ss);

	/*-------------------------------------------------------------------------------------------------
	 * CONST VARS
	 *------------------------------------------------------------------------------------------------*/

	const char types[][10] = {"INT", "LONG", "DOUBLE", "FLOAT", "CHAR", "STRING"};
	enum types_t			 {INT, LONG, DOUBLE, FLOAT, CHAR, STRING};

	const char events[][20] = {"ONCLICK", "ONMOUSEOVER"};
	enum events_t			  {ONCLICK, ONMOUSEOVER};     

	const char objets[][20] = {"BUTTON", "LABEL", "INPUT"};
	enum objets_t			  {BUTTON, LABEL, INPUT};     
}

#endif // _TYPES_HPP

