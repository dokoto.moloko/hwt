#include "header.hpp"

namespace hwt
{
	METHOD PUBLIC DEFINITION Str_t header::toHtml
		(
			void
		)
	{
		Buffer_t buf;
		buf << chtml::head_open;
		
		buf << utl::Stringf(chtml::title, this->Title);
		buf << chtml::meta_header_html;
		buf << chtml::dependencies;
		buf << chtml::head_close;

		return buf.str();
	}

	METHOD PUBLIC DEFINITION Str_t header::toHtmlWithHeader
		(
			void
		)
	{
		Buffer_t buf;
		buf << chtml::head_open;
		buf << toHtml();
		buf << chtml::head_close;
		
		return buf.str();
	}
}