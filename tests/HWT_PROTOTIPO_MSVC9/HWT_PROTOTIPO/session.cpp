#include "session.hpp"

namespace hwt
{

	DESTRUCTOR PUBLIC DEFINITION session::~session
		(
			void
		)
	{
		#ifdef DEBUG
			std::cout << "[session::~session] It release : " << vDocument.size() << " pointers to Document class." << std::endl;
		#endif
		for(std::vector<document*>::iterator it = vDocument.begin(); it != vDocument.end(); it++) if (*it) delete *it;
		#ifdef DEBUG
			std::cout << "[session::~session] It release DeployEngine pointers to Document class." << std::endl;
		#endif
	}

	METHOD PUBLIC DEFINITION document& session::Document
		(
			uInt_t index
		) 
	{ 
		if (index >= vDocument.size()) throw emsg("[ERROR][session::Document] Index out of range.");
		return *vDocument[index]; 
	}

	METHOD PUBLIC DEFINITION const document& session::Document
		(
			uInt_t index
		) const
	{
		if (index >= vDocument.size()) throw emsg("[ERROR][session::Document] Index out of range.");
		return *vDocument[index];
	}

	METHOD PUBLIC DEFINITION void session::AddDocument
		(
			Str_t title, 
			Str_t description, 
			uInt_t width, 
			uInt_t height, 
			bool visible
		) 
	{ 
		vDocument.push_back(new (VAR2STR(document)) document(title, description, width, height, visible)); 
	}

	METHOD PUBLIC DEFINITION uInt_t session::NumOfDocuments
		(
			void
		) const
	{
		return (uInt_t)vDocument.size();
	}

	METHOD PUBLIC DEFINITION void session::DoDeploy
		(
			void
		)
	{
		deploy dp(this);	
	}

	METHOD PUBLIC DEFINITION Str_t session::toHtml
		(
			void
		)
	{
		Buffer_t buf;
		buf << chtml::html_open;
		buf << Header.toHtmlWithHeader();
		buf << chtml::body_open;
		for(std::vector<document*>::iterator DocIt = vDocument.begin(); DocIt != vDocument.end(); DocIt++)
		{
			buf << (*DocIt)->toHtmlWithDiv();
		}
		buf << chtml::body_close;
		buf << chtml::html_close;

		return buf.str();
	}

}

