#ifndef _DEPLOYAPP_HPP
#define _DEPLOYAPP_HPP

#include "types.hpp"
#include "session.hpp"

extern "C"
{
	#include "microhttpd.h"
}

namespace hwt
{
	namespace deployapp
	{
		FUNCTION DECLARATION void Init
			(
				session* ss
			);

		FUNCTION DECLARATION void Destroy
			(
				void
			);
		
		FUNCTION DECLARATION void Run
			(
				void
			);		

		FUNCTION DECLARATION void Publish
			(
				void
			);
	}

}


#endif //_DEPLOYAPP_HPP

