#ifndef _DOCUMENT_HPP
#define _DOCUMENT_HPP

#include "types.hpp"
#include "body.hpp"
#include "webTags.hpp"

namespace hwt
{
	class document
	{
		private:
			COPY_CONSTRUCTOR PRIVATE DEFINITION document
				(
					const document& d
				) :
					Uuid(reinterpret_cast<uInt_t>(&d)),
					Title(d.Title),
					Description(d.Description),
					Width(d.Width),
					Height(d.Height),
					Priority(d.Priority),
					Visible(d.Visible)
					{}

			OPERATOR PRIVATE DEFINITION document& operator=
				(
					const document&
				) { return *this; }

			METHOD PRIVATE DECLARATION Str_t wToHtml
				(
					widget* w, 
					const objets_t objType
				);

		private:
			static uInt_t count;
			const uInt_t Uuid;

		public:
			Str_t Title, Description;
			uInt_t Width, Height, Priority;
			bool Visible;
		
		public:			
			body Body;

		public:
			CONSTRUCTOR PUBLIC DEFINITION document
				(
					Str_t title = "", 
					Str_t description = "", 
					uInt_t width = 600, 
					uInt_t height = 480, 
					bool visible = true
				) :
					Uuid(reinterpret_cast<uInt_t>(this)),
					Title(title),
					Description(description),
					Width(width),
					Height(height),
					Priority(count++),
					Visible(visible){}
			
			DESTRUCTOR PUBLIC DECLARATION ~document
				(
				) {}

			METHOD PUBLIC DECLARATION Str_t toHtml
				(
					void
				);

			METHOD PUBLIC DECLARATION Str_t toHtmlWithDiv
				(
					void
				);


	};
}

#endif //_DOCUMENT_HPP

