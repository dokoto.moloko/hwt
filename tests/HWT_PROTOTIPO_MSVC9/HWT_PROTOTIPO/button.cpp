#include "button.hpp"

namespace hwt
{
	CONSTRUCTOR PUBLIC DEFINITION button::button
		(	
			Str_t labeltx, 
			uInt_t width, 
			uInt_t height 
		) 
		
	:	widget(labeltx, width, height, BUTTON )

	{
	}

	DESTRUCTOR PUBLIC DEFINITION button::~button
		(
			void
		)
	{
	}

	METHOD PUBLIC DEFINITION Str_t button::toHtml
		(
			void
		)
	{
		Str_t text;
		hwt::utl::Stringf(text, hwt::chtml::button, this->Uuid, this->Labeltx);			
		return text;
	}

}

