#ifndef _MEMREPORT_HPP
#define _MEMREPORT_HPP

#include <assert.h>
#include "container.hpp"

#ifdef DEBUG

#define VAR2STR(v) #v
static Container memCont;

inline void ReportMemStatus()
{
	memCont.ReportAllocStatus();
}

inline void * operator new (size_t size, const char* VarName, const char* file, size_t line)
{
	assert(size > 0);
	void* ptr = malloc(size);
	ptrdiff_t dir = (ptrdiff_t)ptr;
	Traze_t wa = {false, VarName, size, file+(std::string(file).find_last_of("/\\")+1), line};
	memCont.Add(dir, wa);
	return ptr;
}

inline void * operator new[](size_t size, const char* VarName, const char* file, size_t line)
{
	assert(size > 0);
	void* ptr = malloc(size);
	ptrdiff_t dir = (ptrdiff_t)ptr;
	Traze_t wa = {false, VarName, size, file+(std::string(file).find_last_of("/\\")+1), line};
	memCont.Add(dir, wa);
	return ptr;
}

#define new(v) new(v, __FILE__, __LINE__)

inline void operator delete (void * ptr)  throw ()
{

	ptrdiff_t dir = (ptrdiff_t)ptr;
	memCont.SetRelease(dir, true);
	assert(ptr != NULL);
	free(ptr);		
	ptr = NULL;
}

inline void operator delete[](void * ptr)  throw ()
{

	ptrdiff_t dir = (ptrdiff_t)ptr;
	memCont.SetRelease(dir, true);
	assert(ptr != NULL);
	free(ptr);		
	ptr = NULL;
}

#else 
	#define new(v) new
#endif // DEBUG	
#endif //_MEMREPORT_HPP

