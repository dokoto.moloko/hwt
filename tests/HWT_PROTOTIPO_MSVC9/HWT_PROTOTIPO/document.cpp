#include "document.hpp"

namespace hwt
{
	uInt_t document::count = 0;

	METHOD PRIVATE DEFINITION Str_t document::wToHtml
		(
			widget* w, 
			const objets_t objType
		)
	{
		switch (objType)
		{
			case hwt::BUTTON:
				return dynamic_cast<button*>(w)->toHtml();
				break;
			case hwt::INPUT:
				return dynamic_cast<input*>(w)->toHtml();
				break;
			case hwt::LABEL:
				return dynamic_cast<label*>(w)->toHtml();
				break;
			default:
				return "";
				break;
		}	
	}

	METHOD PUBLIC DEFINITION Str_t document::toHtml
		(
			void
		)
	{
		Buffer_t buf;
		buf << utl::Stringf(chtml::doc_h4, this->Description);
		buf << chtml::table_open;
		for(std::vector<line*>::iterator itLine = this->Body.vLine.begin(); itLine != this->Body.vLine.end(); itLine++)
		{		
			buf << chtml::tr_open;
			for (std::map<uInt_t, hwt::widget*>::iterator itWidget = (*itLine)->Widgets.begin(); itWidget != (*itLine)->Widgets.end(); itWidget++)
			{
				buf << chtml::td_open;
				buf << wToHtml(itWidget->second, itWidget->second->Type);
				buf << chtml::td_close;
			}
			buf << chtml::tr_close;
		}
		buf << chtml::table_close;

		return buf.str();
	}
	METHOD PUBLIC DEFINITION Str_t document::toHtmlWithDiv(void)
	{
		Buffer_t buf;
		buf << utl::Stringf(chtml::doc_div, this->Uuid, "dialog_window", this->Title);
		buf << toHtml();
		buf << chtml::div_close;

		return buf.str();
	}


}

