#ifndef _WIDGET_HPP
#define _WIDGET_HPP

#include "types.hpp"

namespace hwt
{
	class session;
	class widget
	{
		private:
			COPY_CONSTRUCTOR PRIVATE DEFINITION widget
				(
					const widget& w
				) :	Uuid(reinterpret_cast<uInt_t>(&w)),
					Labeltx(w.Labeltx),
					Width(w.Width),
					Height(w.Height),
					Type(w.Type) {}

			OPERATOR PRIVATE DEFINITION widget& operator=
				(
					const widget&
				) { return *this; }

		private: 
			METHOD PRIVATE DEFINITION void executeEvent
				(
					const Char_t* EventName, 
					const params_t& params, 
					session& ss
				)  
				{ 
        			const ptrParams_t ptrF = EventPool[EventName];
					(*ptrF)(params, ss); 
				}

			METHOD PRIVATE DEFINITION void executeEvent
				(
					const Char_t* EventName, 
					const params_t& params, 
					session& ss
				) const 
				{ 																					
					(*(EventPool.find(EventName)->second)) (params, ss);
				}

		public:
			const uInt_t Uuid;
			Str_t Labeltx;
			uInt_t Width, Height;		
			const objets_t Type;
			friend class session;
			

		private:
			std::map<Str_t, ptrParams_t>		EventPool;
				

		public: // Constructor

			CONSTRUCTOR PUBLIC DEFINITION widget
				(
					Str_t labeltx, 
					uInt_t width, 
					uInt_t height, 
					objets_t type
				) :	
					Uuid(reinterpret_cast<uInt_t>(this)),
					Labeltx(labeltx),
					Width(width),
					Height(height),
					Type(type){}												
														 
			DESTRUCTOR PUBLIC DEFINITION virtual ~widget
				(
					void
				) {}

		public: // Methods

			VIRTUAL METHOD PUBLIC DEFINITION Str_t toHtml
				(
					void
				)=0;
		
			VIRTUAL METHOD PUBLIC DEFINITION void AddEvent
				(
					const ptrParams_t ptrF, 
					const char* eventName
				) { EventPool[eventName] = ptrF; }
	};

}

#endif //_WIDGET_HPP

