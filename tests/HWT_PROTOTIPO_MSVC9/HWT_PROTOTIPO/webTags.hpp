#ifndef _WEBTAGS_HPP
#define _WEBTAGS_HPP

#include "types.hpp"

namespace hwt
{
	namespace chtml
	{
		extern const Char_t* session_title;
		extern const Char_t* dependencies;
		extern const Char_t* header_html;
		extern const Char_t* meta_header_html;
		extern const Char_t* container_minimiz;
		extern const Char_t* doc_div;
		extern const Char_t* doc_h4;
		extern const Char_t* input;
		extern const Char_t* button;
		extern const Char_t* checkbox;

		extern const Char_t* title;				
		extern const Char_t* html_open;		
		extern const Char_t* html_close;
		extern const Char_t* td_id_open;		
		extern const Char_t* td_open;
		extern const Char_t* tr_open;
		extern const Char_t* head_open;
		extern const Char_t* head_close;
		extern const Char_t* table_open;
		extern const Char_t* div_close;
		extern const Char_t* table_close;
		extern const Char_t* tr_close;
		extern const Char_t* td_close;
		extern const Char_t* body_open;
		extern const Char_t* body_close;

		extern const Char_t* error_page;
	}

	namespace cjs
	{
	}

	namespace ccss
	{
	}

}

#endif // _WEBTAGS_HPP


