#include "input.hpp"

namespace hwt
{
	CONSTRUCTOR PUBLIC DEFINITION input::input
		( 
			Str_t labeltx, 
			uInt_t width, 
			uInt_t height 
		)		
	:	widget(labeltx, width, height, INPUT )
	{		
	}

	DESTRUCTOR PUBLIC DEFINITION input::~input
		(
			void
		)
	{
	}

	METHOD PUBLIC DEFINITION Str_t input::toHtml
		(
			void
		)
	{
		Str_t text;
		hwt::utl::Stringf(text, hwt::chtml::input, this->Uuid);			
		return text;
	}

}

