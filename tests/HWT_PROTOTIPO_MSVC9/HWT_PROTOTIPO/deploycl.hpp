#ifndef _DEPLOYCL_HPP
#define _DEPLOYCL_HPP

#include "types.hpp"
#include "deployapp.hpp"
#include "session.hpp"

namespace hwt
{

	class deploy
	{
		private:
			session*	Session;

		private:
			COPY_CONSTRUCTOR PRIVATE DEFINITION deploy
				(
					const deploy& d
				)  : Session(d.Session) {}

			OPERATOR PRIVATE DEFINITION deploy& operator=
				(
					const deploy&
				){ return *this; }
		
		public:
			CONSTRUCTOR PUBLIC DECLARATION deploy
				(
					session* ss
				);

			DESTRUCTOR PUBLIC DECLARATION ~deploy
				(
					void
				);


	};
}

#endif // _DEPLOYCL_HPP

