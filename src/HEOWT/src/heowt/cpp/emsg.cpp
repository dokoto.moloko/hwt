// SOURCE CHARSET ENCODING FOR WINDOWS   : US-ASCII
// SOURCE CHARSET ENCODING FOR LINUX, OS : ASCII

#include "emsg.hpp"
#include <iostream> 

#ifdef _MSC_VER
#define XFUNC func
uInt_t mmtime = GetTickCount();
inline float msToSec(const uInt_t time_ms){
	float ftime = static_cast<float>(time_ms);
	return  ftime / 1000;}
inline uInt_t CalcElapsetTime(const uInt_t time_ms){ return  GetTickCount() - time_ms; }
#else
#include <sys/time.h>
#include <time.h>
#define XFUNC cleanFunc(func).c_str()
inline double usecToSec(const double time_usec){ return  time_usec / 1000000.0; }
inline double CalcElapsetTime(const timeval t1){
	timeval t2;
	gettimeofday(&t2, NULL);
	double diff = static_cast<float>(t2.tv_usec - t1.tv_usec);
	return diff;}
inline const timeval StarTimeCount(void){ timeval t1; gettimeofday(&t1, NULL); return t1; }
timeval mmtime = StarTimeCount();
#endif


namespace hwt
{
    FUNCTION PUBLIC DECLARATION static void performCmd
        (
            const std::string& cmd
        );
    
    FUNCTION PUBLIC DECLARATION static const std::string cleanFunc
        (
            const std::string& funcName
        );
    
    

	std::ofstream msg::logger;
	mode_t msg::Mode;

	METHOD PUBLIC DEFINITION void msg::config
		(
			const char* timestamp,
			mode_t mode			
		)
	{
		Mode = mode;
		if (Mode == FILE)	
		{			
			std::stringstream buf;
			buf << "hwt_" << timestamp << ".log";
			logger.open(buf.str().c_str());
			if (logger.is_open() == false)
				std::printf("[ERROR][%s] hwt.log fail to open.\n", __func__);

		}
		else if (Mode == CONSOLE)
		{

		}
		else
		{

		}
	}

	METHOD PUBLIC DEFINITION void msg::close
		(
			void
		)
	{
		if (logger.is_open())
			logger.close();
	}
    
	METHOD PUBLIC DEFINITION void msg::ferrorln
		(
			const char* func,
			const std::string& text
		)
	{
#ifdef _MSC_VER
		float ss =  msToSec(CalcElapsetTime(mmtime));
#else
		float ss = usecToSec(CalcElapsetTime(mmtime));
#endif
        char cmsg[2024];
        memset(cmsg, 0, 2024);
		if (Mode == CONSOLE)
			std::printf("[%f][ERROR][%s] %s\n", ss, XFUNC, text.c_str());
		else if (Mode == FILE)
			logger << "[" << ss << "][ERROR][" << XFUNC << "]" << text << std::endl;
        
		#ifdef _WIN32           
			sprintf(cmsg, "libHEFOT::Messsage [ERROR][%s] %s APPLICATION TERMINATED", XFUNC, text.c_str());
			std::string msg = "echo MSGBOX \"";						
			msg.append(cmsg);
			msg.append("\" > %temp%\\TEMPmessage.vbs\ncall %temp%\\TEMPmessage.vbs\ndel %temp%\\TEMPmessage.vbs /f /q");
			performCmd(msg);
           // std::printf("\nThe HWT library has failed, check hwt.log file for detailed info. Press any key to continue..\n");
           // std::cin.get();
        #elif __APPLE__
			sprintf(cmsg, "libHEFOT::Messsage\n\n[ERROR][%s] %s\nAPPLICATION TERMINATED", XFUNC, text.c_str());
            std::string msg = "osascript -e 'tell app \"System Events\" to display dialog \"";
            msg.append(cmsg);
            msg.append("\"' 2> /dev/null > /dev/null");
            performCmd(msg);
        #elif __linux__
			sprintf(cmsg, "libHEFOT::Messsage\n\n[ERROR][%s] %s\nAPPLICATION TERMINATED", XFUNC, text.c_str());
            std::string msg = "xmessage \"";
            msg.append(cmsg);
            msg.append("\" -buttons Finish 2> /dev/null > /dev/null");
            performCmd(msg);
		#endif
		exit(1);
	}

	METHOD PUBLIC DEFINITION void msg::fwarningln
		(
			const char* func,
			const std::string& text
		)
	{
#ifdef _MSC_VER
		float ss =  msToSec(CalcElapsetTime(mmtime));
#else
		float ss = usecToSec(CalcElapsetTime(mmtime));
#endif
		if (Mode == CONSOLE)
			std::printf("[%f][WARNING][%s] %s\n", ss, XFUNC, text.c_str());
		else if (Mode == FILE)
			logger << "[" << ss << "][WARNING][" << XFUNC << "]" << text << std::endl;
	}

	METHOD PUBLIC DEFINITION void msg::finfoln
		(
		const char* func,
		const std::string& text
		)
	{
		#ifdef _MSC_VER
		float ss =  msToSec(CalcElapsetTime(mmtime));
		#else
		float ss = usecToSec(CalcElapsetTime(mmtime));
		#endif
		if (Mode == CONSOLE)
			std::printf("[%f][INFO][%s] %s\n", ss, XFUNC, text.c_str());
		else if (Mode == FILE)
			logger << "[" << ss << "][INFO][" << XFUNC << "]" << text << std::endl;
	}

	METHOD PUBLIC DEFINITION void msg::fshutdownln
		(
			const char* func,
			const std::string& text
		)
	{
#ifdef _MSC_VER
		float ss =  msToSec(CalcElapsetTime(mmtime));
#else
		float ss = usecToSec(CalcElapsetTime(mmtime));
#endif
		if (Mode == CONSOLE)
			std::printf("[%f][SHUTDOWN][%s] %s\n", ss, XFUNC, text.c_str());
		else if (Mode == FILE)
			logger << "[" << ss << "][SHUTDOWN][" << XFUNC << "]" << text << std::endl;
	}

	METHOD PUBLIC DEFINITION void msg::println
		(
			const std::string& text
		)
	{
		if (Mode == CONSOLE)
			std::printf("%s\n", text.c_str());
		else if (Mode == FILE)
			logger << text << std::endl;
	}

	METHOD PUBLIC DEFINITION void msg::print
		(
			const std::string& text
		)
	{
		if (Mode == CONSOLE)
			std::printf("%s", text.c_str());
		else if (Mode == FILE)
			logger << text;
	}
    
	FUNCTION PUBLIC DEFINITION static void performCmd
        (
            const std::string& cmd
        )
	{
		std::string lcmd(cmd);
        #ifdef _MSC_VER
            std::ofstream FileOutput;
            FileOutput.open("cmd.bat");
            FileOutput << "@ECHO OFF" << std::endl;
            FileOutput << lcmd << std::endl;
            FileOutput << "EXIT /B";
            FileOutput.close();
            lcmd = "cmd.bat";
        #endif
		system(lcmd.c_str());
        
	}
    
	FUNCTION PUBLIC DEFINITION static const std::string cleanFunc
        (
            const std::string& funcName
         )
	{
        std::string st(funcName);
        size_t ind = st.find_first_of("(");
        if (ind == std::string::npos) return st;
        st = st.substr(0, ind);
        ind = st.find_last_of(" ");
        if (ind == std::string::npos) return st;
        st = st.substr(ind+1);
        
        
        return st;
    }
    
}

