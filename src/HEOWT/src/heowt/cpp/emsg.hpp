// SOURCE CHARSET ENCODING FOR WINDOWS   : US-ASCII
// SOURCE CHARSET ENCODING FOR LINUX, OS : ASCII

#ifndef _EMSG_HPP
#define _EMSG_HPP 

#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include "types.hpp"
#include "layout_defs.hpp"

	#ifdef _MSC_VER
		#define __func__ __FUNCTION__
	#else		
        #define __func__ __PRETTY_FUNCTION__
	#endif
	#define shutdownln(text) hwt::msg::fshutdownln(__func__, text)
	#define infoln(text) hwt::msg::finfoln(__func__, text)
	#define errorln(text) hwt::msg::ferrorln(__func__, text)
	#define warningln(text) hwt::msg::fwarningln(__func__, text)

namespace hwt
{	
	const char* const mode[] = {"CONSOLE", "FILE" "NONE"};
	enum mode_t			  {CONSOLE, FILE};
	class msg
	{
		private:
			static std::ofstream logger;
			static mode_t Mode;

		public:
			METHOD PUBLIC DECLARATION static void config
				(
					const char* timestamp,
					mode_t mode = FILE				
				);

			METHOD PUBLIC DECLARATION static void close
				(
					void
				);

			METHOD PUBLIC DECLARATION static void ferrorln
				(
					const char* func,
					const std::string& text
				);

			METHOD PUBLIC DECLARATION static void fwarningln
				(
					const char* func,
					const std::string& text
				);

			METHOD PUBLIC DECLARATION static void finfoln
				(
					const char* func,
					const std::string& text
				);

			METHOD PUBLIC DECLARATION static void fshutdownln
				(
					const char* func,
					const std::string& text
				);

			METHOD PUBLIC DECLARATION static void println
				(
					const std::string& text
				);

			METHOD PUBLIC DECLARATION static void print
				(
					const std::string& text
				);


        
	};
}

#endif // _EMSG_HPP


