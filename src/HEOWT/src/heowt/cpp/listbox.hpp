// SOURCE CHARSET ENCODING FOR WINDOWS   : US-ASCII
// SOURCE CHARSET ENCODING FOR LINUX, OS : ASCII 

#ifndef _LISTBOX_HPP
#define _LISTBOX_HPP

#include "types.hpp"
#include "widget.hpp"
#include "utils.hpp"
#include "webTags.hpp"
#include "tools.hpp" 

namespace hwt
{

	class listbox : public widget
	{
		private:
			COPY_CONSTRUCTOR PRIVATE DEFINITION listbox
				(
					const listbox& l 
				)  
				:	widget( l.Father, l.UniqueName, l.Labeltx, l.Width, l.Height, LISTBOX, l.Visible, l.Disable, l.AianaEncoding.c_str() ),
					SessionWidgets(l.SessionWidgets){}

			OPERATOR PRIVATE DEFINITION listbox& operator=
				(
					const listbox&
				) { return *this; }

		private:				
			sessionWidgets_t* SessionWidgets;
			std::vector<listboxOpts_t> Options;

		public:
			CONSTRUCTOR PUBLIC DECLARATION listbox
				(	
					sessionWidgets_t* sessionwidgets,
                    const Char_t* aianaencoding,
					const uInt_t father,	
					const Str_t& uniquename = "",
					const Str_t& labeltx = "", 					
					const uInt_t width = 0,
					const uInt_t height = 0,	
					const bool visible = true,
					const bool disable = false
				);

			DESTRUCTOR PUBLIC DECLARATION ~listbox
				(
					void
				);

			METHOD PUBLIC DECLARATION void addOption
				(
					const Str_t& optionText,
					const Str_t& optionKey,
					const bool selected
				);

			METHOD PUBLIC DECLARATION Str_t toHtml
				(
					bool no_event = false
				) const;

			METHOD PUBLIC DECLARATION const Str_t toJavaScript
				(
					bool no_event = false
				) const;
	};
}


#endif // _LISTBOX_HPP

