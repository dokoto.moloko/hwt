// SOURCE CHARSET ENCODING FOR WINDOWS   : US-ASCII
// SOURCE CHARSET ENCODING FOR LINUX, OS : ASCII

#include "line.hpp" 

namespace hwt
{

	CONSTRUCTOR PUBLIC DEFINITION line::line
		(
			sessionWidgets_t* sessionwidgets,
			documentWidgets_t* documentwidgets,
			const uInt_t father,
            const Char_t* aianaencoding
		) : 
			SessionWidgets(sessionwidgets),
			DocumentWidgets(documentwidgets),			
			Father(father),
            AianaEncoding(aianaencoding)
	{
        
	}  


	DESTRUCTOR PUBLIC DEFINITION line::~line
		(
			void
		) 
	{
		#ifdef VERBOSE
		infoln(utl::Stringf(
			"Releasing Line with %l pointers to widgets classes.", 
			LineWidgets.byId.size()));
		#endif		
		for(byId_t::iterator it_line = LineWidgets.byId.begin(); it_line != LineWidgets.byId.end(); it_line++)
		{
			if (it_line->second != NULL)
			{
				#ifdef VERBOSE
				infoln(utl::Stringf("Delete widget Uuid : %l UniqueName : %s and realeased his dependencies.", 
					it_line->second->Uuid, 
					it_line->second->UniqueName.c_str()));
				#endif
				RemoveDocumentDepOfWidget(it_line->second->Uuid, it_line->second->UniqueName);  
				RemoveSessionDepOfWidget(it_line->second->Uuid, it_line->second->UniqueName);  
				delete it_line->second;
				it_line->second = NULL;
			}
		}  
	}


	METHOD PRIVATE DEFINITION widget* line::createWidget
		(
			enum objets_t type,
			const Str_t& uniquename,
			const Str_t& labeltx, 
			const uInt_t width, 
			const uInt_t height	
		)
	{
		widget* w = NULL;
		switch (type)
		{
			case hwt::BUTTON:
				w = new button(SessionWidgets, AianaEncoding.c_str(), Father, uniquename, labeltx, width, height);
				SessionWidgets->w.buttons[w->Uuid] = static_cast<button*>(w);				
				DocumentWidgets->w.buttons[w->Uuid] = static_cast<button*>(w);				
				LineWidgets.w.buttons[w->Uuid] = static_cast<button*>(w);	
				LineWidgets.byOrder.push_back(w);
				break;
			case hwt::INPUT:
				w = new input(SessionWidgets, AianaEncoding.c_str(), Father, uniquename, labeltx, width, height);
				SessionWidgets->w.inputs[w->Uuid] = static_cast<input*>(w);
				DocumentWidgets->w.inputs[w->Uuid] = static_cast<input*>(w);				
				LineWidgets.w.inputs[w->Uuid] = static_cast<input*>(w);
				LineWidgets.byOrder.push_back(w);
				break;
            case hwt::INPUT_DATE:
				w = new input(SessionWidgets, AianaEncoding.c_str(), Father, uniquename, labeltx, width, height, INPUT_DATE);
				SessionWidgets->w.inputs_date[w->Uuid] = static_cast<input*>(w);
				DocumentWidgets->w.inputs_date[w->Uuid] = static_cast<input*>(w);				
				LineWidgets.w.inputs_date[w->Uuid] = static_cast<input*>(w);
				LineWidgets.byOrder.push_back(w);
				break;
			case hwt::LABEL:
				w = new label(SessionWidgets, AianaEncoding.c_str(), Father, uniquename, labeltx, width, height);
				SessionWidgets->w.labels[w->Uuid] = static_cast<label*>(w);
				DocumentWidgets->w.labels[w->Uuid] = static_cast<label*>(w);				
				LineWidgets.w.labels[w->Uuid] = static_cast<label*>(w);
				LineWidgets.byOrder.push_back(w);
				break;
			case hwt::GRID:
				w = new grid(SessionWidgets, AianaEncoding.c_str(), Father, uniquename, labeltx, width, height);
				SessionWidgets->w.grids[w->Uuid] = static_cast<grid*>(w);
				DocumentWidgets->w.grids[w->Uuid] = static_cast<grid*>(w);				
				LineWidgets.w.grids[w->Uuid] = static_cast<grid*>(w);
				LineWidgets.byOrder.push_back(w);
				break;
			case hwt::FILEEXPLORER:
				w = new fileexplorer(SessionWidgets, AianaEncoding.c_str(), Father, uniquename, labeltx, width, height);
				SessionWidgets->w.fileexplorers[w->Uuid] = static_cast<fileexplorer*>(w);
				DocumentWidgets->w.fileexplorers[w->Uuid] = static_cast<fileexplorer*>(w);				
				LineWidgets.w.fileexplorers[w->Uuid] = static_cast<fileexplorer*>(w);
				LineWidgets.byOrder.push_back(w);
				break;
			case hwt::CHECKBOXGROUP:
				w = new checkboxGroup(SessionWidgets, AianaEncoding.c_str(), Father, uniquename, labeltx, width, height);
				SessionWidgets->w.checkboxGroups[w->Uuid] = static_cast<checkboxGroup*>(w);
				DocumentWidgets->w.checkboxGroups[w->Uuid] = static_cast<checkboxGroup*>(w);				
				LineWidgets.w.checkboxGroups[w->Uuid] = static_cast<checkboxGroup*>(w);
				LineWidgets.byOrder.push_back(w);
				break;
			case hwt::LISTBOX:
				w = new listbox(SessionWidgets, AianaEncoding.c_str(), Father, uniquename, labeltx, width, height);
				SessionWidgets->w.listboxes[w->Uuid] = static_cast<listbox*>(w);
				DocumentWidgets->w.listboxes[w->Uuid] = static_cast<listbox*>(w);				
				LineWidgets.w.listboxes[w->Uuid] = static_cast<listbox*>(w);
				LineWidgets.byOrder.push_back(w);
				break;
			case hwt::TEXTAREA:
				w = new textArea(SessionWidgets, AianaEncoding.c_str(), Father, uniquename, labeltx, width, height);
				SessionWidgets->w.textAreas[w->Uuid] = static_cast<textArea*>(w);
				DocumentWidgets->w.textAreas[w->Uuid] = static_cast<textArea*>(w);
				LineWidgets.w.textAreas[w->Uuid] = static_cast<textArea*>(w);
				LineWidgets.byOrder.push_back(w);
				break;
			default:
				errorln("Unknown widget type.");
				break;
		}
		if (w == NULL)
			errorln("Allocate Memory error.");

		return w;
	}

	METHOD PUBLIC DEFINITION size_t line::AddWidget
		(			
			enum objets_t type,			
			const Str_t& uniquename,
			const Str_t& labeltx, 
			const uInt_t width, 
			const uInt_t height			
		)
	{
        widget* w = createWidget(type, uniquename, labeltx, width, height);

		SessionWidgets->byId[w->Uuid] = w;      
		SessionWidgets->byName[w->UniqueName] = w;

		DocumentWidgets->byId[w->Uuid] = w;      
		DocumentWidgets->byName[w->UniqueName] = w;

		LineWidgets.byId[w->Uuid] = w;      
		LineWidgets.byName[w->UniqueName] = w;     

		return LineWidgets.byId.size();
	}

	METHOD PRIVATE DEFINITION widget* line::advanceWidget
		(
			const size_t index
		)
	{
		if (index > LineWidgets.byId.size())
			errorln("Index out of range");
		byId_t::iterator it = LineWidgets.byId.begin();
		std::advance(it, index);

		return it->second;
	}
    
	METHOD PUBLIC DEFINITION button* line::ModButton
		(
			const size_t index
		)
	{
		return modWidget<button>(BUTTON, advanceWidget(index));
	}

	METHOD PUBLIC DEFINITION button* line::ModButton
		(
			const Str_t& uniquename
		)
	{
		byName_t::const_iterator it = LineWidgets.byName.find(uniquename);
		if (it == LineWidgets.byName.end())
			errorln("unKnown UniqueName.");
		return modWidget<button>(BUTTON, it->second);
	}

	METHOD PUBLIC DEFINITION label* line::ModLabel
		(
			size_t index
		)
	{
		return modWidget<label>(LABEL, advanceWidget(index));
	}

	METHOD PUBLIC DEFINITION label* line::ModLabel
		(
			const Str_t& uniquename
		)
	{
		byName_t::const_iterator it = LineWidgets.byName.find(uniquename);
		if (it == LineWidgets.byName.end())
			errorln("unKnown UniqueName.");
		return modWidget<label>(LABEL, it->second);
	}

	METHOD PUBLIC DEFINITION input* line::ModInput
		(
			size_t index
		)
	{
		return modWidget<input>(INPUT, advanceWidget(index));
	}

	METHOD PUBLIC DEFINITION input* line::ModInput
		(
			const Str_t& uniquename
		)
	{
		byName_t::const_iterator it = LineWidgets.byName.find(uniquename);
		if (it == LineWidgets.byName.end())
			errorln("unKnown UniqueName.");
		return modWidget<input>(INPUT, it->second);
	}

	METHOD PRIVATE DEFINITION void line::RemoveSessionDepOfWidget
		(
			const uInt_t uuid,
			const Str_t& uniquename
		)
	{
		utl::delDepWidget<uInt_t, byId_t>(uuid, SessionWidgets->byId);
		utl::delDepWidget<Str_t, byName_t>(uniquename, SessionWidgets->byName);
		utl::delDepWidget_Redund1<sessionWidgets_t>(uuid, *SessionWidgets);	
	}

	METHOD PRIVATE DEFINITION void line::RemoveDocumentDepOfWidget
		(
			const uInt_t uuid,
			const Str_t& uniquename
		)
	{
		utl::delDepWidget<uInt_t, byId_t>(uuid, DocumentWidgets->byId);
		utl::delDepWidget<Str_t, byName_t>(uniquename, DocumentWidgets->byName);
		utl::delDepWidget_Redund1<documentWidgets_t>(uuid, *DocumentWidgets);
	}

	METHOD PRIVATE DEFINITION void line::RemoveLineDepOfWidget
		(
			const uInt_t uuid,
			const Str_t& uniquename
		)
	{
		utl::delDepWidget<uInt_t, byId_t>(uuid, LineWidgets.byId);
		utl::delDepWidget<Str_t, byName_t>(uniquename, LineWidgets.byName);
		utl::delDepWidget_Redund1<lineWidgets_t>(uuid, LineWidgets);
	}

	METHOD PRIVATE DEFINITION void line::RemoveAllDepOfWidget
		(
			const uInt_t uuid,
			const Str_t& uniquename
		)
	{
		// LINE
		RemoveLineDepOfWidget(uuid, uniquename);
		// DOCUMENT
		RemoveDocumentDepOfWidget(uuid, uniquename);
		// SESSION
		RemoveSessionDepOfWidget(uuid, uniquename);

	}

	METHOD PUBLIC DEFINITION void line::DelWidget
		(
			const size_t index
		)
	{
		if (index >= LineWidgets.byId.size())
			errorln ("Index out of range.");

		byId_t::iterator it_line = LineWidgets.byId.begin();
		if (it_line == LineWidgets.byId.end()) errorln ("unKnown Uuid.");

		std::advance(it_line, index);
		RemoveAllDepOfWidget(it_line->second->Uuid, it_line->second->UniqueName);        
		delete it_line->second;	
		it_line->second = NULL;

		#ifdef VERBOSE
		infoln(utl::Stringf("Delete widget Uuid : %l UniqueName : %s and realeased his dependencies.", 
			it_line->second->Uuid, 
			it_line->second->UniqueName.c_str()));
		#endif
	}

	METHOD PUBLIC DEFINITION void line::DelWidget
		(
			const Str_t& uniquename
		)
	{
		byName_t::iterator it_line = LineWidgets.byName.find(uniquename);
		if (it_line == LineWidgets.byName.end())
			errorln ("unKnown Uuid.");
		RemoveAllDepOfWidget(it_line->second->Uuid, it_line->second->UniqueName);
		delete it_line->second;	
		it_line->second = NULL;

		#ifdef VERBOSE
		infoln(utl::Stringf("Delete widget Uuid : %l UniqueName : %s and realeased his dependencies.", 
			it_line->second->Uuid, 
			it_line->second->UniqueName.c_str()));
		#endif
	}

	METHOD PUBLIC DEFINITION const lineWidgets_t& line::getconstAllWidgets
		(
			void
		) const
	{
		return LineWidgets;
	}

	METHOD PUBLIC DEFINITION lineWidgets_t& line::getAllWidgets
		(
			void
		)
	{
		return LineWidgets;
	}
    
}
