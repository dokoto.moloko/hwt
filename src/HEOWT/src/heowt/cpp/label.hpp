// SOURCE CHARSET ENCODING FOR WINDOWS   : US-ASCII
// SOURCE CHARSET ENCODING FOR LINUX, OS : ASCII

#ifndef _LABEL_HPP
#define _LABEL_HPP
 
#include "types.hpp"
#include "widget.hpp"

namespace hwt
{
	class label : public widget
	{
		private:
			COPY_CONSTRUCTOR PRIVATE DEFINITION label
				(
					const label& l 
				)  :	widget
						(	
							l.Father, 
							l.UniqueName, 
							l.Labeltx, 
							l.Width, 
							l.Height, 
							LABEL, 
							l.Visible, 
							l.Disable,
                            l.AianaEncoding.c_str()
						) 
				{}

			OPERATOR PRIVATE DEFINITION label& operator=
				(
					const label&
				) { return *this; }
		
		private:
			//sessionWidgets_t* SessionWidgets;

		public:
			CONSTRUCTOR PUBLIC DECLARATION label
				(	
					sessionWidgets_t* sessionwidgets,
                    const Char_t* aianaencoding,
					const uInt_t father,	
					const Str_t& uniquename = "",
					const Str_t& labeltx = "", 					
					const uInt_t width = 0,
					const uInt_t height = 0,
					const bool visible = true,
					const bool disable = false
				);

			DESTRUCTOR PUBLIC DECLARATION ~label
				(
				);

			METHOD PUBLIC DECLARATION Str_t toHtml
				(
					bool no_event = false
				) const;
	};
}

#endif //_LABEL_HPP


