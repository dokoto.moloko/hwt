// SOURCE CHARSET ENCODING FOR WINDOWS   : US-ASCII
// SOURCE CHARSET ENCODING FOR LINUX, OS : ASCII

#include "document.hpp" 

namespace hwt
{
	uInt_t document::count = 0;

	CONSTRUCTOR PUBLIC DEFINITION document::document
		(
			sessionWidgets_t* sessionwidgets,
			const uInt_t father,	
			const Char_t* aianaencoding,
			const Str_t& uniquename,
			const Str_t& title,
			const Str_t& description, 					
			const uInt_t width,
			const uInt_t height, 
			const bool visible,
			const bool ontop,
			const bool disable            
		) :
			widget(	father, uniquename, title, width, height, DOCUMENT, visible, disable, aianaencoding ),
			SessionWidgets(sessionwidgets),
			onTop(ontop),
			Description(description),
			Priority(count++)
	{
	}    

    DESTRUCTOR PUBLIC DEFINITION document::~document
    (
        void
    )
    {
        #ifdef VERBOSE
			infoln(utl::Stringf(
					"Releasing Document Uuid: %l UniqueName : %s with %l pointers to lines classes.", 
					this->Uuid,
					this->UniqueName.c_str(),
					vLine.size()));
        #endif
        for (std::vector<line*>::iterator it = vLine.begin(); it != vLine.end(); it++)
        {
            if (*it != NULL) { delete *it; *it = NULL; }
        }
		utl::delDepWidget<uInt_t, byId_t>(this->Uuid, SessionWidgets->byId);
		utl::delDepWidget<Str_t, byName_t>(this->UniqueName, SessionWidgets->byName);		
    }
    
    METHOD PUBLIC DEFINITION size_t document::AddLine
        (
            void
        )
    {
        vLine.push_back(new line( SessionWidgets, &DocumentWidgets, this->Uuid, AianaEncoding.c_str()));
        return vLine.size() - 1;
    }
    
    METHOD PUBLIC DEFINITION void document::DelLine
        (
            size_t index
        )
    {
        if (index >= vLine.size()) errorln("Index out of range.");        		

        line* auxLine = vLine[index];
		if (auxLine)
		{
			delete auxLine;
			auxLine = NULL;
		}
        vLine.erase (vLine.begin () + (const int)index );
    }
    
    METHOD PUBLIC DEFINITION line* document::ModLine
        (
            size_t index
        )
    {
        if (index >= vLine.size())
            errorln ("Index out of range.");
        
        return vLine[index];
    }
    
    METHOD PUBLIC DEFINITION const line* document::ModLine
        (
            size_t index
        ) const
    {
        if (index >= vLine.size())
            errorln ("Index out of range.");
        
        return vLine[index];
    }
    
    
	METHOD PRIVATE DEFINITION Str_t document::wToHtml
		(
			widget* w, 
			const objets_t objType
		) const
	{
		Str_t utf8;
		switch (objType)
		{
			case hwt::BUTTON:
				return static_cast<button*>(w)->toHtml();
				break;
			case hwt::INPUT:
				return static_cast<input*>(w)->toHtml();
				break;
			case hwt::INPUT_DATE:
				return static_cast<input*>(w)->toHtml();
				break;
			case hwt::LABEL:
				return static_cast<label*>(w)->toHtml();
				break;
			case hwt::GRID:
				return static_cast<grid*>(w)->toHtml();
				break;
			case hwt::FILEEXPLORER:
				return static_cast<fileexplorer*>(w)->toHtml();
				break;
			case hwt::LISTBOX:
				return static_cast<listbox*>(w)->toHtml();
				break;
			case hwt::CHECKBOXGROUP:
				return static_cast<checkboxGroup*>(w)->toHtml();
				break;
			case hwt::TEXTAREA:
				return static_cast<textArea*>(w)->toHtml();
				break;
			default:
				return "";
				break;
		}
		return "";
	}

	METHOD PUBLIC DEFINITION Str_t document::toHtml
		(
			bool 
		) const
	{
		Buffer_t buf;
		Str_t utf8;
		if(!utl::anyToUTF(AianaEncoding.c_str(), Description.c_str(), utf8))
			errorln("Charset to UTF-8 conversion has failed.");
		buf << utl::Stringf(chtml::doc_h4, utf8.c_str());		
		buf << chtml::table_open;
		for
            (
                std::vector<line*>::const_iterator itLine = this->vLine.begin();
                itLine != this->vLine.end();
                itLine++
            )
		{		
			buf << chtml::tr_open;
			if ((*itLine)->getconstAllWidgets().byId.size() > 0 )
			{
			
				for (
						inOrder_t::const_iterator itWidget = (*itLine)->getconstAllWidgets().byOrder.begin();                        
					    itWidget != (*itLine)->getconstAllWidgets().byOrder.end();
						itWidget++
					)
				{
					if ((*itWidget)->Type == FILEEXPLORER || (*itWidget)->Type == CHECKBOXGROUP)
						buf << wToHtml(*itWidget, (*itWidget)->Type);
					else
					{				
						buf << chtml::td_open;
						buf << wToHtml(*itWidget, (*itWidget)->Type);
						buf << chtml::td_close;
					}
				}
			}
			else
			{
				buf << chtml::void_line;
			}
			buf << chtml::tr_close;
		}
		buf << chtml::table_close;

		return buf.str();
	}

	METHOD PUBLIC DEFINITION Str_t document::toHtmlWithDiv
        (
            void
         )
	{
		Buffer_t buf;
		buf << utl::Stringf
            (
                chtml::doc_div,
                this->Uuid,
                "dialog_window",
                this->getconstUtf8Labeltx().c_str()
             );
        
		buf << toHtml();
		buf << chtml::div_close;

		return buf.str();
	}


	METHOD PUBLIC DECLARATION const Str_t document::getconstTitle
		(
			void
		) const
	{
		return getconstLabeltx();
	}

	METHOD PUBLIC DECLARATION const Str_t document::getconstUtf8Title
		(
			void
		) const
	{
		return getconstUtf8Labeltx();
	}

	METHOD PUBLIC DECLARATION void document::setTitle
		(
			const Str_t title
		)
	{
		this->Labeltx = title;
	}

	METHOD PUBLIC DEFINITION bool document::isOnTop
		(
			void
		) const
	{
		return onTop;
	}

	METHOD PRIVATE DEFINITION void document::setOnTop
		(
			const bool ontop
		)
	{
		onTop = ontop;
	}

	METHOD PUBLIC DEFINITION const std::vector<line*>& document::getconstAllLines
		( 
			void 
		)const
	{
		return vLine;
	}

	METHOD PUBLIC DEFINITION std::vector<line*>& document::getAllLines
		( 
			void 
		)
	{
		return vLine;
	}

	METHOD PUBLIC DEFINITION void document::AddAlert
		(
			const Str_t& text
		)
	{
		alerts.push_back(text);
	}

	METHOD PUBLIC DEFINITION const std::vector<Str_t>& document::getconstAlerts
		(
			void
		) const
	{
		return alerts;
	}

	METHOD PUBLIC DECLARATION void document::clearAlerts
		(
			void
		)
	{
		alerts.clear();
	}

}

