// SOURCE CHARSET ENCODING FOR WINDOWS   : US-ASCII
// SOURCE CHARSET ENCODING FOR LINUX, OS : ASCII

#ifndef _DEPLOYCL_HPP
#define _DEPLOYCL_HPP

#include "types.hpp"
#include "deployapp.hpp"
#include "session.hpp"
 
namespace hwt
{

	class deploy
	{
		private:
			session*	Session;
            sessionWidgets_t* mapOfWidgets;

		private:
			COPY_CONSTRUCTOR PRIVATE DEFINITION deploy
				(
					const deploy& d
				)  : Session(d.Session),
                     mapOfWidgets(d.mapOfWidgets){}

			OPERATOR PRIVATE DEFINITION deploy& operator=
				(
					const deploy&
				){ return *this; }
		
		public:
			CONSTRUCTOR PUBLIC DECLARATION deploy
				(
					session* ss,
                    sessionWidgets_t* mapofwidgets
				);

			DESTRUCTOR PUBLIC DECLARATION ~deploy
				(
					void
				);


	};
}

#endif // _DEPLOYCL_HPP

