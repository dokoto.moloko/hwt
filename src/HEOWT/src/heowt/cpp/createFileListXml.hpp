// SOURCE CHARSET ENCODING FOR WINDOWS   : US-ASCII
// SOURCE CHARSET ENCODING FOR LINUX, OS : ASCII
#ifndef CREATEFILEXML_HPP
#define CREATEFILEXML_HPP

/*--------------------------------------------------------------------------------------------------------------
* INCLUDES
*-------------------------------------------------------------------------------------------------------------*/
#include <iostream>
#include <set>
#include "pugixml.hpp"
#include "utils.hpp"

namespace hwt 
{
	class createFileListXml
	{
	/*--------------------------------------------------------------------------------------------------------------
    * PRIVATE TYPES
    *--------------------------------------------------------------------------------------------------------------*/         
    private:		
		Vfile_t	VecStr;		
		pugi::xml_document XmlDoc;
		
    /*--------------------------------------------------------------------------------------------------------------
    * PUBLIC METHODS
    *--------------------------------------------------------------------------------------------------------------*/
	public:
								createFileListXml				(Vfile_t lVecStr);
								~createFileListXml				(void);
		bool					Conv							(void);
		Str_t					ToString						(void);

	};
}

#endif


