// SOURCE CHARSET ENCODING FOR WINDOWS   : US-ASCII
// SOURCE CHARSET ENCODING FOR LINUX, OS : ASCII

#ifndef _HEADER_HPP
#define _HEADER_HPP 

#include "types.hpp"
#include "webTags.hpp"
#include "utils.hpp"

namespace hwt
{
	class header
	{
		private:
			COPY_CONSTRUCTOR PRIVATE DEFINITION header
				(
					const header& h
				) : AianaEncoding(h.AianaEncoding),
                    Title(h.Title){}

			OPERATOR PRIVATE DEFINITION header& operator=
				(
					const header&
				) { return *this; }

		private:
            Str_t AianaEncoding;
			Str_t Title;
            

		public:
			CONSTRUCTOR PUBLIC DEFINITION header
				(
                    const Char_t* aianaencoding = NULL,
					const Char_t* title = "New Web C++ Application"
				) :
					AianaEncoding(aianaencoding),
                    Title(title){}

			DESTRUCTOR PUBLIC DEFINITION ~header
				(
				) {}

			METHOD PUBLIC DECLARATION const Str_t toHtml
				(
					void
				) const;

			METHOD PUBLIC DECLARATION const Str_t toHtmlWithHeader
				(
					void
				) const;

			METHOD PUBLIC DECLARATION const Str_t getconstUtf8Title
				(
					void
				) const;

			METHOD PUBLIC DECLARATION const Str_t getconstTitle
				(
					void
				) const;

			METHOD PUBLIC DECLARATION void setTitle
				(
					const Char_t* value
				);
				
	};
}

#endif //_HEADER_HPP

