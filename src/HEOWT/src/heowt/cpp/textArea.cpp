// SOURCE CHARSET ENCODING FOR WINDOWS   : US-ASCII
// SOURCE CHARSET ENCODING FOR LINUX, OS : ASCII

#include "textArea.hpp" 

namespace hwt
{
	CONSTRUCTOR PUBLIC DEFINITION textArea::textArea
		( 
			sessionWidgets_t* sessionwidgets,
            const Char_t* aianaencoding,
			const uInt_t father,
			const Str_t& uniquename,
			const Str_t& labeltx, 			
			const uInt_t width, 
			const uInt_t height,
            enum objets_t type,
			const bool visible,
			const bool disable,
            const uInt_t maxlen
		)		
	:	widget(father, uniquename, labeltx, width, height, type, visible, disable, aianaencoding ),
		SessionWidgets(sessionwidgets),
		MaxLen(maxlen)
	{        
	}

	DESTRUCTOR PUBLIC DEFINITION textArea::~textArea
		(
			void
		)
	{
	}

	METHOD PUBLIC DEFINITION Str_t textArea::toHtml
		(
			bool
		) const
	{	
		return utl::Stringf(hwt::chtml::textArea, this->Uuid, getconstUtf8Labeltx().c_str());
	}

	METHOD PUBLIC DEFINITION const Str_t textArea::getconstValue
		(
			void
		) const 
	{
        if (Value.str().empty()) return "";
		return Value.str();
	}

	METHOD PUBLIC DEFINITION const Str_t textArea::getconstUtf8Value
		(
			void
		) const 
	{
        if (Value.str().empty()) return "";
		Str_t utf8;
		if(!utl::anyToUTF(AianaEncoding.c_str(), Value.str().c_str(), utf8))
			errorln("Charset to UTF-8 conversion has failed.");
		return utf8; 
	}

	METHOD PUBLIC DEFINITION void textArea::setMaxLen
		(
			uInt_t len
		)
	{
		MaxLen = len;
	}

	METHOD PUBLIC DEFINITION uInt_t textArea::getconstMaxLen
		(
			void
		) const
	{
		return MaxLen;
	}

}

