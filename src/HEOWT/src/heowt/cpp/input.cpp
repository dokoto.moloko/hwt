// SOURCE CHARSET ENCODING FOR WINDOWS   : US-ASCII
// SOURCE CHARSET ENCODING FOR LINUX, OS : ASCII

#include "input.hpp" 

namespace hwt
{
	CONSTRUCTOR PUBLIC DEFINITION input::input
		( 
			sessionWidgets_t* sessionwidgets,
            const Char_t* aianaencoding,
			const uInt_t father,
			const Str_t& uniquename,
			const Str_t& labeltx, 			
			const uInt_t width, 
			const uInt_t height,
            enum objets_t type,
			const bool visible,
			const bool disable,
			const uInt_t maxlen          
		)		
	:	widget(father, uniquename, labeltx, width, height, type, visible, disable, aianaencoding ),
		SessionWidgets(sessionwidgets),
		MaxLen(maxlen)
	{        
	}

	DESTRUCTOR PUBLIC DEFINITION input::~input
		(
			void
		)
	{
	}

	METHOD PUBLIC DEFINITION Str_t input::toHtml
		(
			bool
		) const
	{	
		return utl::Stringf(hwt::chtml::input, this->Uuid);
	}

	METHOD PUBLIC DEFINITION const value_t input::getconstValue
		(
			void
		) const 
	{ 
		return Value; 
	}

	METHOD PUBLIC DEFINITION const Str_t input::getconstUtf8Value
		(
			void
		) const 
	{ 
		Str_t utf8;
		if(!utl::anyToUTF(AianaEncoding.c_str(), Value.value.c_str(), utf8))
			errorln("Charset to UTF-8 conversion has failed.");
		return utf8; 
	}

	METHOD PUBLIC DEFINITION void input::setMaxLen
		(
			uInt_t len
		)
	{
		MaxLen = len;
	}

	METHOD PUBLIC DEFINITION uInt_t input::getconstMaxLen
		(
			void
		) const
	{
		return MaxLen;
	}

	METHOD PUBLIC DECLARATION const Str_t input::toJavaScript
		(
			bool 
		) const
	{		
		return utl::Stringf(cjs::event_keypress, Uuid, Uuid, objets[Type]);
	}

}

