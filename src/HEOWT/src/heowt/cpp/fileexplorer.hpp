// SOURCE CHARSET ENCODING FOR WINDOWS   : US-ASCII
// SOURCE CHARSET ENCODING FOR LINUX, OS : ASCII

#ifndef _FILEEXPLORER_HPP
#define _FILEEXPLORER_HPP 

#include "types.hpp"
#include "widget.hpp"
#include "utils.hpp"
#include "webTags.hpp"
#include "tools.hpp" 
#include "pugixml.hpp"

namespace hwt
{
	class fileexplorer : public widget
	{
		private:
		COPY_CONSTRUCTOR PRIVATE DEFINITION fileexplorer
			(
				const fileexplorer& f 
			)  
			:	widget( f.Father, f.UniqueName, f.Labeltx, f.Width, f.Height, FILEEXPLORER, f.Visible, f.Disable, f.AianaEncoding.c_str() ),
				SessionWidgets(f.SessionWidgets){}

		OPERATOR PRIVATE DEFINITION fileexplorer& operator=
			(
				const fileexplorer&
			) { return *this; }

		private:				
			sessionWidgets_t* SessionWidgets;
			uInt_t *UuidLabelPtr, *UuidInputPtr, *UuidButtonPtr;
			uInt_t UuidLabel, UuidInput, UuidButton; 
			value_t Value;

		public:
		CONSTRUCTOR PUBLIC DECLARATION fileexplorer
			(	
				sessionWidgets_t* sessionwidgets,
                const Char_t* aianaencoding,
				const uInt_t father,	
				const Str_t& uniquename = "",
				const Str_t& labeltx = "", 					
				const uInt_t width = 0,
				const uInt_t height = 0,	
				const bool visible = true,
				const bool disable = false
			);

		DESTRUCTOR PUBLIC DECLARATION ~fileexplorer
			(
				void
			);

		METHOD PUBLIC DECLARATION Str_t toHtml
			(
				bool no_event = false
			) const;

		METHOD PUBLIC DECLARATION const Str_t toJavaScript
			(
				bool no_events = false
			) const;

		METHOD PUBLIC DECLARATION uInt_t getconstUuidLabel
			(
				void
			) const;

		METHOD PUBLIC DECLARATION uInt_t getconstUuidInput
			(
				void
			) const;

		METHOD PUBLIC DECLARATION uInt_t getconstUuidButton
			(
				void
			) const;

		METHOD PUBLIC DECLARATION const Str_t getconstInputValue
			(
				void
			) const;

		METHOD PUBLIC DECLARATION void setInputValue
			(
				const value_t& value
			) ;
	};
}
#endif // _FILEEXPLORER_HPP

