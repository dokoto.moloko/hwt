#include "listbox.hpp" 

namespace hwt
{
	CONSTRUCTOR PUBLIC DEFINITION listbox::listbox
		(	
			sessionWidgets_t* sessionwidgets,
            const Char_t* aianaencoding,
			const uInt_t father,
			const Str_t& uniquename,
			const Str_t& labeltx, 			
			const uInt_t width, 
			const uInt_t height,
			const bool visible,
			const bool disable            
		) 

		:	widget(father, uniquename, labeltx, width, height, LISTBOX, visible, disable, aianaencoding ),
			SessionWidgets(sessionwidgets)
	{		
	}

	DESTRUCTOR PUBLIC DEFINITION listbox::~listbox
		(
			void
		)
	{
	}

	METHOD PUBLIC DEFINITION void listbox::addOption
		(
			const Str_t& optionText,
			const Str_t& optionKey,
			const bool selected
		)
	{
		listboxOpts_t v = {optionText, optionKey, selected};
		Options.push_back(v);
	}

	METHOD PUBLIC DEFINITION Str_t listbox::toHtml
		(
			bool 
		) const
	{
		Buffer_t buf;
		Str_t utf8;
		buf << utl::Stringf(chtml::listbox_open, Uuid);
		for(std::vector<listboxOpts_t>::const_iterator it = Options.begin();
			it != Options.end();
			it++)
		{
			if(!utl::anyToUTF(AianaEncoding.c_str(), it->optionText.c_str(), utf8))
				errorln("Charset to UTF-8 conversion has failed.");
			if (it->selected)
				buf << utl::Stringf(chtml::listbox_option_default, it->optionKey.c_str(), utf8.c_str()); 
			else
				buf << utl::Stringf(chtml::listbox_option, it->optionKey.c_str(), utf8.c_str());
			utf8.clear();
		}
		buf << utl::Stringf(chtml::listbox_close);

		return buf.str();
	}

	METHOD PUBLIC DEFINITION const Str_t listbox::toJavaScript
		(
			bool
		) const
	{
		Buffer_t buf;
		buf << utl::Stringf(cjs::widget_width, Uuid, Width, Height);
		buf << utl::Stringf(cjs::event_change, Uuid, Uuid, objets[Type]);

		return buf.str();
	}


}

