// SOURCE CHARSET ENCODING FOR WINDOWS   : US-ASCII
// SOURCE CHARSET ENCODING FOR LINUX, OS : ASCII

#include "utils.hpp"

namespace hwt 
{
namespace utl
{	

	FUNCTION PRIVATE DECLARATION static void recListFilesOnFolder
		(
			const hwt::Char_t* path,
			hwt::Vfile_t& fileList,		
			hwt::Vfile_t& FolderItemList,
			hwt::Vfile_t::iterator begin
		);


	FUNCTION PUBLIC DEFINITION Str_t Uint2Str
		(
			uInt_t value
		)
	{
		return Any2Any<Str_t, uInt_t>(value);
	}


	FUNCTION PUBLIC DEFINITION uInt_t Str2Uint
		(
			Str_t value
		)
	{
		return Any2Any<uInt_t, Str_t>(value);
	}

	FUNCTION PUBLIC DEFINITION value_t Any2Value
		(
			int value
		)
	{
		value_t ret = { Any2Str<int>(value), hwt::INT };  
		return ret; 
	}

	FUNCTION PUBLIC DEFINITION value_t Any2Value
		(
			long value
		)
	{ 
		value_t ret = { Any2Str<long>(value), hwt::LONG };  
		return ret; 
	}

	FUNCTION PUBLIC DEFINITION value_t Any2Value
		(
			double value
		)
	{ 
		value_t ret = { Any2Str<double>(value), hwt::DOUBLE };  
		return ret; 
	}

	FUNCTION PUBLIC DEFINITION value_t Any2Value
		(
			float value
		)
	{ 
		value_t ret = { Any2Str<float>(value), hwt::FLOAT };  
		return ret; 
	}

	FUNCTION PUBLIC DEFINITION value_t Any2Value
		(
			char value
		)
	{ 
		value_t ret = { Any2Str<char>(value), hwt::CHAR };  
		return ret; 
	}

	FUNCTION PUBLIC DEFINITION value_t Any2Value
		(
			Str_t value
		)
	{ 
		value_t ret = { Any2Str<Str_t>(value), hwt::STRING };  
		return ret; 
	}	

	FUNCTION PUBLIC DEFINITION value_t Any2Value
		(
			wchar_t* value
		)
	{ 
		value_t ret = { Any2Str<wchar_t*>(value), hwt::STRING };  
		return ret; 
	}	

	FUNCTION PUBLIC DECLARATION const Str_t ReplaceAll
		(
			const Str_t& text,
			const Str_t& argToSearch,
			const Str_t& argWithReplace
		)
	{
		Str_t ltext = text;
		size_t ind = ltext.find_first_of(argToSearch);
		while (ind != Str_t::npos)
		{
			ltext.replace(ind, argToSearch.size(), argWithReplace);
			ind = ltext.find_first_of(argToSearch);
		}

		return ltext;
	}

		template<class type>
		FUNCTION PUBLIC DEFINITION static bool Split_T
			(
				type sep, 
				const Char_t* raw, 
				std::vector<Str_t>& split_values
			)
		{
			Str_t line(raw), extr;
			size_t ini = 0, end = 0;
			end = line.find_first_of(sep);
			while(end != std::string::npos)
			{
				extr = line.substr(ini, end-ini);
				if (extr.size() > 0)
					split_values.push_back(extr);
				
				ini = end+1;
				end = line.find_first_of(sep, end+1);
				if (end == std::string::npos && ini < line.size())
				{
					Str_t extr = line.substr(ini);
					if (extr.size() > 0)
						split_values.push_back(extr);
				}
			}

			if (split_values.size() == 0) return false;

			return true;		
		}


		FUNCTION PUBLIC DEFINITION bool Split
			(
				const Char_t* sep, 
				const Char_t* raw, 
				std::vector<Str_t>& split_values
			)
		{
			return Split_T<const Char_t*>(sep, raw, split_values);
		}

		FUNCTION PUBLIC DEFINITION bool Split
			(
				const Char_t sep, 
				const Char_t* raw, 
				std::vector<Str_t>& split_values
			)
		{
			return Split_T<const Char_t>(sep, raw, split_values);
		}

		FUNCTION PUBLIC DEFINITION void Json2Params
			(
				const Str_t& json,
				std::map<Str_t, Str_t>& params
			)
		{
			Str_t cut = json.substr(1, json.size());
			cut = cut.substr(0, cut.find_first_of("}"));
			std::vector<Str_t> pairs, keys;
			Str_t key, val;
			Split(',', cut.c_str(), pairs);
			if (!pairs.empty())
			{			
				for(std::vector<Str_t>::const_iterator it = pairs.begin();
					it != pairs.end();
					it++)
				{
					Split(':', it->c_str(), keys);
					key = (keys[0][0] == '"') ? keys[0].substr(1, keys[0].size()-2): keys[0];
					val = (keys[1][0] == '"') ? val = keys[1].substr(1, keys[1].size()-2): keys[1];
					params[key] = val;
					key.clear();
					val.clear();
					keys.clear();
				}
			}
			else
			{
				Split(':', cut.c_str(), keys);
				if (!keys.empty())
				{
					key = (keys[0][0] == '"') ? keys[0].substr(1, keys[0].size()-2): keys[0];
					val = (keys[1][0] == '"') ? val = keys[1].substr(1, keys[1].size()-2): keys[1];
					params[key] = val;
					key.clear();
					val.clear();
					keys.clear();
				}
			}
		}

        FUNCTION PUBLIC DEFINITION Str_t getSysEncoding
            (
                void
            )
        {
			UErrorCode myError = U_ZERO_ERROR;
			UConverter *convSource = ucnv_open(NULL, &myError);
            if (myError != U_ZERO_ERROR) return "ENCODING-ERROR";
            
            Str_t aiana = ucnv_getName(convSource, &myError);
			if (myError != U_ZERO_ERROR) return "ENCODING-ERROR";
            
            return aiana;
        }

		/*
		 * Encoding Validos :
		 * http://www.iana.org/assignments/character-sets/character-sets.xml
		 */
        FUNCTION PUBLIC DEFINITION bool UTFToAny
            (                
                const char* in,
				const char* out_iana_encoding,
                Str_t& out
             )
        {
			if (strlen(in) == 0) return true;
			UErrorCode myError = U_ZERO_ERROR;
			UConverter *convSource = ucnv_open("UTF-8", &myError);
            if (myError != U_ZERO_ERROR) return false;            
                
			UConverter *convTarget = ucnv_open(out_iana_encoding, &myError);
			if (myError != U_ZERO_ERROR)
			{
				ucnv_close(convSource);
				return false;
			}
            
			uInt_t capacity = strlen(in)*4;
			char* target = new char[capacity];
			char* ptr_target = target;
            
			ucnv_convertEx(	convTarget, convSource,
                           &ptr_target, ptr_target+capacity,
                           &in, in+strlen(in),
                           NULL, NULL, NULL, NULL, TRUE, TRUE, &myError);
            
			if (myError != U_ZERO_ERROR)
			{
				if (target) delete[] target;
				ucnv_close(convSource);
				ucnv_close(convTarget);
				return false;
			}
            
			out.append(target);
			if (target) delete[] target;
			ucnv_close(convSource); 
			ucnv_close(convTarget); 
            
			return true;
        }


		/*
		 * Encoding Validos :
		 * http://www.iana.org/assignments/character-sets/character-sets.xml
		 */
        FUNCTION PUBLIC DEFINITION bool anyToUTF
            (
                const char* in_iana_encoding,
                const char* in,
                Str_t& out
             )
        {
			if (strlen(in) == 0) return true;
			UErrorCode myError = U_ZERO_ERROR;
			UConverter *convSource = ucnv_open(in_iana_encoding, &myError);
            if (myError != U_ZERO_ERROR) return false;            
                
			UConverter *convTarget = ucnv_open("UTF-8", &myError);
			if (myError != U_ZERO_ERROR)
			{
				ucnv_close(convSource);
				return false;
			}
            
			uInt_t capacity = strlen(in)*4;
			char* target = new char[capacity];
			char* ptr_target = target;
            
			ucnv_convertEx(	convTarget, convSource,
                           &ptr_target, ptr_target+capacity,
                           &in, in+strlen(in),
                           NULL, NULL, NULL, NULL, TRUE, TRUE, &myError);
            
			if (myError != U_ZERO_ERROR)
			{
				if (target) delete[] target;
				ucnv_close(convSource);
				ucnv_close(convTarget);
				return false;
			}
            
			out.append(target);
			if (target) delete[] target;
			ucnv_close(convSource); 
			ucnv_close(convTarget); 
            
			return true;
        }
    
		FUNCTION PUBLIC DEFINITION bool sysToUTF
			(
				const char* in,
				Str_t& out
			)
		{
			UErrorCode myError = U_ZERO_ERROR;
			UConverter *convSource = ucnv_open(NULL, &myError);
            if (myError != U_ZERO_ERROR) return false;
            
            const char* iana = ucnv_getName(convSource, &myError);
			if (myError != U_ZERO_ERROR) return false;
            if (strncmp(iana, "UTF-8", 5) == 0)
            {
                ucnv_close(convSource);
                out.append(in);
                return true;
            }
            

			UConverter *convTarget = ucnv_open("UTF-8", &myError);
			if (myError != U_ZERO_ERROR)
			{ 
				ucnv_close(convSource);
				return false; 
			}

			uInt_t capacity = strlen(in)*4;
			char* target = new char[capacity];
			char* ptr_target = target;

			ucnv_convertEx(	convTarget, convSource, 
				&ptr_target, ptr_target+capacity, 
				&in, in+strlen(in),
				NULL, NULL, NULL, NULL, TRUE, TRUE, &myError);	

			if (myError != U_ZERO_ERROR) 
			{
				if (target) delete[] target;
				ucnv_close(convSource); 
				ucnv_close(convTarget);
				return false;
			}

			out.append(target);
			if (target) delete[] target;
			ucnv_close(convSource); 
			ucnv_close(convTarget); 

			return true;	
		}

		FUNCTION PUBLIC DEFINITION bool UTFtoSys
			(
				const char* in,
				Str_t& out
			)
		{
			UErrorCode myError = U_ZERO_ERROR;
			UConverter *convSource = ucnv_open("UTF-8", &myError);
			if (myError != U_ZERO_ERROR) return false;

			UConverter *convTarget = ucnv_open(NULL, &myError);
			if (myError != U_ZERO_ERROR)
			{ 
				ucnv_close(convSource);
				return false; 
			}
            
            const char* iana = ucnv_getName(convTarget, &myError);
			if (myError != U_ZERO_ERROR) return false;
            if (strncmp(iana, "UTF-8", 5) == 0)
            {
                ucnv_close(convSource);
                ucnv_close(convTarget);
                out.append(in);
                return true;
            }

			uInt_t capacity = strlen(in)*4;
			char* target = new char[capacity];
			char* ptr_target = target;

			ucnv_convertEx(	convTarget, convSource, 
				&ptr_target, ptr_target+capacity, 
				&in, in+strlen(in),
				NULL, NULL, NULL, NULL, TRUE, TRUE, &myError);	

			if (myError != U_ZERO_ERROR) 
			{
				if (target) delete[] target;
				ucnv_close(convSource); 
				ucnv_close(convTarget);
				return false;
			}

			out.append(target);
			if (target) delete[] target;
			ucnv_close(convSource); 
			ucnv_close(convTarget); 

			return true;	
		}		

		FUNCTION PUBLIC DEFINITION Str_t CurrentDir
			(
				void
			)
		{
			char rawDir[FILENAME_MAX];
			GetCurrentDir(rawDir, FILENAME_MAX);			
			return std::string(rawDir);
		}

		FUNCTION PUBLIC DEFINITION size_t NumOfDirLevels
			(
				void
			)
		{
			std::vector<Str_t> dirs;
			Split(GetDirSep(), CurrentDir().c_str(), dirs);
			return dirs.size();
		}
        
        FUNCTION PUBLIC DEFINITION size_t NumOfDirLevels
			(
				const Char_t* path
			)
		{
			std::vector<Str_t> dirs;
			Split(GetDirSep(), path, dirs);
			return dirs.size();
		}
        

		FUNCTION PUBLIC DEFINITION Str_t DirLevel
			(
				const uInt_t& Level
			)
		{
			std::vector<Str_t> dirs;
			Str_t Dir;
			Split(GetDirSep(), CurrentDir().c_str(), dirs);
			if (Level > 0 && Level < dirs.size())			
			{
				Dir.clear();
				size_t i = 0;
				for
                    (
                        std::vector<Str_t>::iterator it = dirs.begin();
                        i < Level && it != dirs.end();
                        it++, i++
                     )
				{
					if (it == dirs.begin())
                    {
                        if (GetDirSep() == '/')
                            Dir.append(1, '/');
						Dir.append(*it);
                    }
					else
						Dir.append(GetDirSep() + *it);				
				}
			}
			return Dir;
		}
	
		FUNCTION PUBLIC DEFINITION Str_t DirLevel
			(
				const uInt_t& Level, 
				const Str_t& path
			)
		{
			std::vector<Str_t> dirs;
			Str_t Dir;
			Split(GetDirSep(), path.c_str(), dirs);
#           ifndef WIN
            if (dirs.size() == 1)
                return Str_t("/");
#           endif
			if (Level > 0 && Level < dirs.size())
			{
				Dir.clear();
				size_t i = 0;
                
				for
                    (
                        std::vector<Str_t>::iterator it = dirs.begin();
                        i < Level && it != dirs.end();
                        it++, i++
                     )
				{
					if (it == dirs.begin())
                    {
                        if (GetDirSep() == '/')
                            Dir.append(1, '/');
						Dir.append(*it);
                    }
					else
						Dir.append(GetDirSep() + *it);				
				}
			}
			return Dir;
		}

		FUNCTION PUBLIC DEFINITION Char_t GetDirSep
			(
				void
			)
		{			
			Str_t like_unix = CurrentDir().substr(0, 1);
			Str_t like_win  = CurrentDir().substr(1, 2);

			if ( like_unix.compare("/") == 0 )
				return '/';
			else if ( like_win.compare(":\\") == 0 ) 
				return '\\';
			
			return '#';

		}

		FUNCTION PUBLIC DEFINITION Str_t FixWinPath
			(
				const Str_t& path
			)
		{
			size_t len = path.size();
			if (len == 0) return "";
			Str_t n_path;
			n_path.resize(len+1);
			char* c_str = new char[len+1];		
			memset(c_str, 0, len+1);
			strncpy(c_str, path.c_str(), len);
			size_t x = 0;
			for(size_t i = 0; i < len; i++)
			{
				if (*(c_str+i) == '\\' && *(c_str+i+1) != '\\')
				{
					n_path.resize(n_path.size()+1);
					n_path[x++] = '\\';
				}
				n_path[x++] = *(c_str+i);
			}
			delete[] c_str;
			c_str = NULL;

			return n_path;
		}

		FUNCTION PUBLIC DEFINITION Str_t ExtractEXTENSION
			(
				const Str_t& path
			)
		{
			return path.substr(path.find_last_of(".")+1, path.size());
		}

        /**
        * Lista el contenido de un directorio en una ruta dada. Este metodo
        * se diseC1o para el uso en conjunto con un widget HTML de navegacion
        * por la estructura de directorios de un sistema.
        *
        * @param path Ruta del directorio a listar
        * @param tipo 'R': De la ruta pasada resta un nivel de profundidad
        *                  y lista el contenido del directorio.
        * @param files Vector de structura con el listado resultado
        *        Vfile_t.tipo : 'F' = Fichero 'D' = Directorio
        */
		FUNCTION PUBLIC DEFINITION bool ListForderForCtrl
			(
				const Char_t* path, 
				const Char_t tipo, 
				Vfile_t& files
			)
		{
			Str_t spath;
			if (tipo == 'R')
            {
                size_t level = NumOfDirLevels(path);
                if (level - 1 == 0)
                    level = 1;
                else if (level - 1 < 0)
                    goto list;
                else
                    level--;
				spath = DirLevel(level, path);
            }
        list:
			#ifdef _MSC_VER
                return ListFolderWIN((spath.empty())?path:spath.c_str(), files);
            #else
                return ListFolderUNIX((spath.empty())?path:spath.c_str(), files);
			#endif
		}
    
    /**
     * Lista el contenido de un directorio en una ruta dada
     *
     * @param path Ruta del directorio a listar
     * @param files Vector de structura con el listado resultado
     *        Vfile_t.tipo : 'F' = Fichero 'D' = Directorio
     */
    FUNCTION PUBLIC DEFINITION bool ListFolder
        (
            const Char_t* path,
            Vfile_t& files
         )
    {

        #ifdef _MSC_VER
            return ListFolderWIN(path, files);
        #else
            return ListFolderUNIX(path, files);
        #endif
    }

	FUNCTION PUBLIC DECLARATION const Str_t getDate
		(
			void
		)
	{
		time_t t = time(0);
		struct tm * now = localtime( & t );
		Buffer_t buf;
		buf << now->tm_year << now->tm_mon << now->tm_mday;
		return buf.str();
	}


	FUNCTION PUBLIC DECLARATION const Str_t getTimeStamp
		(
			void
		)
	{		
		Buffer_t buf;
		buf <<  getDate() << getTime();
		return buf.str();
	}
    
#ifdef _MSC_VER

	FUNCTION PUBLIC DECLARATION const Str_t getTime
		(
		void
		)
	{
		SYSTEMTIME st;
		::GetSystemTime(&st);
		Buffer_t buf;
		buf << st.wHour << st.wMinute << st.wSecond << st.wMilliseconds;
		return buf.str();
	}

		FUNCTION PUBLIC DEFINITION bool ListFolderWIN
			(
				const Char_t* path, 
				Vfile_t& files
			)
		{
			HANDLE hFind;
			WIN32_FIND_DATA data;
			
			// I - char* TO wchar_t* <== IN(path)
			size_t newsize = strlen(path) + 20;
			wchar_t * wcstring = new wchar_t[newsize];
			size_t convertedChars = 0;
			mbstowcs_s(&convertedChars, wcstring, newsize, path, _TRUNCATE);
			if ( convertedChars == 0 ) return false;
			newsize = convertedChars = 0;
			// F - char* TO wchar_t* ==> OUT(wcstring)

			StringCchCat(wcstring, MAX_PATH, TEXT("\\*"));
			hFind = FindFirstFile(wcstring, &data);
			if (hFind != INVALID_HANDLE_VALUE) 
			{
				char *nstring = NULL;
				size_t origsize = 0;
				newsize = 0;
				file_t hfile;
				do 
				{
					// I - wchar_t* TO char* <== IN(data.cFileName)
					origsize = wcslen(data.cFileName) + 20;						
					newsize = origsize * 2;								
					nstring = new char[newsize];
					wcstombs_s
                        (
                            &convertedChars,
                            nstring,
                            newsize,
                            data.cFileName,
                            _TRUNCATE
                         );
					if ( convertedChars == 0 ) return false;
					// F - wchar_t* TO char* ==> OUT(nstring)
					
					if (data.dwFileAttributes & FILE_ATTRIBUTE_ARCHIVE)
						hfile.tipo = "F";
					else if (data.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
						hfile.tipo = "D";
					hfile.name = nstring;
					if (hfile.name.compare(".") == 0)
						goto clear;
                    if (hfile.name.compare("..") != 0 && hfile.name[0] == '.')
                        goto clear;
					hfile.ruta = path;
					if (hfile.name.compare("..") != 0)
					{
						Char_t lastc = hfile.ruta[hfile.ruta.size()-1];
						if (lastc != utl::GetDirSep())
							hfile.ruta.append(1, utl::GetDirSep());
						hfile.ruta.append(hfile.name);
						lastc = '\0';
						lastc = hfile.ruta[hfile.ruta.size()-1];
						if (lastc != utl::GetDirSep() &&
                            hfile.tipo.compare("D") == 0)
                                hfile.ruta.append(1, utl::GetDirSep());
					}
                    else if (hfile.name.compare("..") == 0)
                    {
                        hfile.tipo = "R";
                    }
					files.push_back(hfile);
					clear:
						if (nstring) { delete[] nstring; nstring = NULL; }
						convertedChars = newsize = origsize = 0;
				} while (FindNextFile(hFind, &data));
				if (nstring) { delete[] nstring; nstring = NULL; }
			} else return false;
							
			//  LIMPIAR RECURSOS
			FindClose(hFind);
			if (wcstring) { delete[] wcstring; wcstring = NULL; }
			
			return true;
		}

		FUNCTION PUBLIC DEFINITION uInt_t StarTimeCount
			(
				void
			)
		{
			return GetTickCount();
		}

		FUNCTION PUBLIC DEFINITION uInt_t CalcElapsetTime
			(
				const uInt_t time_ms
			)
		{
			return  GetTickCount() - time_ms;
		}

		FUNCTION PUBLIC DEFINITION float msToSec
			(
				const uInt_t time_ms
			)
		{
			float ftime = static_cast<float>(time_ms);
			return  ftime / 1000;
		}

		FUNCTION PUBLIC DEFINITION const Char_t* getCurrEncoding
			(
				void
			)
		{
			return NULL;
		}
        
#else // LINUX & MAC

		FUNCTION PUBLIC DECLARATION const Str_t getTime
			(
				void
			)
		{			
			timeval time;
			time_t rawtime;
			struct tm * timeinfo;
			gettimeofday(&time, NULL);
			//long millis = (time.tv_sec * 1000) + (time.tv_usec / 1000);						
			timeinfo = localtime ( &rawtime );
			Buffer_t buf;
			buf << timeinfo->tm_hour << timeinfo->tm_min << time.tv_sec << time.tv_usec;
            
            return buf.str();
		}
        
        FUNCTION PUBLIC DEFINITION bool ListFolderUNIX
			(
				const Char_t* path, 
				Vfile_t& files
			)
        {
            DIR *pdir;
            struct dirent *pent;
            struct stat st;

            pdir=opendir(path);
            if (!pdir) return false;
            
            file_t hfile;
            std::string lpath;
            while ((pent=readdir(pdir)))
            {
                lpath.clear();
                lpath = path;
                if (lpath[lpath.size()-1] != hwt::utl::GetDirSep())
                    lpath.append(1, hwt::utl::GetDirSep());
                lpath.append(pent->d_name);
                memset(&st, 0, sizeof(struct stat));
                lstat(lpath.c_str(), &st);
                if(S_ISDIR(st.st_mode))
                    hfile.tipo = "D";
                else
                    hfile.tipo = "F";
                hfile.name = pent->d_name;
                if (hfile.name.compare(".") == 0)
                    continue;
                if (hfile.name.compare("..") != 0 && hfile.name[0] == '.')
                    continue;
                hfile.ruta = path;
                if (hfile.name.compare("..") != 0)
                {
                    Char_t lastc = hfile.ruta[hfile.ruta.size()-1];
                    if (lastc != hwt::utl::GetDirSep())
                        hfile.ruta.append(1, hwt::utl::GetDirSep());
                    hfile.ruta.append(hfile.name);
                    lastc = '\0';
                    lastc = hfile.ruta[hfile.ruta.size()-1];
                    if (lastc != hwt::utl::GetDirSep() &&
                        hfile.tipo.compare("D") == 0)
                            hfile.ruta.append(1, hwt::utl::GetDirSep());
                }
                else if (hfile.name.compare("..") == 0)
                {
                    hfile.tipo = "R";
                }
                files.push_back(hfile);
                
            }

            closedir(pdir);
            
            return true;
        }

        // tiempo en usec
		FUNCTION PUBLIC DEFINITION const timeval StarTimeCount
			(
				void
			)
		{
            timeval t1;
            gettimeofday(&t1, NULL);
			return t1;
		}

		FUNCTION PUBLIC DEFINITION double CalcElapsetTime
			(
				const timeval t1
			)
		{
            timeval t2;
            gettimeofday(&t2, NULL);
            double diff = static_cast<float>(t2.tv_usec - t1.tv_usec);
            
            return diff;
            
		}

		FUNCTION PUBLIC DEFINITION double usecToSec
			(
				const double time_usec
			)
		{
			return  time_usec / 1000000.0;
		}
    
        FUNCTION PUBLIC DEFINITION double msToSec
            (
                const double time_ms
            )
        {
            return  time_ms / 1000.0;
        }

#endif


	FUNCTION PUBLIC DEFINITION void DoFormatting
		(
			Str_t& sF, 
			const Char_t* sformat, 
			va_list marker
		)
	{
		Char_t* s, ch=0;
        int n = 0;
		size_t i=0, m = 0;
		long l;
		double d;
		Str_t sf = sformat;
		Buffer_t ss;

		m = sf.length();
		while (i<m)
		{
			ch = sf.at(i);
			if (ch == '%')
			{
				i++;
				if (i<m)
				{
					ch = sf.at(i);
					switch(ch)
					{
						case 's':
                        {
                            s = va_arg(marker, char*);  ss << s;
                        } break;
						case 'c':
                        {
                            n = va_arg(marker, int);    ss << (char)n;
                        } break;
						case 'd':
                        {
                            n = va_arg(marker, int);    ss << (int)n;
                        } break;
						case 'l':
                        {
                            l = va_arg(marker, long);   ss << (long)l;
                        } break;
						case 'f':
                        {
                            d = va_arg(marker, double); ss << (float)d;
                        } break;
						case 'e':
                        {
                            d = va_arg(marker, double); ss << (double)d;
                        } break;
						case 'X':
						case 'x':
							{
								if (++i<m)
								{
									ss  << std::hex
                                        << std::setiosflags
                                            (
                                                std::ios_base::showbase
                                            );
                                    
									if (ch == 'X')
                                        ss << std::setiosflags
                                                (
                                                    std::ios_base::uppercase
                                                 );
                                    
									char ch2 = sf.at(i);
									if (ch2 == 'c')
                                    {
                                        n = va_arg(marker, int);
                                        ss << std::hex << (char)n;
                                    }
									else if (ch2 == 'd')
                                    {
                                        n = va_arg(marker, int);
                                        ss << std::hex << (int)n;
                                    }
									else if (ch2 == 'l')
                                    {
                                        l = va_arg(marker, long);
                                        ss << std::hex << (long)l;
                                    }
									else ss << '%' << ch << ch2;
									ss << std::resetiosflags
                                            (
                                                std::ios_base::showbase |
                                                std::ios_base::uppercase
                                             )
                                        << std::dec;
								}
							} break;
						case '%': { ss << '%'; } break;
						default:
						{
							ss << "%" << ch;
							//i = m; //get out of loop
						}
					}
				}
			}
			else ss << ch;
			i++;
		}
		va_end(marker);
		sF = ss.str();
	}

	FUNCTION PUBLIC DEFINITION void Stringf
		(
			Str_t& stgt, 
			const Char_t* sformat, 
			... 
		)
	{
		va_list marker;
		va_start(marker, sformat);
		DoFormatting(stgt, sformat, marker);
	}

	FUNCTION PUBLIC DEFINITION Str_t Stringf
		(
			const Char_t* sformat,
			... 
		)
	{
		Str_t stgt;
		va_list marker;
		va_start(marker, sformat);
		DoFormatting(stgt, sformat, marker);

		return stgt;

	}    

	FUNCTION PUBLIC DEFINITION bool ReadFullTxFile	
		(
			const Char_t* path, 
			Str_t& file
		)
	{
		return ReadFullTxFile(Str_t(path), file);
	}

	FUNCTION PUBLIC DEFINITION bool ReadFullBinFile
		(
			const Char_t* path, 
			Str_t& file
		)
	{
		return ReadFullBinFile(Str_t(path), file);
	}

	FUNCTION PUBLIC DEFINITION bool SaveFullTxFile
		(
			const Char_t* path, 
			const Str_t& content
		)
	{
		return SaveFullTxFile(Str_t(path), content);
	}

	FUNCTION PUBLIC DEFINITION bool SaveFullTxFile	
		(
			const Str_t& path, 
			const Str_t& content
		)
	{
		std::ofstream of;
		of.open(utl::FixWinPath(path).c_str());
		if (of.is_open())
		{
			of.write(content.c_str(), content.size());
		} else return false;

		return true;
	}


	FUNCTION PUBLIC DEFINITION bool ReadFullTxFile	
		(
			const Str_t& path, 
			Str_t& file
		)
	{
		std::ifstream fileInput;
		fileInput.open( utl::FixWinPath(path).c_str() );
		if (fileInput.is_open())
		{            
			Str_t line;
			fileInput.seekg(0, std::ios::beg);
			Buffer_t ss;
			ss << fileInput.rdbuf();
			file = ss.str();
			fileInput.close();
			if (file.size() == 0) return false;

		} else return false;

		return true;
	}

	FUNCTION PUBLIC DEFINITION bool ReadFullBinFile
		(
			const Str_t& path, 
			Str_t& file
		)
	{
		std::ifstream BinFile
            (
                utl::FixWinPath(path).c_str(),
                std::ios::in | std::ios::binary | std::ios::ate
             );
        
		std::ifstream::pos_type fileSize = 0;
		Char_t* fileContents = NULL;
		if (BinFile.is_open())
		{
			fileSize = BinFile.tellg();
			fileContents = new char[static_cast<long>(fileSize)];
			BinFile.seekg(0, std::ios::beg);
			if (
                !BinFile.read
                    (
                        fileContents,
                        static_cast<std::streamsize>(fileSize)
                     )
                )
			{
				delete[] fileContents;
				fileContents = NULL;
				return false;
			}
			BinFile.close();
			file.append(fileContents, static_cast<size_t>(fileSize) );
			delete[] fileContents;
			fileContents = NULL;		

		} else return false;

		return true;    		
	}

	FUNCTION PRIVATE DEFINITION static void recListFilesOnFolder
		(
		const hwt::Char_t* path,
		hwt::Vfile_t& fileList,		
		hwt::Vfile_t& FolderItemList,
		hwt::Vfile_t::iterator begin
		)
	{
		for (
			hwt::Vfile_t::iterator it = begin; 
			it != FolderItemList.end(); 
		it++
			)
		{

			if (0 == it->tipo.compare("D"))
			{
				hwt::Vfile_t new_FolderItemList;
				hwt::utl::ListFolder ( it->ruta.c_str(), new_FolderItemList);
				recListFilesOnFolder
					(
					path, 
					fileList, 
					new_FolderItemList, 
					new_FolderItemList.begin()
					);
			}
			else if(0 == it->tipo.compare("F"))
			{	        
				fileList.push_back(*it);
			}
		}

	}
	FUNCTION PUBLIC DEFINITION void performCmd
		(
			const Str_t& cmd
		)
	{
		Str_t lcmd(cmd);
		#ifdef _MSC_VER			
			std::ofstream FileOutput;			
			FileOutput.open("cmd.bat");
			FileOutput << "@ECHO OFF" << std::endl;
			FileOutput << lcmd << std::endl;
			FileOutput << "EXIT /B";
			FileOutput.close();		
			lcmd = "cmd.bat";
		#endif
		system(lcmd.c_str());

	}

	FUNCTION PUBLIC DEFINITION void listAllFilesOnFolder
		(
			const hwt::Char_t* path,
			hwt::Vfile_t& fileList
		)
	{
		hwt::Vfile_t FolderItemList;
		hwt::utl::ListFolder( path, FolderItemList );
		recListFilesOnFolder
			(
				path, 
				fileList, 
				FolderItemList, 
				FolderItemList.begin()
			);
	}	

}
}

