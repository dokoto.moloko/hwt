#include "checkboxGroup.hpp"

namespace hwt
{
    
    CONSTRUCTOR PUBLIC DEFINITION checkboxGroup::checkboxGroup
        (
            sessionWidgets_t* sessionwidgets,
            const Char_t* aianaencoding,
            const uInt_t father,
            const Str_t& uniquename,
            const Str_t& labeltx,
            const uInt_t width,
            const uInt_t height,
            const bool visible,
            const bool disable
        )
    
	:	widget(father, uniquename, labeltx, width, height, CHECKBOXGROUP, visible, disable, aianaencoding ),
        SessionWidgets(sessionwidgets)
        {
        }
    
    DESTRUCTOR PUBLIC DEFINITION checkboxGroup::~checkboxGroup
        (
            void
        )
        {
            #ifdef VERBOSE
            infoln(utl::Stringf(
                                "Releasing CheckBoxGroup with %l pointers to checkbox classes.",
                                Checkbox_list.size()));
            #endif
            for (std::vector<checkbox*>::iterator it = Checkbox_list.begin();
                 it != Checkbox_list.end();
                 it++)
            {
                if (*it != NULL)
                {
                    #ifdef VERBOSE
                    infoln(utl::Stringf("Delete checkbox Uuid : %l UniqueName : %s from his checkboxGroup.",
                                        (*it)->Uuid,
                                        (*it)->UniqueName.c_str()));
                    #endif                    
                    delete *it;
                    *it = NULL;
                }
            }
        }
    
    
    METHOD PUBLIC DEFINITION checkbox* checkboxGroup::Add
        (
            const Str_t& uniquename,
            const Str_t& labeltx,
            const bool checked,
            const uInt_t width,
            const uInt_t height,
            const bool visible,
            const bool disable
        )
    {
        widget* cb = new checkbox(SessionWidgets, AianaEncoding.c_str(), Uuid, uniquename, labeltx, width, height, visible, disable, checked);
        Checkbox_list.push_back(static_cast<checkbox*>(cb));
        return  static_cast<checkbox*>(cb);
    }
    
    METHOD PUBLIC DEFINITION Str_t checkboxGroup::toHtml
        (
            bool
        ) const
    {
        Buffer_t buf;
        buf << utl::Stringf(chtml::td_id_open, Uuid);
		for (std::vector<checkbox*>::const_iterator it = Checkbox_list.begin();
             it != Checkbox_list.end();
             it++)
        {
            buf << (*it)->toHtml();
        }
        buf << chtml::td_close;
        
        return buf.str();
    }
    
	METHOD PUBLIC DEFINITION const Str_t checkboxGroup::toJavaScript
        (
            bool
        ) const
    {
		Buffer_t buf;
		buf << utl::Stringf(cjs::intCheckBox, Uuid);  
		for (std::vector<checkbox*>::const_iterator it = Checkbox_list.begin();
			it != Checkbox_list.end();
			it++)
			buf << utl::Stringf(cjs::event_click, (*it)->Uuid, (*it)->Uuid, objets[(*it)->Type]);
		return buf.str();
	}
    
	/*
    METHOD PUBLIC DEFINITION void checkboxGroup::toJScbValues
        (
            Buffer_t& buf
        ) const
    {
        for (std::vector<checkbox*>::const_iterator it = Checkbox_list.begin();
             it != Checkbox_list.end();
             it++)
            buf << utl::Stringf(cjs::checkbox_key_value_pair, (*it)->Uuid, (*it)->Uuid);
    }
    */
}
