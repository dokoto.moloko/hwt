// SOURCE CHARSET ENCODING FOR WINDOWS   : US-ASCII
// SOURCE CHARSET ENCODING FOR LINUX, OS : ASCII

#include "grid.hpp"

namespace hwt
{
	CONSTRUCTOR PUBLIC DEFINITION grid::grid
		(	
			sessionWidgets_t* sessionwidgets,
            const Char_t* aianaencoding,
			const uInt_t father,
			const Str_t& uniquename,
			const Str_t& labeltx, 			
			const uInt_t width, 
			const uInt_t height,
			const bool visible,
			const bool disable           
		) 

		:	widget(father, uniquename, labeltx, width, height, GRID, visible, disable, aianaencoding ),
			SessionWidgets(sessionwidgets),
			Stream(NULL)
	{		
	}

	DESTRUCTOR PUBLIC DEFINITION grid::~grid
		(
			void
		)
	{
		if(Stream != NULL)
		{
			delete[] Stream;
			Stream = NULL;
		}
	}

	METHOD PUBLIC DEFINITION Str_t grid::toHtml
		(
			bool
		) const
	{
		Buffer_t buf;
		Str_t utf8;
		buf << utl::Stringf(chtml::dataTable_open, Uuid);
		for(std::vector<fieldCatalog_t*>::const_iterator itField = fieldCatOrder.begin();
			itField != fieldCatOrder.end();
			itField++)
		{
			if (!utl::anyToUTF(AianaEncoding.c_str(), (*itField)->headerName.c_str(), utf8))
				errorln("Charset to UTF-8 conversion has failed.");
			buf << utl::Stringf(chtml::dataTable_header, utf8.c_str());
			utf8.clear();
		}
		buf << chtml::dataTable_close;
		return buf.str();
	}

	METHOD PUBLIC DEFINITION const Str_t grid::toJavaScript
		(
			bool
		) const
	{		
		Buffer_t buf;
		buf << utl::Stringf(cjs::grid_select_click, Uuid, Uuid, Uuid);
		buf << utl::Stringf(cjs::dataTable_open, Uuid, Uuid, 10, Uuid);
		return buf.str();
	}

	METHOD PUBLIC DECLARATION void grid::clearContent
		(
			void
		)
	{
		Content.clear();
		lineContent.clear();
		if(Stream != NULL)
		{
			delete[] Stream;
			Stream = NULL;
		}
	}

	METHOD PUBLIC DECLARATION void grid::loadFromString
		( 
			const Char_t* str
		)
	{
		if (str == NULL)
			errorln("In param 'str' is NULL.");
		size_t len = strlen(str);
		Stream = new Char_t[len+10];
		memset(Stream, 0, len+10);
		memcpy(Stream, str, len);
	}

	METHOD PUBLIC DECLARATION void grid::toJsonFromPlainText
		(
			Str_t& stream
		) 
	{
		if(Stream == NULL)
		{
			infoln("Se solicita XXXX_grid.txt pero como es NULL no se retorna nada [NO ME GUSTA BUSCAR MEJOR SOLUCION]");
			return;		
		}
		Buffer_t buf;
		buf << "{ \"aaData\": [";
		size_t i = 0;
		Char_t* ptr = NULL;
		while( *(ptr = (Stream+i) ) != '\0')
		{
			if (i == 0) 
				buf << "[\"";

			if (*ptr == '\t')
			{
				buf << "\",\"";
			}
			else if ( *(ptr+1) == '\0' )
			{
				buf << "\"]";
			}
			else if (*ptr == '\n')
			{
				buf << "\"],[\"";
			}
			else
			{
				buf << *ptr;
			}

			i++;
		}

		buf << "] }";
		if(Stream != NULL)
		{
			delete[] Stream;
			Stream = NULL;
		}
		stream.append(buf.str());
		buf.str("");
	}

	METHOD PUBLIC DECLARATION void grid::toUtf8JsonFromPlainText
		(
			Str_t& stream
		) 
	{		
		Str_t utf8;
		toJsonFromPlainText(stream);		
		if (!utl::anyToUTF(AianaEncoding.c_str(), stream.c_str(), utf8))
			errorln("Charset to UTF-8 conversion has failed.");
		stream.clear();
		stream.append(utf8);
	}

	METHOD PUBLIC DECLARATION void grid::toUtf8Json
		(
			Str_t& stream
		) const
	{
		Str_t utf8;
		toJson(stream);		
		if(!utl::anyToUTF(AianaEncoding.c_str(), stream.c_str(), utf8))
			errorln("Charset to UTF-8 conversion has failed.");
		stream.clear();
		stream.append(utf8);
	}

	METHOD PUBLIC DECLARATION void grid::toJson
		(
			Str_t& stream
		) const
	{
		Buffer_t buf;
		buf << "{ \"aaData\": [";
		std::vector<std::map<Str_t, Str_t> >::const_iterator itLine;
		std::map<Str_t, fieldCatalog_t>::const_iterator itFcat;
		itLine = Content.begin();
		while(itLine != Content.end())			
		{
			buf << "[";
			itFcat = fieldCat.begin();
			while(itFcat != fieldCat.end())
			{
				buf << "\"";
				if (itLine->find(itFcat->first)  == itLine->end())
					errorln(utl::Stringf("Grid FieldCatalog differs of Content columns, the field %d not exist.", itFcat->first.c_str()));				
				std::map<Str_t, Str_t>::const_iterator itValue = itLine->find(itFcat->first);
				buf << itValue->second;
				buf << "\"";
				if (++itFcat != fieldCat.end())
					buf << ",";
			}			
			buf << "]";
			if (++itLine != Content.end())
				buf << ",";
		}

		buf << "] }";
		stream.append(buf.str());
	}


	METHOD PUBLIC DECLARATION void grid::addFieldHeaderDesc
		(
			const Char_t* name,
			const Char_t* type,
			const uInt_t  len,
			const Char_t* headerName
		)
	{
		fieldCatalog_t fcat = {type, len, headerName};
		fieldCat[name] = fcat;
		fieldCatOrder.push_back(&fieldCat[name]);
	}

	METHOD PUBLIC DECLARATION void grid::addFieldValueToContent
		(
			const Char_t* name,
			const Char_t* value	
		)
	{
		lineContent[name] = value;
	}

	METHOD PUBLIC DECLARATION void grid::addLineContent
		( 
			void
		)
	{
		Content.push_back(lineContent);
		lineContent.clear();
	}
}