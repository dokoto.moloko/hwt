// SOURCE CHARSET ENCODING FOR WINDOWS   : US-ASCII
// SOURCE CHARSET ENCODING FOR LINUX, OS : ASCII

#ifndef _WIDGET_HPP
#define _WIDGET_HPP

#include "types.hpp" 
#include "utils.hpp"

namespace hwt
{
	class session;
	class widget
	{
		private:
			COPY_CONSTRUCTOR PRIVATE DEFINITION widget
				(
					const widget& w
				) :	Uuid(reinterpret_cast<uInt_t>(&w)),					
					Father(w.Father),
					UniqueName(w.UniqueName),
					Width(w.Width),
					Height(w.Height),
					Type(w.Type),
					Visible(w.Visible),
                    Disable(w.Disable),
                    AianaEncoding(w.AianaEncoding),
                    Labeltx(w.Labeltx){}

			OPERATOR PRIVATE DEFINITION widget& operator=
				(
					const widget&
				) { return *this; }

		public:
			const uInt_t Uuid;			
			const uInt_t Father;		
			Str_t UniqueName;			
			uInt_t Width, Height;		
			const objets_t Type;
			bool Visible;
			bool Disable;
            Str_t AianaEncoding;
			friend class session;

		protected:
			Str_t Labeltx;

		private:
			std::map<Str_t, ptrParams_t> EventPool;
			std::map<Str_t, scope_t> EventPoolScope;	

		public: // Constructor

			CONSTRUCTOR PUBLIC DEFINITION widget
				(
					const uInt_t father,
					const Str_t& uniquename,
					const Str_t& labeltx, 
					const uInt_t width, 
					const uInt_t height, 
					objets_t type,
					const bool visible,
					const bool disable,
                    const Char_t* aianaencoding
				);											
														 
			VIRTUAL DESTRUCTOR PUBLIC DEFINITION ~widget
				(
					void
				);

		public: // Methods

			VIRTUAL METHOD PUBLIC DEFINITION Str_t toHtml
				(
					bool
				) const { return ""; };

			VIRTUAL METHOD PUBLIC DEFINITION Str_t toHtml
				(
					void
				) { return ""; };

            VIRTUAL METHOD PUBLIC DECLARATION void AddEvent
                (
                    const ptrParams_t ptrF,
                    events_t event,
					scope_t scope
				);

			VIRTUAL METHOD PUBLIC DECLARATION void AddEvent
				(
					const ptrParams_t ptrF,
					events_t event,
					scope_val_t scope = WINDOW
				);
        
			VIRTUAL METHOD PUBLIC DEFINITION const Str_t getconstLabeltx
				(
					void
				) const;

			VIRTUAL METHOD PUBLIC DECLARATION const Str_t getconstUtf8Labeltx
				(
					void
				) const;

			VIRTUAL METHOD PUBLIC DECLARATION void setLabeltx
				(
					const Str_t& value
				);

            METHOD PUBLIC DECLARATION void executeEvent
                (
                    scope_val_t event,
                    const params_t& params,
                    session& ss
                 );
        
            METHOD PUBLIC DECLARATION void executeEvent
                (
                    scope_val_t event,
                    const params_t& params,
                    session& ss
                ) const;


			METHOD PUBLIC DECLARATION void executeEvent
				(
					const Char_t* eventName,
					const params_t& params,
					session& ss
				) const;


			METHOD PUBLIC DECLARATION const std::map<Str_t, ptrParams_t>& getconstEvents
				(
					void
				) const;

			METHOD PUBLIC DECLARATION scope_val_t getconstEventScope
				(
					events_t event
				) const;

			METHOD PUBLIC DECLARATION const scope_t& getconstEventScopeFull
				(
					events_t event
				) const;


			

	};

}

#endif //_WIDGET_HPP

