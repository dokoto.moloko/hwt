// SOURCE CHARSET ENCODING FOR WINDOWS   : US-ASCII
// SOURCE CHARSET ENCODING FOR LINUX, OS : ASCII

#include "session.hpp"
 
namespace hwt
{

	CONSTRUCTOR PUBLIC DEFINITION session::session
		(
			const Char_t* weblibrarypath,
			mode_t logreportingmode,
            const Char_t* aianaencoding,
			uInt_t port, 
			uInt_t timeoutseg            
		):	WebLibraryPath(weblibrarypath),		
			LogReportingMode(logreportingmode),
			Port(port),
			TimeOutSeg(timeoutseg),	
			Uuid(reinterpret_cast<uInt_t>(this)),
			turnON(false),
            Header(NULL)
	{
		std::locale::global(std::locale("C"));
		msg::config(utl::getTimeStamp().c_str(), LogReportingMode);
        AianaEncoding.append( (aianaencoding == NULL) ? utl::getSysEncoding() : aianaencoding );
        Header = new header(this->AianaEncoding.c_str());
	}

	DESTRUCTOR PUBLIC DEFINITION session::~session
		(
			void
		)
	{
		#ifdef VERBOSE	
		shutdownln
			(
				utl::Stringf (
				"Releasing %l pointers to document class", 
				SessionWidgets.documents.size()
				)
			);
		#endif

		for 
			(
				documents_t::iterator it = SessionWidgets.documents.begin();
				it != SessionWidgets.documents.end();
				it++
			)
		{			
			if (it->second != NULL) 
			{
				delete it->second;
				it->second = NULL;
			}			
		}		
		SessionWidgets.documents.clear();		
		msg::close();
        if (Header != NULL)
            delete Header;
	}

	METHOD PUBLIC DEFINITION uInt_t session::AddDocument
		(
			const Str_t& uniquename,
			const Str_t& title, 
			const Str_t& description, 
			const uInt_t width, 
			const uInt_t height, 
			const bool visible
		) 
	{		
        document* doc = new document
			(
				&SessionWidgets, 				
				this->Uuid, 
				AianaEncoding.c_str(),
				uniquename, 
				title, 
				description, 
				width, 
				height, 
				visible                
			);
		
		SessionWidgets.byId[doc->Uuid] = doc;
		SessionWidgets.byName[doc->UniqueName] = doc;
		SessionWidgets.documents[doc->Uuid] = doc;

		if (SessionWidgets.documents.size() == 1)
		{
			main = doc->Uuid;
			doc->onTop = true;
		}
		return doc->Uuid;
	}

	METHOD PUBLIC DECLARATION void session::DelDocument
		(
			const uInt_t uuid
		)
	{
		document* doc = getDocById(uuid);
		if (doc)
		{
			delete doc;
			doc = NULL;
		}
	}

	METHOD PUBLIC DECLARATION void session::DelDocument
		(
			const Str_t& uniquename
		)
	{
		document* doc = getDocByName(uniquename);
		if (doc)
		{
			delete doc;
			doc = NULL;
		}
	}

	METHOD PUBLIC DEFINITION uInt_t session::NumOfDocuments
		(
			void
		) const
	{
		return SessionWidgets.documents.size();
	}

	METHOD PUBLIC DEFINITION void session::DoDeploy
		(
			void
		)
	{
		deploy dp(this, &SessionWidgets);
	}

	METHOD PUBLIC DEFINITION const Str_t session::toHtml
		(
			void
		) const
	{
		Buffer_t buf;
		buf << chtml::header_html;
		buf << chtml::html_open;
		buf << Header->toHtmlWithHeader();
		buf << chtml::body_open;
		buf << chtml::fexp_Html_div;
        buf << chtml::popup_warn_close;
		for(
                documents_t::const_iterator DocIt = SessionWidgets.documents.begin();
                DocIt != SessionWidgets.documents.end();
                DocIt++
            )
		{
			buf << DocIt->second->toHtmlWithDiv();
		}
		buf << chtml::body_close;
		buf << chtml::html_close;

		return buf.str();
	}

	METHOD PUBLIC DEFINITION const Str_t session::toJavaScript
		(
			void
		) const
	{
		Buffer_t buf;
		buf << cjs::var_Map;
		for (grid_t::const_iterator it = SessionWidgets.w.grids.begin();
			it != SessionWidgets.w.grids.end();
			it++)
		{
			buf << utl::Stringf(cjs::grid_var, it->first);
		}
		buf << cjs::arrayToJson;
		buf << cjs::grid_func_get_properties;		
		buf << cjs::fexp_getFileList;
		buf << cjs::fexp_StringToXML;
		buf << cjs::fexp_fixWinPath;
		buf << cjs::fexp_OnClickLink;
		buf << cjs::open_ready;
        buf << utl::Stringf("$('#%s').hide();\n", "dialog-confirm");				
		
		for(
                documents_t::const_iterator DocIt = SessionWidgets.documents.begin();
                DocIt != SessionWidgets.documents.end();
                DocIt++
            )
		{
			// Se ocultan las pantallas o documentos que no son el principal
			// Se declaran todas las pantallas para poder usar onTop en vez de Open
			// $('#document_id_2').hide();
			if (DocIt->second->Width == 0 && DocIt->second->Height == 0)
				buf << utl::Stringf( cjs::initMainDocumentAuto, DocIt->second->Uuid );
			else
			{
				buf << utl::Stringf
					(
						cjs::initDocument,
						main,
						DocIt->second->Width,
						DocIt->second->Height
					);
			}		
			if (DocIt->second->Visible == false)
				buf << utl::Stringf(cjs::div_to_close, DocIt->second->Uuid);	

			if( DocIt->second->getconstEvents().find(events[CLOSE_WIN]) != DocIt->second->getconstEvents().end())
			{
				// Init click event
				buf << utl::Stringf(cjs::close_win_begin, DocIt->second->Uuid, DocIt->second->Uuid,
					utl::Stringf(cjs::dynmic_func_call, DocIt->second->Uuid).c_str());
			}

            for
                (
                    std::vector<line*>::iterator itLine = DocIt->second->vLine.begin();
                    itLine != DocIt->second->vLine.end();
                    itLine++
                 )
            {
                   for (
					   byId_t::iterator itWidget = (*itLine)->LineWidgets.byId.begin();                          
                        itWidget != (*itLine)->LineWidgets.byId.end();
                        itWidget++
                        )
                   {
					tl::PRE_updateWidgetValues(itWidget->second, buf);
                   }
            }
        }

        // Se fija el evento de cierre de aplicacion
        buf << utl::Stringf( cjs::close_app, main, main, events[CLOSE_APP], CloseAllWindows().c_str(), main );
        
		buf << cjs::close_ready;

		return buf.str();
	}

    METHOD PRIVATE DEFINITION Str_t session::CloseAllWindows
        (
            void
        ) const
    {
        Buffer_t buf;
		for(
            documents_t::const_iterator DocIt = SessionWidgets.documents.begin();
            DocIt != SessionWidgets.documents.end();
            DocIt++
            )
		{            
            buf << utl::Stringf( cjs::div_to_destroy, DocIt->second->Uuid );
        }
        return buf.str();
    }
    
	METHOD PUBLIC DEFINITION bool session::isDocOnTop
		(
			const uInt_t uuid
		) const
	{
		documents_t::const_iterator DocIt = SessionWidgets.documents.find(uuid);
		if (DocIt == SessionWidgets.documents.end())			
			errorln(utl::Stringf("Uuid : %l not found.", uuid));

		return DocIt->second->isOnTop();
	}

	METHOD PUBLIC DECLARATION void session::setDocUnTop
		(
			const Str_t& uniquename
		)
	{
		setDocUnTop(getDocByName(uniquename)->Uuid);
	}

	METHOD PUBLIC DEFINITION void session::setDocUnTop
		(
			const uInt_t uuid
		)
	{
		getDocById(uuid)->setOnTop(false);
	}

	METHOD PUBLIC DECLARATION void session::setDocOnTopAndUnTopRest
		(
			const Str_t& uniquename
		)
	{
		setDocOnTopAndUnTopRest(this->getDocByName(uniquename)->Uuid);
	}

	METHOD PUBLIC DEFINITION void session::setDocOnTopAndUnTopRest
		(
			const uInt_t uuid
		)
	{
		documents_t::const_iterator DocIt = SessionWidgets.documents.find(uuid);
		if (DocIt == SessionWidgets.documents.end())
			errorln(utl::Stringf("Uuid : %l not found.", uuid));

		for(
			documents_t::const_iterator DocIt = SessionWidgets.documents.begin();
			DocIt != SessionWidgets.documents.end();
			DocIt++
			)
		{
			if (DocIt->second->Uuid == uuid)
				DocIt->second->setOnTop(true);
			else
				DocIt->second->setOnTop(false);
		}	
	}


	METHOD PUBLIC DEFINITION document* session::getDocById
		(
			const uInt_t uuid
		)
	{
        return const_cast<document*>(getconstDocById(uuid));
	}

	METHOD PUBLIC DEFINITION const document* session::getconstDocById
		(
			const uInt_t uuid
		) const
	{
        documents_t::const_iterator DocIt = SessionWidgets.documents.find(uuid);
		if (DocIt == SessionWidgets.documents.end())
			errorln(utl::Stringf("Uuid : %l not found.", uuid));
        
		return DocIt->second;		
	}

	METHOD PUBLIC DEFINITION document* session::getDocByInd
		(
			const size_t index
		)
	{
        return const_cast<document*>(getconstDocByInd(index));
	}

	METHOD PUBLIC DEFINITION const document* session::getconstDocByInd
		(
			const size_t index
		) const
	{		
		if (index == 0)
			errorln("Index is 0.");
        
		if ((index - 1) > SessionWidgets.documents.size())
			errorln(utl::Stringf("Index : %l out of range", index));
        
		documents_t::const_iterator DocIt = SessionWidgets.documents.begin();
		std::advance(DocIt, index);
		return DocIt->second;
	}


	METHOD PUBLIC DEFINITION document* session::getDocByName
		(
			const Str_t& uniquename
		)
	{
		return const_cast<document*>(getconstDocByName(uniquename));
	}

	METHOD PUBLIC DEFINITION const document* session::getconstDocByName
		(
			const Str_t& uniquename
		) const
	{
		byName_t::const_iterator it = SessionWidgets.byName.find(uniquename);
		if (it == SessionWidgets.byName.end())
			errorln(utl::Stringf("UnKnown Document Unique Name: %s", uniquename.c_str()));
        
		return static_cast<document*>(it->second);
	}


	METHOD PUBLIC DECLARATION widget* session::getWidgetByName
		(
			const Str_t& uniquename
		)
	{
        return const_cast<widget*>(getconstWidgetByName(uniquename));
	}

	METHOD PUBLIC DECLARATION const widget* session::getconstWidgetByName
		(
			const Str_t& uniquename
		) const
	{
		byName_t::const_iterator it = SessionWidgets.byName.find(uniquename);
		if (it == SessionWidgets.byName.end())
			errorln(utl::Stringf("UnKnown Widget Unique Name: %s", uniquename.c_str()));
        
		return it->second;		
	}

	METHOD PUBLIC DECLARATION widget* session::getWidgetById
		(
			const uInt_t uuid
		)
	{
		return const_cast<widget*>(getconstWidgetById(uuid));
	}

	METHOD PUBLIC DECLARATION const widget* session::getconstWidgetById
		(
			const uInt_t uuid
		) const
	{
		byId_t::const_iterator it = SessionWidgets.byId.find(uuid);
		if (it == SessionWidgets.byId.end())
			errorln(utl::Stringf("Uuid : %l not found.", uuid));
        
		return it->second;
	}

	METHOD PUBLIC DEFINITION document* session::getMainDoc
		(
			void
		) 
	{ 
        return const_cast<document*>(getconstDocById(main));
	}

	METHOD PUBLIC DEFINITION const document* session::getconstMainDoc
		(
			void
		) const 
	{
        return getconstDocById(main);
	}

	METHOD PUBLIC DEFINITION uInt_t session::getUuidOfMainDoc
		(
			void
		)const { return main; }
    
	METHOD PUBLIC DEFINITION bool session::isTurnON
        (
            void
         )const { return turnON; }

	METHOD PUBLIC DEFINITION sessionWidgets_t& session::getSessionWidgets
		(
			void
		)
	{
		return SessionWidgets;
	}

	METHOD PUBLIC DEFINITION const sessionWidgets_t& session::getconstSessionWidgets
		(
			void
		) const
	{
		return SessionWidgets;
	}

	// NO COPY IS line is session reference is destroy also.
	METHOD PUBLIC DEFINITION documents_t& session::getDocuments
		(
			void
		)
	{
		return SessionWidgets.documents;
	}

	METHOD PUBLIC DEFINITION const documents_t& session::getconstDocuments
		(
			void
		) const
	{
		return SessionWidgets.documents;
	}
}

