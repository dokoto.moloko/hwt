#include "checkbox.hpp" 

namespace hwt
{
	CONSTRUCTOR PUBLIC DEFINITION checkbox::checkbox
		(	
			sessionWidgets_t* sessionwidgets,
            const Char_t* aianaencoding,
			const uInt_t father,
			const Str_t& uniquename,
			const Str_t& labeltx, 			
			const uInt_t width, 
			const uInt_t height,
			const bool visible,
			const bool disable,
			const bool checked            
		) 

		:	widget(father, uniquename, labeltx, width, height, CHECKBOX, visible, disable, aianaencoding ),
			SessionWidgets(sessionwidgets),
			Checked(checked)
	{		
	}

	DESTRUCTOR PUBLIC DEFINITION checkbox::~checkbox
		(
			void
		)
	{
	}

	METHOD PUBLIC DEFINITION Str_t checkbox::toHtml
		(
			bool
		) const
	{
		if (Checked)
			return utl::Stringf(chtml::checkbox_checked, Uuid, Uuid, getconstUtf8Labeltx().c_str());
		else
			return utl::Stringf(chtml::checkbox, Uuid, Uuid, getconstUtf8Labeltx().c_str());
	}

	METHOD PUBLIC DEFINITION const Str_t checkbox::toJavaScript
		(
			bool
		) const
	{
		Buffer_t buf;
//		buf << utl::Stringf(cjs::intCheckBox, "buttonlist");
		buf << utl::Stringf(cjs::widget_width, Uuid, Width, Height);
		buf << utl::Stringf(cjs::event_click, Uuid, Uuid, objets[Type]);			

		return buf.str();
	}

	METHOD PUBLIC DEFINITION void checkbox::setChecked
		(
			const bool checked
		)
	{
		Checked = checked;
	}
}

