// SOURCE CHARSET ENCODING FOR WINDOWS   : Western European (Windows) - Codepage 1252
// SOURCE CHARSET ENCODING FOR LINUX, OS : ISO-8859-1

#include "ej1_agenda_tel.hpp"

/* 
 * EJEMPLO DE USO de la libreria libhefot BETA 0.1: 
 * *** Este ejemplo de uso tiene el objetivo de servir de apoyo para la construccion de la libreria
 *     , añadir caracteristicas basicas, y detectar bugs. Cuando la libreria este mas madura se compilara
 *     com liberia dinamica y se creara un ej de uso mas completo.
 * ***
 * Para el ejemplo de uso se va ha crear un programa que gestione una agenda telefonica:
 * - Menu :
 * - Altas
 * - Bajas
 * - Modificaciones
 * - Buscar por Nombre o Apellidos o telefono
 * Pantalla principal:
 * Cuatro botones
 * Altas
 * Bajas
 * Modificaciones
 * Buscar
 *
 * Pantalla: Altas, Bajas, Modificaciones, Visualizacion
 * Campos : Nombre, Apellidos, Tel. Fijo, Tel. Movil, email, web, Direccion.
 *
 * Pantalla Buscar:
 * Campos: Nombre, Apellidos, Tel. Fijo, Tel. Movil
 */


#ifdef _MSC_VER
const char* libPath = "D:\\DATOS\\PERSONALES\\DESARROLLOS\\"
"C++\\HEOWT\\src\\HEOWT\\src\\ui-include";
#elif __APPLE__
const char* libPath =  "/Users/dokoto/Developments/CPP/hwt/src"
"/HEOWT/src/ui-include";
#else
const char* libPath =  "/home/administrador/Desarrollos/hwt/src/HEOWT/src/ui-include";
#endif
std::vector<Contact_t> contatcs;

int ej1_agenda_tel(void)
{
	try
	{  		
		// Create a new Forms Aplication
		hwt::session App(libPath, hwt::CONSOLE, "ISO-8859-1");
		loadContactsFromFile(contatcs);

		// Set Title : <head><title>
		App.Header->setTitle("HEFOT BETA 0.1 -- Agenda Telef�nica");

		// Screens
		scrMenu(App);
		scrVisual(App);
		scrBuscar(App);      
		srcListado(App);
        
		App.DoDeploy();
		saveContactsInFile(contatcs);

	}
	catch (std::exception& e)
	{
		saveContactsInFile(contatcs);
		std::cout << "Se ha producido un excepcion incontrolada : " << e.what() << std::endl;
	}
	return 0;
}

void loadContactsFromFile(std::vector<Contact_t>& vContacts)
{	
	hwt::Str_t content;
#ifdef _MSC_VER    
	if (hwt::utl::ReadFullTxFile("contacts.txt", content))
#elif __APPLE__
    if (hwt::utl::ReadFullTxFile("/Users/dokoto/Developments/CPP/hwt/src/HEOWT/HEOWT/HEOWT/contacts.txt", content))
#else	  
	if (hwt::utl::ReadFullTxFile("/home/administrador/Desarrollos/hwt/src/HEOWT/HEOWT/HEOWT/contacts.txt", content))
#endif                                 
		convStr2vContacts(content, vContacts);
    else
        errorln("El fichero contacts.txt no se ha podido leer.");
}

void convContacts2Str(const std::vector<Contact_t>& vContacts, hwt::Str_t& out)
{
	if (vContacts.empty()) return;
	hwt::Buffer_t text;
	for(std::vector<Contact_t>::const_iterator it = vContacts.begin(); it != vContacts.end(); it++)
	{
		text << it->nombre << '\t';
		text << it->apellidos << '\t';
		text << it->telFijo << '\t';
		text << it->telMovil << '\t';
		text << it->email << '\t';
		text << it->web << '\t';
		text << it->dir << '\t';
		text << it->fechaAlta << '\n';
	}

	out.append(text.str());
}

void saveContactsInFile(const std::vector<Contact_t>& vContacts)
{
	hwt::Str_t text;
	convContacts2Str(vContacts, text);
	hwt::utl::SaveFullTxFile("contacts.txt", text.c_str());
}

void convStr2vContacts(const hwt::Str_t& text, std::vector<Contact_t>& vContacts)
{
	if (text.empty()) return;
	std::vector<hwt::Str_t> vLines;
    bool LFCR = (text.find("\r\n") != hwt::Str_t::npos)? true : false;

	hwt::utl::Split('\n', text.c_str(), vLines);
	std::vector<hwt::Str_t> vFields;
	Contact_t cc;
	size_t i = 0;
	for(std::vector<hwt::Str_t>::const_iterator it = vLines.begin(); it != vLines.end(); it++)
	{
        if (LFCR)
            hwt::utl::Split('\t', hwt::utl::ReplaceAll(*it, "\r", "").c_str(), vFields);
        else
            hwt::utl::Split('\t', it->c_str(), vFields);
				
		cc.nombre = vFields[i = 0];
		cc.apellidos = vFields[++i];
		cc.telFijo = vFields[++i];
		cc.telMovil = vFields[++i];
		cc.email = vFields[++i];
		cc.web = vFields[++i];
		cc.dir = vFields[++i];
		cc.fechaAlta = vFields[++i];

		vContacts.push_back(cc);
		vFields.clear();
	}
}

void scrMenu(hwt::session& App)
{
	App.AddDocument("MAIN", "Men� de Opciones", "Acciones Principales");	
	hwt::document* main = App.getDocByName("MAIN");

	// ALTAS
	size_t MENULine = main->AddLine();
	main->ModLine(MENULine)->AddWidget(hwt::BUTTON, "BT_ALTA", "Altas");			
	main->ModLine(MENULine)->ModButton(0)->AddEvent(&abrirVentana, hwt::ONCLICK, hwt::APPLICATION);	

	// BAJAS
	MENULine = main->AddLine();
	main->ModLine(MENULine)->AddWidget(hwt::BUTTON, "BT_BAJA", "Bajas");			
	main->ModLine(MENULine)->ModButton(0)->AddEvent(&abrirVentana, hwt::ONCLICK, hwt::APPLICATION);

	// MODIFICACIONES
	MENULine = main->AddLine();
	main->ModLine(MENULine)->AddWidget(hwt::BUTTON, "BT_MODIF", "Modificaciones");	
	main->ModLine(MENULine)->ModButton(0)->AddEvent(&abrirVentana, hwt::ONCLICK, hwt::APPLICATION);

	// BUSQUEDAS
	MENULine = main->AddLine();
	main->ModLine(MENULine)->AddWidget(hwt::BUTTON, "BT_BUSQ", "Busquedas");			
	main->ModLine(MENULine)->ModButton(0)->AddEvent(&abrirVentana, hwt::ONCLICK, hwt::APPLICATION);	

	// LISTADO
	MENULine = main->AddLine();
	main->ModLine(MENULine)->AddWidget(hwt::BUTTON, "BT_LIST", "Listado");			
	//main->ModLine(MENULine)->ModButton(0)->AddEvent(&abrirVentana, hwt::ONCLICK, hwt::APPLICATION);
    main->ModLine(MENULine)->ModWidget<hwt::button>("BT_LIST", hwt::BUTTON)->AddEvent(&abrirVentana, hwt::ONCLICK, hwt::APPLICATION);
}


void scrVisual(hwt::session& App)
{
	App.AddDocument("VISUAL", "", "");
	hwt::document* visual = App.getDocByName("VISUAL");

	visual->AddEvent(&cerraVentanaAUTO, hwt::CLOSE_WIN, hwt::APPLICATION);	
	visual->Visible = false;

	size_t VISULine = visual->AddLine();
	visual->ModLine(VISULine)->AddWidget(hwt::LABEL, "labNombre", "Nombre");
	visual->ModLine(VISULine)->AddWidget(hwt::INPUT, "inpNombre");

	VISULine = visual->AddLine();
	visual->ModLine(VISULine)->AddWidget(hwt::LABEL, "labApellidos", "Apellidos");
	visual->ModLine(VISULine)->AddWidget(hwt::INPUT, "inpApellidos");

	VISULine = visual->AddLine();
	visual->ModLine(VISULine)->AddWidget(hwt::LABEL, "labTelFijo", "Tel�fono Fijo");
	visual->ModLine(VISULine)->AddWidget(hwt::INPUT, "inpTelFijo");

	VISULine = visual->AddLine();
	visual->ModLine(VISULine)->AddWidget(hwt::LABEL, "labTelMovil", "Tel�fono M�vil");
	visual->ModLine(VISULine)->AddWidget(hwt::INPUT, "inpTelMovil");

	VISULine = visual->AddLine();
	visual->ModLine(VISULine)->AddWidget(hwt::LABEL, "labEmail", "Email");
	visual->ModLine(VISULine)->AddWidget(hwt::INPUT, "inpEmail");

	VISULine = visual->AddLine();
	visual->ModLine(VISULine)->AddWidget(hwt::LABEL, "labWeb", "P�gina Web");
	visual->ModLine(VISULine)->AddWidget(hwt::INPUT, "inpWeb");

	VISULine = visual->AddLine();
	visual->ModLine(VISULine)->AddWidget(hwt::LABEL, "labDireccion", "Direcci�n");
	visual->ModLine(VISULine)->AddWidget(hwt::INPUT, "inpDireccion");

	VISULine = visual->AddLine();
	visual->ModLine(VISULine)->AddWidget(hwt::LABEL, "labFechaAlta", "Fecha de Alta");
	visual->ModLine(VISULine)->AddWidget(hwt::INPUT_DATE, "inpDFechaAlta");

	VISULine = visual->AddLine();	
	visual->ModLine(VISULine)->AddWidget(hwt::FILEEXPLORER, "feFoto1", "Foto Personal");

	VISULine = visual->AddLine();	
	visual->ModLine(VISULine)->AddWidget(hwt::FILEEXPLORER, "feFoto2", "Foto Casa");

	VISULine = visual->AddLine();
	visual->ModLine(VISULine)->AddWidget(hwt::LABEL, "labColorPelo", "Color del Pelo");
	visual->ModLine(VISULine)->AddWidget(hwt::LISTBOX, "listBox1");
	hwt::listbox* lb = static_cast<hwt::listbox*>(App.getWidgetByName("listBox1"));
	lb->addOption("Casta�o", "brown", false);
	lb->addOption("Rubio", "blonde", false);
	lb->addOption("Moreno", "black", false);
	lb->addOption("Pelirojo", "red", false);


	VISULine = visual->AddLine();
	visual->ModLine(VISULine)->AddWidget(hwt::CHECKBOXGROUP, "CBG_GENERO", "Generos");
    hwt::checkboxGroup* cbg = visual->ModLine(VISULine)->ModWidget<hwt::checkboxGroup>("CBG_GENERO", hwt::CHECKBOXGROUP);
    cbg->Add("CB_MASC", "Masculino", true);
    cbg->Add("CB_FEME", "Femenino");
    
    VISULine = visual->AddLine();
    visual->ModLine(VISULine)->AddWidget(hwt::LABEL, "labComentarios", "Comentarios");
    visual->ModLine(VISULine)->AddWidget(hwt::TEXTAREA, "TXA_DESCRIP", "Comentarios", 200, 200);
    
	VISULine = visual->AddLine();

	// BOTON
	VISULine = visual->AddLine();
	visual->ModLine(VISULine)->AddWidget(hwt::BUTTON, "BT_EJECUTAR", "Ejecutar");
	visual->ModLine(VISULine)->ModWidget<hwt::button>("BT_EJECUTAR", hwt::BUTTON)->AddEvent(&gestionAccion, hwt::ONCLICK);
	// DEPRECATED: visual->ModLine(VISULine)->ModButton("BT_EJECUTAR")->AddEvent(&gestionAccion, hwt::ONCLICK);
}

void srcListado(hwt::session& App)
{
	App.AddDocument("LISTADO", "Listar", "Listado Telef�nico");
	hwt::document* listado = App.getDocByName("LISTADO");

	listado->AddEvent(&cerraVentanaAUTO, hwt::CLOSE_WIN, hwt::APPLICATION);
	listado->Visible = false;

	size_t ListLine = listado->AddLine();
	listado->ModLine(ListLine)->AddWidget(hwt::GRID, "glist", "glist");
	hwt::grid* gd = static_cast<hwt::grid*>(App.getWidgetByName("glist"));

	gd->addFieldHeaderDesc("nombre", "Str_t", 0, "Nombre");
	gd->addFieldHeaderDesc("apellidos", "Str_t", 0, "Apellidos");
	gd->addFieldHeaderDesc("telFijo", "Str_t", 0, "Tel�fono Fijo");
	gd->addFieldHeaderDesc("telMovil", "Str_t", 0, "Tel�fono M�vil");
	gd->addFieldHeaderDesc("email", "Str_t", 0, "eMail");
	gd->addFieldHeaderDesc("web", "Str_t", 0, "Url Web");
	gd->addFieldHeaderDesc("dir", "Str_t", 0, "Direcci�n");
	gd->addFieldHeaderDesc("fechaAlta", "Str_t", 0, "Fecha Alta");
}

void scrBuscar(hwt::session& App)
{
	App.AddDocument("BUSCAR", "Buscar", "Buscar contacto por Nombre");
	hwt::document* buscar = App.getDocByName("BUSCAR");
	buscar->Visible = false;
	//App.getDocByName("BUSCAR")->AddEvent(&cerraVentanaAUTO, hwt::CLOSE_WIN, hwt::APPLICATION);

	size_t BUSCLine = buscar->AddLine();
	buscar->ModLine(BUSCLine)->AddWidget(hwt::LABEL, "Nombre", "Nombre");
	buscar->ModLine(BUSCLine)->AddWidget(hwt::INPUT, "inpBusNombre", "Buscar..");

	BUSCLine = buscar->AddLine();

	// BOTON
	BUSCLine = buscar->AddLine();
	buscar->ModLine(BUSCLine)->AddWidget(hwt::BUTTON, "btBuscar", "Buscar");			
	buscar->ModLine(BUSCLine)->ModButton(0)->AddEvent(&buscarContacto, hwt::ONCLICK, hwt::APPLICATION);

	buscar->ModLine(BUSCLine)->AddWidget(hwt::BUTTON, "btCancelar", "Cancelar");			
	buscar->ModLine(BUSCLine)->ModButton(1)->AddEvent(&cerraVentana, hwt::ONCLICK, hwt::APPLICATION);
}

void cerraVentana(const hwt::params_t& params, hwt::session& ss)
{
	hwt::params_t::const_iterator itParams = params.find("caller");
	const hwt::widget* acction = ss.getWidgetById(hwt::utl::Str2Uint(itParams->second.value));
	ss.getDocById(acction->Father)->Visible = false;
    DisableMain(false, ss);
}

void cerraVentanaAUTO(const hwt::params_t& params, hwt::session& Session)
{
	hwt::params_t::const_iterator itParams = params.find("caller");
	const hwt::document* doc = Session.getDocById(hwt::utl::Str2Uint(itParams->second.value));
	if (doc->UniqueName.compare("VISUAL") == 0 )
		Session.getDocByName("VISUAL")->Visible = false;
	if (doc->UniqueName.compare("LISTADO") == 0 )
	{		
		hwt::params_t::const_iterator itParams = params.find(hwt::utl::Uint2Str(Session.getconstWidgetByName("glist")->Uuid));
		if (itParams != params.end())
			Session.getDocByName("LISTADO")->AddAlert(hwt::utl::Stringf("Linea %s seleccionada.", itParams->second.value.c_str()));
		Session.getDocByName("LISTADO")->Visible = false;
	}
	DisableMain(false, Session);
}


void DisableMain(const bool disable, hwt::session& ss)
{
#define CONV(val) static_cast<hwt::button*>(const_cast<hwt::widget*>(val))
	hwt::button* in = CONV(ss.getWidgetByName("inpNombre"));

	in = NULL;
	in = CONV(ss.getWidgetByName("BT_ALTA"));
	in->Disable = disable;

	in = NULL;
	in = CONV(ss.getWidgetByName("BT_BAJA"));
	in->Disable = disable;

	in = NULL;
	in = CONV(ss.getWidgetByName("BT_MODIF"));
	in->Disable = disable;

	in = NULL;
	in = CONV(ss.getWidgetByName("BT_BUSQ"));
	in->Disable = disable;

	in = NULL;
	in = CONV(ss.getWidgetByName("BT_LIST"));
	in->Disable = disable;

#undef CONV
}


void DisableIputs(const bool disable, hwt::session& ss)
{
#define CONV(val) static_cast<hwt::input*>(const_cast<hwt::widget*>(val))
	hwt::input* in = CONV(ss.getWidgetByName("inpNombre"));
	in->Disable = disable;

	in = NULL;
	in = CONV(ss.getWidgetByName("inpApellidos"));
	in->Disable = disable;

	in = NULL;
	in = CONV(ss.getWidgetByName("inpTelFijo"));
	in->Disable = disable;

	in = NULL;
	in = CONV(ss.getWidgetByName("inpTelMovil"));
	in->Disable = disable;

	in = NULL;
	in = CONV(ss.getWidgetByName("inpEmail"));
	in->Disable = disable;

	in = NULL;
	in = CONV(ss.getWidgetByName("inpWeb"));
	in->Disable = disable;

	in = NULL;
	in = CONV(ss.getWidgetByName("inpDireccion"));
	in->Disable = disable;

	in = NULL;
	in = CONV(ss.getWidgetByName("inpDFechaAlta"));
	in->Disable = disable;
#undef CONV
}

void ClearInputValues(hwt::session& ss)
{
	Contact_t cc;
	setContacts(cc, ss);
}

void abrirVentana(const hwt::params_t& params, hwt::session& Session)
{
#define CONV(val) static_cast<hwt::button*>(const_cast<hwt::widget*>(val))
	hwt::params_t::const_iterator itParams = params.find("caller");
	const hwt::widget* click = Session.getWidgetById(hwt::utl::Str2Uint(itParams->second.value));	
	if (click->UniqueName.compare("BT_BUSQ") == 0)
	{
		DisableMain(true, Session);
		Session.getDocByName("VISUAL")->Description = "Buscar telefono por nombre de contacto";
		Session.getDocByName("VISUAL")->setLabeltx("Busquedas");
		Session.getDocByName("VISUAL")->Visible = false;
		hwt::button* ejecutar = CONV(Session.getWidgetByName("BT_EJECUTAR"));
		ejecutar->Visible = false;
		Session.getDocByName("BUSCAR")->Visible = true;		
		Session.setDocOnTopAndUnTopRest("BUSCAR");
		DisableIputs(true, Session);
	}
	else if (click->UniqueName.compare("BT_ALTA") == 0)
	{
		Session.getDocByName("VISUAL")->Description = "Dar de alta un nuevo contacto";
		Session.getDocByName("VISUAL")->setLabeltx("Altas");
		Session.getDocByName("VISUAL")->Visible = true;
		hwt::button* ejecutar = CONV(Session.getWidgetByName("BT_EJECUTAR"));
		ejecutar->Visible = true;
		ejecutar->setLabeltx("Crear");
		ClearInputValues(Session);
		Session.setDocOnTopAndUnTopRest("VISUAL");	
		DisableIputs(false, Session);
	}
	else if (click->UniqueName.compare("BT_BAJA") == 0)
	{
		Session.getDocByName("VISUAL")->Description = "Dar de baja un contacto";
		Session.getDocByName("VISUAL")->setLabeltx("Bajas");
		Session.getDocByName("VISUAL")->Visible = false;
		hwt::button* ejecutar = CONV(Session.getWidgetByName("BT_EJECUTAR"));
		ejecutar->Visible = true;
		ejecutar->setLabeltx("Eliminar");
		Session.getDocByName("BUSCAR")->Visible = true;		
		Session.setDocOnTopAndUnTopRest("BUSCAR");	
		DisableIputs(true, Session);
	}
	else if (click->UniqueName.compare("BT_MODIF") == 0)
	{
		Session.getDocByName("VISUAL")->Description = "Modificar un contacto";
		Session.getDocByName("VISUAL")->setLabeltx("Modificaciones");
		Session.getDocByName("VISUAL")->Visible = false;
		hwt::button* ejecutar = CONV(Session.getWidgetByName("BT_EJECUTAR"));
		ejecutar->Visible = true;
		ejecutar->setLabeltx("Modificar");
		Session.getDocByName("BUSCAR")->Visible = true;		
		Session.setDocOnTopAndUnTopRest("BUSCAR");	
		DisableIputs(false, Session);
	}		
	else if (click->UniqueName.compare("BT_LIST") == 0)
	{
		Session.setDocOnTopAndUnTopRest("LISTADO");
		hwt::document* list = Session.getDocByName("LISTADO");
		list->Visible = true;		
		DisableMain(true, Session);
		hwt::grid* gd = static_cast<hwt::grid*>(Session.getWidgetByName("glist"));
		hwt::Str_t plainText;
		convContacts2Str(contatcs, plainText);
		gd->loadFromString(plainText.c_str());	
		/* VERSION TO SMALL FILES
		gd->clearContent();
		for(std::vector<Contact_t>::const_iterator it = contatcs.begin();
			it != contatcs.end();
			it++)
		{
			gd->addFieldValueToContent("nombre", it->nombre.c_str());
			gd->addFieldValueToContent("apellidos", it->apellidos.c_str());
			gd->addFieldValueToContent("telFijo", it->telFijo.c_str());
			gd->addFieldValueToContent("telMovil", it->telMovil.c_str());
			gd->addFieldValueToContent("email", it->email.c_str());
			gd->addFieldValueToContent("web", it->web.c_str());
			gd->addFieldValueToContent("dir", it->dir.c_str());
			gd->addFieldValueToContent("fechaAlta", it->fechaAlta.c_str());
			gd->addLineContent();
		}	
		*/
	}
#undef CONV
}

void buscarContacto(const hwt::params_t& params, hwt::session& ss)
{
	hwt::params_t::const_iterator it = params.find(hwt::utl::Uint2Str(ss.getWidgetByName("inpBusNombre")->Uuid));
	std::vector<Contact_t>::const_iterator cc = searchContact(it->second.value, contatcs);
	if (cc == contatcs.end())
	{
		ss.getDocByName("BUSCAR")->AddAlert("No se ha encontrado el nombre buscado.");
	}
	else
	{
		setContacts(*cc, ss);
		ss.getDocByName("BUSCAR")->Visible = false;
		ss.getDocByName("VISUAL")->Visible = true;	
	}
}

void gestionAccion(const hwt::params_t& params, hwt::session& ss)
{
	hwt::params_t::const_iterator itParams = params.find("caller");
	const hwt::widget* acction = ss.getWidgetById(hwt::utl::Str2Uint(itParams->second.value));

	Contact_t cc = loadContacts(params, ss);
	if (acction->getconstLabeltx().compare("Crear") == 0)
	{
		contatcs.push_back(cc);
		ss.getDocByName("VISUAL")->Visible = false;
		ss.getDocByName("VISUAL")->AddAlert("Nuevo contacto añadido.");
	}
	else if (acction->getconstLabeltx().compare("Eliminar") == 0)
	{
		std::vector<Contact_t>::iterator it = searchContact(cc.nombre, contatcs);
		if (it != contatcs.end())
			contatcs.erase(it);
		ss.getDocByName("VISUAL")->Visible = false;
		ss.getDocByName("VISUAL")->AddAlert("Contacto eliminado.");
	}
	else if (acction->getconstLabeltx().compare("Modificar") == 0)
	{
		std::vector<Contact_t>::iterator it = searchContact(cc.nombre, contatcs);
		if (it != contatcs.end())
		{
			contatcs.erase(it);
			contatcs.push_back(cc);
		}		
		ss.getDocByName("VISUAL")->Visible = false;
		ss.getDocByName("VISUAL")->AddAlert("Contacto Modificado.");
	}
}

void setContacts(const Contact_t& cc, hwt::session& ss)
{		
#define CONV(val) static_cast<hwt::input*>(const_cast<hwt::widget*>(val))
	hwt::input* in = CONV(ss.getWidgetByName("inpNombre"));
	in->setValue(cc.nombre);	

	in = NULL;
	in = CONV(ss.getWidgetByName("inpApellidos"));
	in->setValue(cc.apellidos);

	in = NULL;
	in = CONV(ss.getWidgetByName("inpTelFijo"));
	in->setValue(cc.telFijo);

	in = NULL;
	in = CONV(ss.getWidgetByName("inpTelMovil"));
	in->setValue(cc.telMovil);

	in = NULL;
	in = CONV(ss.getWidgetByName("inpEmail"));
	in->setValue(cc.email);

	in = NULL;
	in = CONV(ss.getWidgetByName("inpWeb"));
	in->setValue(cc.web);

	in = NULL;
	in = CONV(ss.getWidgetByName("inpDireccion"));
	in->setValue(cc.dir);

	in = NULL;
	in = CONV(ss.getWidgetByName("inpDFechaAlta"));
	in->setValue(cc.fechaAlta);
#undef CONV
}

Contact_t loadContacts(const hwt::params_t& params, const hwt::session& ss)
{
	Contact_t cc;
	for(hwt::params_t::const_iterator it = params.begin(); it != params.end(); it++)
	{
		if (ss.getconstWidgetByName("inpNombre")->Uuid == hwt::utl::Str2Uint(it->first))
			cc.nombre = hwt::utl::Any2Str<hwt::Str_t>(it->second.value);

		else if (ss.getconstWidgetByName("inpApellidos")->Uuid == hwt::utl::Str2Uint(it->first))
			cc.apellidos = hwt::utl::Any2Str<hwt::Str_t>(it->second.value);

		else if (ss.getconstWidgetByName("inpTelFijo")->Uuid == hwt::utl::Str2Uint(it->first))
			cc.telFijo = hwt::utl::Any2Str<hwt::Str_t>(it->second.value);

		else if (ss.getconstWidgetByName("inpTelMovil")->Uuid == hwt::utl::Str2Uint(it->first))
			cc.telMovil = hwt::utl::Any2Str<hwt::Str_t>(it->second.value);

		else if (ss.getconstWidgetByName("inpEmail")->Uuid == hwt::utl::Str2Uint(it->first))
			cc.email = hwt::utl::Any2Str<hwt::Str_t>(it->second.value);

		else if (ss.getconstWidgetByName("inpWeb")->Uuid == hwt::utl::Str2Uint(it->first))
			cc.web = hwt::utl::Any2Str<hwt::Str_t>(it->second.value);

		else if (ss.getconstWidgetByName("inpDireccion")->Uuid == hwt::utl::Str2Uint(it->first))
			cc.dir = hwt::utl::Any2Str<hwt::Str_t>(it->second.value);

		else if (ss.getconstWidgetByName("inpDFechaAlta")->Uuid == hwt::utl::Str2Uint(it->first))
			cc.fechaAlta = hwt::utl::Any2Str<hwt::Str_t>(it->second.value);

	}

	return cc;
}

std::vector<Contact_t>::iterator searchContact(const std::string& name, std::vector<Contact_t>& contacts)
{
	for(std::vector<Contact_t>::iterator it = contacts.begin(); it != contacts.end(); it++)
	{
		if (it->nombre.compare(name) == 0)
			return it;
	}

	return contacts.end();
}

