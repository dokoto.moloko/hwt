// SOURCE CHARSET ENCODING FOR WINDOWS   : US-ASCII
// SOURCE CHARSET ENCODING FOR LINUX, OS : ASCII
#include "widget.hpp" 

namespace hwt
{

	CONSTRUCTOR PUBLIC DEFINITION widget::widget
		(
			const uInt_t father,
			const Str_t& uniquename,
			const Str_t& labeltx, 
			const uInt_t width,
			const uInt_t height, 
			objets_t type,
			const bool visible,
			const bool disable,
            const Char_t* aianaencoding
		) :	
			Uuid(reinterpret_cast<uInt_t>(this)),
			Father(father),
			UniqueName(uniquename),
			Width(width),
			Height(height),
			Type(type),
			Visible(visible),
			Disable(disable),
            AianaEncoding(aianaencoding),
            Labeltx(labeltx)
		{
			if (uniquename.empty() == true)
				UniqueName = utl::Any2Str<long>(static_cast<long>(Uuid));
		}


	DESTRUCTOR PUBLIC DEFINITION widget::~widget
		(
			void
		)
	{
	}

	METHOD PUBLIC DEFINITION void widget::setLabeltx
		(
			const Str_t& value
		)
	{
		Labeltx = value;
	}

	METHOD PUBLIC DEFINITION const Str_t widget::getconstLabeltx
		(
			void
		) const
	{
		return Labeltx;
	}

	METHOD PUBLIC DEFINITION const Str_t widget::getconstUtf8Labeltx
		(
			void
		) const
	{
		Str_t utf8;
		if (!utl::anyToUTF(AianaEncoding.c_str(), Labeltx.c_str(), utf8))
			errorln("Charset to UTF-8 conversion has failed.");
		return utf8;
	}

	FUNCTION PRIVATE DEFINITION static scope_t mkScope
		( 
			const scope_val_t scope,
			const uInt_t uuid
		)
	{ 
		scope_t Scope = {scope, uuid};
		return Scope; 
	}


    METHOD PUBLIC DEFINITION void widget::AddEvent
        (
            const ptrParams_t ptrF,
            events_t event,
			scope_val_t scope
        )
    {
        EventPool.insert(std::make_pair(events[event], ptrF));
		EventPoolScope.insert(std::make_pair(events[event], mkScope(scope, 0)));
    }

	METHOD PUBLIC DEFINITION void widget::AddEvent
		(
			const ptrParams_t ptrF,
			events_t event,
			scope_t scope
		)
	{
		EventPool.insert(std::make_pair(events[event], ptrF));
		EventPoolScope.insert(std::make_pair(events[event], scope));
	}
    
    METHOD PUBLIC DEFINITION void widget::executeEvent
        (
            scope_val_t event,
            const params_t& params,
            session& ss
        )
    {
        std::map<Str_t, ptrParams_t>::const_iterator it = EventPool.find(events[event]);
        if (it == EventPool.end())
        {
            errorln
            (
                utl::Stringf("Event: '%s' not found.", events[event])
            );
        }
        const ptrParams_t ptrF = it->second;
        (*ptrF)(params, ss);
    }
    
    METHOD PUBLIC DEFINITION void widget::executeEvent
        (
            scope_val_t event,
            const params_t& params,
            session& ss
        ) const
    {
        std::map<Str_t, ptrParams_t>::const_iterator it = EventPool.find(events[event]);
        if (it == EventPool.end())
        {
			errorln
            (
                utl::Stringf("Event: '%s' not found.", events[event])
            );
        }
        (*(EventPool.find(events[event])->second)) (params, ss);
    }


	METHOD PUBLIC DEFINITION const std::map<Str_t, ptrParams_t>& widget::getconstEvents
		(
			void
		) const
	{
		return EventPool;
	}

	METHOD PUBLIC DEFINITION scope_val_t widget::getconstEventScope
		(
			events_t event
		) const
	{
		std::map<Str_t, scope_t>::const_iterator it = EventPoolScope.find(events[event]);
		if (it == EventPoolScope.end())
			return NO_SCOPE;
		return it->second.scope;
	}

	METHOD PUBLIC DEFINITION const scope_t& widget::getconstEventScopeFull
		(
			events_t event
		) const
	{
		std::map<Str_t, scope_t>::const_iterator it = EventPoolScope.find(events[event]);
		if (it == EventPoolScope.end())
			errorln
			(
				utl::Stringf("Event: '%s' not found.", events[event])
			);
		return it->second;
	}

}

