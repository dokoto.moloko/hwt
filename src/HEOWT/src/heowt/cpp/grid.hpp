// SOURCE CHARSET ENCODING FOR WINDOWS   : US-ASCII
// SOURCE CHARSET ENCODING FOR LINUX, OS : ASCII

#ifndef _GRID_HPP
#define _GRID_HPP 

#include "types.hpp"
#include "widget.hpp"
#include "utils.hpp"
#include "webTags.hpp"

namespace hwt
{
	class grid : public widget
	{
		private:
			COPY_CONSTRUCTOR PRIVATE DEFINITION grid
				(
					const grid& g 
				)  
				:	widget( g.Father, g.UniqueName, g.Labeltx, g.Width, g.Height, GRID, g.Visible, g.Disable, g.AianaEncoding.c_str() ),
					SessionWidgets(g.SessionWidgets){}

			OPERATOR PRIVATE DEFINITION grid& operator=
				(
					const grid&
				) { return *this; }

		private:				
			sessionWidgets_t* SessionWidgets;
			std::map<Str_t, fieldCatalog_t> fieldCat;
			std::vector<fieldCatalog_t*> fieldCatOrder;
			std::vector<std::map<Str_t, Str_t> > Content;
			std::map<Str_t, Str_t> lineContent;
			Char_t* Stream;

		public:
			CONSTRUCTOR PUBLIC DECLARATION grid
				(	
					sessionWidgets_t* sessionwidgets,
                    const Char_t* aianaencoding,
					const uInt_t father,	
					const Str_t& uniquename = "",
					const Str_t& labeltx = "", 					
					const uInt_t width = 0,
					const uInt_t height = 0,	
					const bool visible = true,
					const bool disable = false
				);

			DESTRUCTOR PUBLIC DECLARATION ~grid
				(
					void
				);


			METHOD PUBLIC DECLARATION void clearContent
				(
					void
				);

			METHOD PUBLIC DECLARATION void addFieldHeaderDesc
				(
					const Char_t* name,
					const Char_t* type,
					const uInt_t  len,
					const Char_t* headerName
				);

			METHOD PUBLIC DECLARATION void addFieldValueToContent
				(
					const Char_t* name,
					const Char_t* value
				);

			METHOD PUBLIC DECLARATION void addLineContent
				( 
					void
				);

			/*
			 * Method for big contents
			 *
			 * @param str plain text fields separated by '\t' 
			 *            and lines separated by '\n'.
			 */
			METHOD PUBLIC DECLARATION void loadFromString
				( 
					const Char_t* str
				);


			/*
			 * To use only with 'loadFromString'
			 */
			METHOD PUBLIC DECLARATION void toJsonFromPlainText
				(
					Str_t& stream
				);

			/*
			 * To use only with 'addFieldValueToContent' and 'addLineContent'
			 */
			METHOD PUBLIC DECLARATION void toJson
				(
					Str_t& stream
				) const;

			/*
			 * To use only with 'loadFromString'
			 */
			METHOD PUBLIC DECLARATION void toUtf8JsonFromPlainText
				(
					Str_t& stream
				);

			/*
			 * To use only with 'addFieldValueToContent' and 'addLineContent'
			 */
			METHOD PUBLIC DECLARATION void toUtf8Json
				(
					Str_t& stream
				) const;

			METHOD PUBLIC DECLARATION Str_t toHtml
				(
					bool no_event = false
				) const;

			METHOD PUBLIC DECLARATION const Str_t toJavaScript
				(
					bool no_events = false
				) const;
	};
}

#endif // _GRID_HPP

