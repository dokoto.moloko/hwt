// SOURCE CHARSET ENCODING FOR WINDOWS   : US-ASCII
// SOURCE CHARSET ENCODING FOR LINUX, OS : ASCII

#ifndef _UTILS_HPP
#define _UTILS_HPP

#include "types.hpp" 
#include "unicode/utypes.h"
#include "unicode/ucnv.h"

namespace hwt
{	
namespace utl
{
        
	template <class type_tp>
    FUNCTION PUBLIC DEFINITION  Str_t Any2Str
        (
            type_tp value
        )
	{
		Buffer_t ss; ss << value; return ss.str();
	}
        
	template <class out_t, class in_t>
    FUNCTION PUBLIC DECLARATION out_t Any2Any
        (
            const in_t value
        )
	{
		Buffer_t ss;
		out_t aux;
		ss << value;
		ss >> aux;
		return aux;
	}

	FUNCTION PUBLIC DECLARATION uInt_t Str2Uint
		(
			Str_t value
		);

	FUNCTION PUBLIC DECLARATION Str_t Uint2Str
		(
			uInt_t value
		);

	FUNCTION PUBLIC DECLARATION value_t Any2Value
		(
			int value
		);

	FUNCTION PUBLIC DECLARATION value_t Any2Value
		(
			long value
		);

	FUNCTION PUBLIC DECLARATION value_t Any2Value
		(
			double value
		);

	FUNCTION PUBLIC DECLARATION value_t Any2Value
		(
			float value
		);

	FUNCTION PUBLIC DECLARATION value_t Any2Value
		(
			char value
		);

	FUNCTION PUBLIC DECLARATION value_t Any2Value
		(
			Str_t value
		);	

	FUNCTION PUBLIC DECLARATION value_t Any2Value
		(
			wchar_t* value
		);
    
	template<typename type_key, typename type_container>
	FUNCTION PUBLIC DEFINITION void delDepWidget
        (
            const type_key& key,
            type_container& container
         )
	{
		typename type_container::iterator it = container.find(key);
		if (it != container.end())
			container.erase(it);
	}

	template<typename type_struct>
	FUNCTION PUBLIC DEFINITION void delDepWidget_Redund1
		(
			const uInt_t& uuid,
			type_struct& structure
		)
	{
		delDepWidget(uuid, structure.w.buttons);
		delDepWidget(uuid, structure.w.inputs);
		delDepWidget(uuid, structure.w.inputs_date);
		delDepWidget(uuid, structure.w.labels);
	}

	FUNCTION PUBLIC DECLARATION const Str_t ReplaceAll
		(
			const Str_t& text,
			const Str_t& argToSearch,
			const Str_t& argWithReplace
		);	

	FUNCTION PUBLIC DECLARATION bool Split
		(
			const Char_t sep, 
			const Char_t* raw, 
			std::vector<Str_t>& split_values
		);

	FUNCTION PUBLIC DECLARATION bool Split
		(
			const Char_t* sep, 
			const Char_t* raw, 
			std::vector<Str_t>& split_values
		);


	FUNCTION PUBLIC DECLARATION void Json2Params
		(
			const Str_t& json,
			std::map<Str_t, Str_t>& params
		);

	FUNCTION PUBLIC DECLARATION Str_t CurrentDir
		(
			void
		);

	FUNCTION PUBLIC DECLARATION size_t NumOfDirLevels
		(
			void
		);

    FUNCTION PUBLIC DECLARATION size_t NumOfDirLevels
		(
			const Char_t* path
		);

	FUNCTION PUBLIC DECLARATION Char_t GetDirSep
		(
			void
		);

	FUNCTION PUBLIC DECLARATION void DoFormatting
		(
			Str_t& sF, 
			const Char_t* sformat, 
			va_list marker
		);

	FUNCTION PUBLIC DECLARATION void Stringf
		(
			Str_t& stgt, 
			const Char_t* sformat, 
			... 
		);

	FUNCTION PUBLIC DECLARATION Str_t Stringf
		(
			const Char_t* sformat, 
			... 
		);
    
	FUNCTION PUBLIC DECLARATION bool ReadFullTxFile
		(
			const Str_t& path, 
			Str_t& file
		);

	FUNCTION PUBLIC DECLARATION bool ReadFullBinFile
		(
			const Str_t& path, 
			Str_t& file
		);

	FUNCTION PUBLIC DECLARATION bool ReadFullTxFile
		(
			const Char_t* path, 
			Str_t& file
		);

	FUNCTION PUBLIC DECLARATION bool SaveFullTxFile
		(
			const Str_t& path, 
			const Str_t& content
		);

	FUNCTION PUBLIC DECLARATION bool SaveFullTxFile
		(
			const Char_t* path, 
			const Str_t& content
		);
    
	FUNCTION PUBLIC DECLARATION Str_t getSysEncoding
        (
            void
        );
    
	FUNCTION PUBLIC DECLARATION bool UTFToAny
		(                
			const char* in,
			const char* out_iana_encoding,
			Str_t& out
		);

	FUNCTION PUBLIC DECLARATION bool anyToUTF
        (
            const char* in_iana_encoding,
            const char* in,
            Str_t& out
        );

	FUNCTION PUBLIC DECLARATION bool sysToUTF
		(
			const char* in,
			Str_t& out
		);

	FUNCTION PUBLIC DECLARATION bool UTFtoSys
		(
			const char* in,
			Str_t& out
		);


	/* DIRLEVEL(CInt_t& Level, Str_t& Dir)
	* Si tenemos el Directorio C:\uno\dos\tres\cuatro y queremos obtener la 
    * ruta hasta C:\uno\dos le pasamos (No?=NIVELES - 2), es decir, dos niveles 
    * arriba.
	* Ej:
	* Str_t& Dir;
	* DirLevel(NumOfDirLevels() - 2, Dir);
	*/
	FUNCTION PUBLIC DECLARATION Str_t DirLevel
		(
			const uInt_t& Level, 
			const Str_t& path
		);		

	FUNCTION PUBLIC DECLARATION Str_t DirLevel
		(
			const uInt_t& Level
		);

	FUNCTION PUBLIC DECLARATION Str_t FixWinPath
		(
			const Str_t& path
		);	

	FUNCTION PUBLIC DECLARATION Str_t ExtractEXTENSION
		(
			const Str_t& path
		);

	FUNCTION PUBLIC DECLARATION bool ListForderForCtrl
		(
			const Char_t* path, 
			const Char_t tipo, 
			Vfile_t& files
		);

	FUNCTION PUBLIC DECLARATION bool ListFolder
        (
            const Char_t* path,
            Vfile_t& files
         );

	FUNCTION PUBLIC DECLARATION bool ReadFullBinFile
		(
			const Char_t* path, 
			Str_t& file
		);

	FUNCTION PUBLIC DECLARATION void performCmd
		(
			const Str_t& cmd
		);

	FUNCTION PUBLIC DECLARATION void listAllFilesOnFolder
		(
			const hwt::Char_t* path,
			hwt::Vfile_t& fileList
		);

	FUNCTION PUBLIC DECLARATION const Str_t getDate
		(
			void
		);

	FUNCTION PUBLIC DECLARATION const Str_t getTime
		(
			void
		);

	FUNCTION PUBLIC DECLARATION const Str_t getTimeStamp
		(
			void
		);

#ifdef _MSC_VER

	FUNCTION PUBLIC DECLARATION bool ListFolderWIN
		(
			const Char_t* path, 
			Vfile_t& files
		);

	FUNCTION PUBLIC DECLARATION uInt_t StarTimeCount
		(
			void
		);

    // Tiempo en ms
	FUNCTION PUBLIC DECLARATION uInt_t CalcElapsetTime
		(
			const uInt_t time_ms
		);

	FUNCTION PUBLIC DECLARATION float msToSec
		(
			const uInt_t time_ms
		);

#else

    FUNCTION PUBLIC DECLARATION bool ListFolderUNIX
		(
			const Char_t* path, 
			Vfile_t& files
		);

	FUNCTION PUBLIC DECLARATION const timeval StarTimeCount
		(
			void
		);

    // Tiempo en usec
	FUNCTION PUBLIC DECLARATION double CalcElapsetTime
		(
			const timeval t1
		);

	FUNCTION PUBLIC DECLARATION double usecToSec
		(
			const double time_usec
		);
    
    FUNCTION PUBLIC DEFINITION double msToSec
        (
            const double time_ms
        );

#endif

}
}

#endif //_UTILS_HPP

