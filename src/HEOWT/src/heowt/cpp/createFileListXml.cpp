// SOURCE CHARSET ENCODING FOR WINDOWS   : US-ASCII
// SOURCE CHARSET ENCODING FOR LINUX, OS : ASCII
#include "createFileListXml.hpp"

namespace hwt
{
	createFileListXml::createFileListXml(Vfile_t lVecStr) : 
		VecStr(lVecStr)
	{
	}

	createFileListXml::~createFileListXml(void)
	{
		VecStr.clear();
		XmlDoc.reset();
	}

	Str_t createFileListXml::ToString(void)
	{
		std::ostringstream stream;
		XmlDoc.save(stream);

		return stream.str();
	}

	bool createFileListXml::Conv(void)
	{
		if (VecStr.empty()) return false;
		pugi::xml_node decl = XmlDoc.prepend_child(pugi::node_declaration);
		decl.append_attribute("version") = "1.0";
		decl.append_attribute("encoding") = "UTF-8";
				
		pugi::xml_node lista = XmlDoc.append_child("lista");
		pugi::xml_node item, tipo, nombre, ruta;
		for(Vfile_t::const_iterator cit = VecStr.begin(); cit != VecStr.end(); cit++)
		{
			item = lista.append_child("item");
				
			tipo = item.append_child("tipo");
			tipo.append_child(pugi::node_pcdata).set_value(cit->tipo.c_str());

			nombre = item.append_child("nombre");
			nombre.append_child(pugi::node_pcdata).set_value(cit->name.c_str());

			ruta = item.append_child("ruta");
			ruta.append_child(pugi::node_pcdata).set_value(cit->ruta.c_str());
		}

		return true;
	}
}

