#include "main.hpp"


const size_t MAX_DOC = 100;


#ifdef _MSC_VER
const char* jQueryPath = "D:\\DATOS\\PERSONALES\\DESARROLLOS\\"
"C++\\HEOWT\\src\\HEOWT\\src\\ui-include";
#elif __APPLE__
const char* jQueryPath =  "/Users/dokoto/Developments/CPP/hwt/src"
"/HEOWT/src/ui-include";
#else
const char* jQueryPath =  "/home/administrador/Desarrollos/hwt/src/HEOWT/src/ui-include";
#endif


int eje2(int argc, char* argv[])
{
    size_t numDocs = 0;
    if (argc == 1)
        numDocs = MAX_DOC;
    else if (argc == 2)
        numDocs = hwt::utl::Str2Uint(hwt::Str_t(argv[1]));
    
    hwt::session App(jQueryPath, hwt::CONSOLE, "UTF-8");
    App.Header->setTitle("HEFOT BETA 0.1 -- TEST");
    
    App.AddDocument("MAIN", "MAIN", "Main");
    hwt::document* main = App.getDocByName("MAIN");
    
    size_t lineNum = 0;
    hwt::Buffer_t txt;
    for (size_t i = 0; i < numDocs; i++)
    {
        lineNum = main->AddLine();
        txt << "BUT" << i;
        main->ModLine(lineNum)->AddWidget(hwt::BUTTON, txt.str().c_str(), txt.str().c_str());
        main->ModLine(lineNum)->ModWidget<hwt::button>(txt.str().c_str(), hwt::BUTTON)->AddEvent(&OpenDoc, hwt::ONCLICK, hwt::APPLICATION);
        txt.str("");
    }
    
    
    for (size_t i = 0; i < numDocs; i++)
    {
        lineNum = 0;
        txt << "DOC" << i;
        App.AddDocument(txt.str().c_str(), txt.str().c_str(), txt.str().c_str());
        hwt::document* doc = App.getDocByName(txt.str().c_str());
        doc->Visible = false;
        lineNum = doc->AddLine();
        doc->AddEvent(&closeDoc, hwt::CLOSE_WIN, hwt::APPLICATION);
        txt.str("");
        txt << "LABEL" << i;
        doc->ModLine(lineNum)->AddWidget(hwt::LABEL, txt.str().c_str(), txt.str().c_str());
        txt.str("");
        txt << "INPUT" << i;
        doc->ModLine(lineNum)->AddWidget(hwt::INPUT, txt.str().c_str(), txt.str().c_str());
        txt.str("");
    }
    
    App.DoDeploy();
    
	return 0;
}

void OpenDoc(const hwt::params_t& params, hwt::session& Session)
{
	hwt::params_t::const_iterator itParams = params.find("caller");
	const hwt::widget* click = Session.getWidgetById(hwt::utl::Str2Uint(itParams->second.value));
    unsigned long num = hwt::utl::Str2Uint(click->UniqueName.substr(3, click->UniqueName.size()));
    hwt::Buffer_t txt;
    txt << "DOC" << num;
    hwt::document* doc = Session.getDocByName(txt.str().c_str());
    doc->Visible = true;;
    Session.setDocOnTopAndUnTopRest(txt.str().c_str());
}

void closeDoc(const hwt::params_t& params, hwt::session& ss)
{
	hwt::params_t::const_iterator itParams = params.find("caller");
    hwt::document* doc = ss.getDocById(hwt::utl::Str2Uint(itParams->second.value));
	doc->Visible = false;
}


