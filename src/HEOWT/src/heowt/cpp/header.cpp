// SOURCE CHARSET ENCODING FOR WINDOWS   : US-ASCII
// SOURCE CHARSET ENCODING FOR LINUX, OS : ASCII

#include "header.hpp" 

namespace hwt
{
	METHOD PUBLIC DEFINITION const Str_t header::toHtml
		(
			void
		) const
	{
		Buffer_t buf;
		buf << utl::Stringf(chtml::title, getconstUtf8Title().c_str());
		buf << chtml::meta_header_html;
		buf << chtml::dependencies;

		return buf.str();
	}

	METHOD PUBLIC DEFINITION const Str_t header::toHtmlWithHeader
		(
			void
		) const
	{
		Buffer_t buf;
		buf << chtml::head_open;
		buf << toHtml();
		buf << chtml::head_close;
		
		return buf.str();
	}

	METHOD PUBLIC DEFINITION const Str_t header::getconstUtf8Title
		(
			void
		) const
	{
		Str_t utf8;
		if (!utl::anyToUTF(AianaEncoding.c_str(), Title.c_str(), utf8))
			errorln("Charset to UTF-8 conversion has failed.");

		return utf8;
	}

	METHOD PUBLIC DEFINITION const Str_t header::getconstTitle
		(
			void
		) const
	{
		return Title;
	}

	METHOD PUBLIC DEFINITION void header::setTitle
		(
			const Char_t* value
		)
	{
		Title = value;
	}


}

