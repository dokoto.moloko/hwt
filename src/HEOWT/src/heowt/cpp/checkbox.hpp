// SOURCE CHARSET ENCODING FOR WINDOWS   : US-ASCII 
// SOURCE CHARSET ENCODING FOR LINUX, OS : ASCII

#ifndef _CHECKBOX_HPP
#define _CHECKBOX_HPP

#include "types.hpp"
#include "widget.hpp"
#include "utils.hpp"
#include "webTags.hpp"
#include "tools.hpp" 

namespace hwt
{
	class checkbox : public widget
	{
		private:
			COPY_CONSTRUCTOR PRIVATE DEFINITION checkbox
				(
					const checkbox& c 
				)  
				:	widget( c.Father, c.UniqueName, c.Labeltx, c.Width, c.Height, CHECKBOX, c.Visible, c.Disable, c.AianaEncoding.c_str() ),
					SessionWidgets(c.SessionWidgets),
					Checked(c.Checked){}

			OPERATOR PRIVATE DEFINITION checkbox& operator=
				(
					const checkbox&
				) { return *this; }

		private:				
			sessionWidgets_t* SessionWidgets;
			bool Checked;

		public:
			CONSTRUCTOR PUBLIC DECLARATION checkbox
				(	
					sessionWidgets_t* sessionwidgets,
                    const Char_t* aianaencoding,
					const uInt_t father,	
					const Str_t& uniquename = "",
					const Str_t& labeltx = "", 					
					const uInt_t width = 0,
					const uInt_t height = 0,	
					const bool visible = true,
					const bool disable = false,
					const bool checked = false
				);

			DESTRUCTOR PUBLIC DECLARATION ~checkbox
				(
					void
				);

			METHOD PUBLIC DECLARATION Str_t toHtml
				(
					bool no_event = false
				) const;

			METHOD PUBLIC DECLARATION const Str_t toJavaScript
				(
					bool no_events = false
				) const;

			METHOD PUBLIC DECLARATION void setChecked
				(
					const bool checked
				);        
        
	};

}


#endif // _CHECKBOX_HPP

