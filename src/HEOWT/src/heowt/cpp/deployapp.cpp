// SOURCE CHARSET ENCODING FOR WINDOWS   : US-ASCII
// SOURCE CHARSET ENCODING FOR LINUX, OS : ASCII

#include "deployapp.hpp"
#include <stack> 

namespace hwt
{    
	namespace deployapp
	{				
		/*----------------------------------------------------------------------
		 * TIPOS LOCALES A UNIDAD
		 *--------------------------------------------------------------------*/
		struct ConnInf_tag
		{	
			Int_t							connectiontype;
			Str_t							answerstring;				
			struct MHD_PostProcessor*		postprocessor;
			std::map<Str_t, Str_t>          post_key_value;                        		
		};
		
		typedef struct ConnInf_tag			Connection_info_struct_t;
		typedef std::map<Str_t, Str_t>		Protocolo_t;

		/*----------------------------------------------------------------------
		 * VARIABLES LOCALES A UNIDAD
		 *--------------------------------------------------------------------*/
		static const Char_t* extensionsP[] = 
		{
			".jar", ".html", ".htm", ".css", ".js", ".jpeg", ".jpg", ".png", 
			".gif", ".txt", ".xml", ".php", ".json", ".md", ".ico", ".psd"
		};

		static const Char_t* extensions[] = 
		{
			"jar", "html", "htm", "css", "js", "jpeg", "jpg", "png",
			"gif", "txt", "xml", "php", "json", "md", "ico", "psd"
		};
		
		static const Char_t* sendHtmlMethods[]=
		{
			"POST", "GET"
		};
		
		enum 
		{
			POST, GET
		};

		enum	
		{
			JAR, HTML, HTM, CSS, JS, JPEG, JPG, PNG, GIF, TXT, XML, PHP, 
			JSON, MD, ICO, PSD
		};
		
		static const uInt_t		POST_BUFFER_SIZE		= 512;
		static const uInt_t		MAX_NAME_SIZE			= 20;
		static const uInt_t		MAX_ANSWER_SIZE			= 512;
		static const Char_t*	URL						= "http://127.0.0.1";


		
		//enum	{JQUERY_JS, JQUERY_CSS, JQUERY_UI_JS, CUSTOM_JS, CUSTOM_CSS};
		

		static std::map<int, Str_t*>	request_files;
		// Se usara que cuando se genere la rutina JS dinimica de respuesta al evento
		// si se necesita se pueda saver que evento lo produjo y por quien. Por quien 
		// para situar el evento en una ventana.
		static std::stack<std::pair<uInt_t, events_t> >		callsStack;
		static struct MHD_Daemon*		daemon;
		static session*					Session;
        sessionWidgets_t*				mapOfWidgets;
		Protocolo_t						protocolo;
		static bool						turnOFF;
		std::map<Str_t, Str_t>			mimetypes;
		std::map<Str_t, bool>			binaryFiles;
		std::map<Str_t, Str_t>			webFiles;
		Str_t							urlWithPort;
		createFileListXml*				fileListXML;
			
		/*----------------------------------------------------------------------
		 * FUNCTIONS DECLARATIONS OF PRIVATE FUNCTIONS
		 *--------------------------------------------------------------------*/
		FUNCTION PRIVATE DECLARATION static int send_page 
			(
				struct MHD_Connection* connection, 
				const Str_t& page, 
				const Str_t& mimetype,
				const Str_t& url

			);

		FUNCTION PRIVATE DECLARATION static int answer_to_connection
			(	
				void *cls,
				struct MHD_Connection * connection,
				const char *url,
				const char *method,
				const char *version,
				const char *upload_data,
				size_t *upload_data_size,
				void **con_cls
			);

		FUNCTION PRIVATE DECLARATION static int on_client_connect
			(
				void* cls, 
				const sockaddr * addr, 
				socklen_t addrlen 
			);

		FUNCTION PRIVATE DECLARATION static void request_completed
			(
				void* , 
				struct MHD_Connection* , 
				void** con_cls, 
				enum MHD_RequestTerminationCode 
			);

		FUNCTION PRIVATE DECLARATION static int iterate_post 
			(	
				void *cls,
				enum MHD_ValueKind kind,
				const char *key,
				const char *filename,
				const char *content_type,
				const char *transfer_encoding,
				const char *data, 
				uint64_t off, 
				size_t size 
			);

		FUNCTION PRIVATE DECLARATION static void page_dispatcher
			(
				const Str_t& page_request, 
				Str_t& page_return, 
				Str_t& mimetype
			);

		FUNCTION PRIVATE DECLARATION static bool isBinaryFile
			(
				const Str_t file
			);

		FUNCTION PRIVATE DECLARATION static void initFileExtensions
			(
				void
			);

		FUNCTION PRIVATE DECLARATION static void initMimeTypes
			(
				void
			);

		FUNCTION PRIVATE DECLARATION static void loadJsLibs
			(
				void
			);

		FUNCTION PRIVATE DECLARATION static void loadEvents
			(
				void
			);

		FUNCTION PRIVATE DECLARATION static void openBrowser
			(
				void
			);
        
		FUNCTION PRIVATE DECLARATION static void closeApplication
			(
				const params_t& params,
				session& Session
			);	

		FUNCTION PRIVATE DECLARATION static void generateFileList
			(
				const params_t& params,
				session& Session
			);

        FUNCTION PRIVATE DECLARATION static void handleEvents
            (
                std::map<Str_t, Str_t> post_key_value
            );

        FUNCTION PRIVATE DECLARATION static const Str_t createDynamicJS
			(
				const std::pair<uInt_t, events_t> value
			);

		FUNCTION PRIVATE DECLARATION static const Str_t getSerialGrid
			(
				const Str_t& filename
			);

		FUNCTION PRIVATE DECLARATION static const Str_t getFileList
			(
				void
			);

		FUNCTION PRIVATE DECLARATION static void mkJsforApplication
			(
				Buffer_t& buf
			);

		/*----------------------------------------------------------------------
		 * PUBLIC FUNCTIONS DEFINITIONS 
		 *--------------------------------------------------------------------*/
		FUNCTION PUBLIC DEFINITION void Init
			(
				session* ss,
                sessionWidgets_t* mapofwidgets
			)			
		{
            #ifdef _MSC_VER
			uInt_t time = utl::StarTimeCount();
            #else
            timeval time = utl::StarTimeCount();
            #endif
			daemon = NULL;
			Session = ss;
            mapOfWidgets = mapofwidgets;
            Session->turnON = true;
            Session->getDocById(Session->getUuidOfMainDoc())->AddEvent(&closeApplication, hwt::CLOSE_APP);
			turnOFF = false;
			fileListXML = NULL;
			Buffer_t buff;
			buff << URL << ":" << Session->Port;
			urlWithPort = buff.str();
			#ifdef VERBOSE
			infoln("Wainting to configuration of service ends...");
			#endif
			initFileExtensions();
			initMimeTypes();
			loadJsLibs();	
			loadEvents();
			#ifdef VERBOSE	
            #ifdef _MSC_VER
			infoln(utl::Stringf("Finalized in %f seg", utl::msToSec(utl::CalcElapsetTime(time))));
            #else
            infoln(utl::Stringf("Finalized in %f seg", utl::usecToSec(utl::CalcElapsetTime(time))));
            #endif
			#endif
		}
		
        FUNCTION PUBLIC DEFINITION void Destroy
            (
                void
            )
        {
			if (fileListXML != NULL)
			{
				delete fileListXML;
				fileListXML = NULL;
			}
        }        
        
		FUNCTION PUBLIC DEFINITION void Run
			(
				void
			)
		{

			daemon = MHD_start_daemon
                        (
                            MHD_USE_THREAD_PER_CONNECTION,
                            static_cast<uint16_t>(Session->Port),
                            on_client_connect,
                            NULL,
                            answer_to_connection,
                            NULL,
                            MHD_OPTION_NOTIFY_COMPLETED,
                            request_completed,
                            NULL,
                            MHD_OPTION_END
                        );

			if (daemon == NULL)
            	errorln("Fail to up the service.");
             
			#ifdef VERBOSE
			else
			{
				infoln(
					utl::Stringf("HEOWT Service listening"
					" on : %l", static_cast<unsigned long>(Session->Port))
				);
			}
			#endif
            openBrowser();
            while (Session->isTurnON()) 
			{ 
				#ifdef _MSC_VER
					Sleep(1); 
				#else
					sleep(1); 
				#endif
			}

            #ifdef VERBOSE
                shutdownln("HEOWT Service Shutdown" );
            #endif
			         
		}

		/*----------------------------------------------------------------------
		 * PRIVATE FUNCTIONS DEFINITIONS 
		 *--------------------------------------------------------------------*/
		FUNCTION PRIVATE DEFINITION static void mkJsforApplication
			(
				Buffer_t& buf
			)
		{	
			for(std::map<uInt_t, document*>::const_iterator DocIt = Session->getconstDocuments().begin();
				DocIt != Session->getconstDocuments().end();
				DocIt++)
			{
				buf << utl::Stringf(cjs::div_empty, DocIt->second->Uuid);
				buf << utl::Stringf(cjs::div_append, DocIt->second->Uuid, utl::ReplaceAll(DocIt->second->toHtml(), "\n", "").c_str());	
				buf << utl::Stringf(cjs::div_title, DocIt->second->Uuid, DocIt->second->getconstUtf8Title().c_str());

				if (DocIt->second->Visible == false)
				{
					Session->setDocUnTop(DocIt->second->Uuid);
					buf << utl::Stringf(cjs::div_to_close, DocIt->second->Uuid);
				}

				if (DocIt->second->Visible == true) 	
				{
					buf << utl::Stringf(cjs::div_to_open, DocIt->second->Uuid);
				}
				
				if (DocIt->second->isOnTop() == true)
					buf << utl::Stringf(cjs::div_to_top, DocIt->second->Uuid);
				
				for(std::vector<Str_t>::const_iterator itAlerts = DocIt->second->getconstAlerts().begin(); 
					itAlerts != DocIt->second->getconstAlerts().end(); itAlerts++)
				{
					buf << utl::Stringf(cjs::alert, itAlerts->c_str());
				}
				DocIt->second->clearAlerts();

				for
					(
						std::vector<line*>::const_iterator itLine = DocIt->second->getAllLines().begin();
						itLine != DocIt->second->getAllLines().end();
						itLine++
					)
				{
					if ((*itLine)->getconstAllWidgets().byId.size() > 0)
					{

						for (
								byId_t::const_iterator itWidget = (*itLine)->getconstAllWidgets().byId.begin();                          
								itWidget != (*itLine)->getconstAllWidgets().byId.end();
								itWidget++
							)
						{
							tl::POST_updateWidgetValues(itWidget->second, &Session->getconstSessionWidgets(), buf, false);
						}
					}
				}		
			}			
		}

		FUNCTION PRIVATE DEFINITION static const Str_t getFileList
			(
				void
			)
		{
			if (fileListXML == NULL)
				errorln("'fileListXML is NULL'");
			fileListXML->Conv();
			Str_t ret = fileListXML->ToString();
			delete fileListXML;
			fileListXML = NULL;
			if (ret.empty())
				errorln("'toString has fail'");

			return ret;
		}

		FUNCTION PRIVATE DEFINITION static const Str_t getSerialGrid
			(
				const Str_t& filename
			)
		{
			Str_t idGrid = filename.substr(0, filename.find_first_of("_"));
			grid* gd = static_cast<grid*>(Session->getWidgetById(hwt::utl::Str2Uint(idGrid)));
			Str_t stream;
			//gd->toUtf8Json(stream);
			gd->toUtf8JsonFromPlainText(stream);
			return stream;
		}

		FUNCTION PRIVATE DEFINITION static const Str_t createDynamicJS
			(
				const std::pair<uInt_t, events_t> value
			)
		{
			const widget* w = Session->getWidgetById(value.first);			
			Buffer_t buf;
			buf << cjs::dynmic_func_open;	
			mkJsforApplication(buf);
			buf << cjs::dynmic_func_close;
			return buf.str();
		}

        FUNCTION PRIVATE DEFINITION static void handleEvents
            (
                std::map<Str_t, Str_t> post_key_value
            )
        {

		// ATENCION !!!!!!!
		// Crear una funcion para cargar el XML de fichero cuando se detecte 
		// el evento GETLISTOFFILES despues pasara al page_dispacher donde
		// enviaremos el XML como respuesta.
		
            params_t param;
            size_t index = 0;
            uInt_t uuid = 0;
            Str_t event;
            
            // UUID
            std::map<Str_t, Str_t>::iterator it = post_key_value.find("caller");
            if (it == post_key_value.end())
            {
                errorln("The first Key-Pair value must be 'caller'");
            }
            else
            {
                Buffer_t buf;
                buf << it->second;
                buf >> uuid;
            }
            
            // EVENT
            it = post_key_value.find("event");
            if (it == post_key_value.end())
            {
                errorln("The second Key-Pair value must be 'event'");
            }
            else
            {
                event = it->second;
				if (event.compare(events[ONCLICK]) == 0 ||
					event.compare(events[GET_DIV]) == 0 ||
					event.compare(events[CLOSE_WIN]) == 0 )
					callsStack.push(std::make_pair(uuid, static_cast<events_t>(GETINDEX(event.c_str(), events)) ) );					
            }
            
            // END OF TRANSMISION
            it = post_key_value.find("endTransmision");
            if (it == post_key_value.end())
            {
                errorln("'endTransmision' not found.");
            }
            else
            {
                post_key_value.erase(it);
            }
            
            // Se cargan los parametros
            for
                (
                    std::map<Str_t, Str_t>::iterator it = post_key_value.begin();
                    it != post_key_value.end();
                    it++, index++
                 )
            {

                param[it->first] = utl::Any2Value(it->second);
            }
            
            // Se lanza el evento
           byId_t::const_iterator itw = mapOfWidgets->byId.find(uuid);
            if (itw == mapOfWidgets->byId.end())
            {                
                errorln
				(
                    utl::Stringf("uuid: '%d' not found.", uuid).c_str()
                );
            }
            widget* w = itw->second;
			if(event.compare(events[GETLISTOFFILES]) == 0)
				w->AddEvent(&generateFileList, hwt::GETLISTOFFILES);

			w->executeEvent( static_cast<scope_val_t>( GETINDEX(event.c_str(), events) ), param, *Session );
        }       


		FUNCTION PRIVATE DEFINITION static void generateFileList
			(
				const params_t& params,
				session&
			)
		{
			if (fileListXML != NULL) 
			{
				delete fileListXML;
				fileListXML = NULL;
			}
			params_t::const_iterator itTipo, itRuta;
			if ( (itTipo = params.find("tipo")) == params.end())
				errorln("'tipo' paramater no found");
			if ( (itRuta = params.find("ruta")) == params.end())
				errorln("'ruta' paramater no found");
			Vfile_t files;
			utl::ListForderForCtrl((*itRuta).second.value.c_str(), (*itTipo).second.value[0], files);
			if (files.empty())
				errorln(utl::Stringf("%s no found.", (*itRuta).second.value.c_str()));
			fileListXML = new createFileListXml(files);
		}

        FUNCTION PRIVATE DEFINITION static void closeApplication
            (
                const params_t& params,
                session& iSession
            )
        {
            params_t::const_iterator cit = params.find("turnON");
            if (cit == params.end())
                errorln("Param 'turnON' no found");

            if (cit->second.value.compare("false") == 0)
                iSession.turnON = false;
        }
        
		FUNCTION PRIVATE DEFINITION static void openBrowser
			(
				void
			)
		{			
			#ifdef _MSC_VER
				#ifdef VERBOSE
				infoln(utl::Stringf("Try to open %s", urlWithPort.c_str()));
                #endif
				utl::performCmd
                (
                    Str_t("\"C:\\Program Files (x86)"
                          "\\Mozilla Firefox\\firefox.exe\" " + urlWithPort)
                 );

			#elif __APPLE__
                #ifdef VERBOSE
                infoln(utl::Stringf("Try to open %s", urlWithPort.c_str()));
                #endif
				utl::performCmd(Str_t("open -a firefox " + urlWithPort));
			#else
                #ifdef VERBOSE
				infoln(utl::Stringf("Try to open %s", urlWithPort.c_str()));
                #endif
				utl::performCmd(Str_t("firefox " + urlWithPort));
			#endif    

		}

		FUNCTION PRIVATE DEFINITION static void initFileExtensions
			(
				void
			)
		{
			// FICHEROS DE TEXTO
			binaryFiles[extensionsP[HTML]]		= false;
			binaryFiles[extensionsP[HTM]]		= false;
			binaryFiles[extensionsP[CSS]]		= false;
			binaryFiles[extensionsP[JS]]		= false;
			binaryFiles[extensionsP[TXT]]		= false;
			binaryFiles[extensionsP[XML]]		= false;
			binaryFiles[extensionsP[PHP]]		= false;
			binaryFiles[extensionsP[JSON]]		= false;
			binaryFiles[extensionsP[MD]]		= false;


			// FICHEROS BINARIOS
			binaryFiles[extensionsP[JAR]]		= true;
			binaryFiles[extensionsP[JPEG]]		= true;
			binaryFiles[extensionsP[JPG]]		= true;
			binaryFiles[extensionsP[PNG]]		= true;
			binaryFiles[extensionsP[GIF]]		= true;
			binaryFiles[extensionsP[ICO]]		= true;
			binaryFiles[extensionsP[PSD]]		= true;
			
		}

		FUNCTION PRIVATE DEFINITION static void initMimeTypes
			(
				void
			)
		{
			// FICHEROS DE TEXTO
			mimetypes[extensions[HTML]]		= "text/html";
			mimetypes[extensions[HTM]]		= "text/html";
			mimetypes[extensions[CSS]]		= "text/css";
			mimetypes[extensions[JS]]		= "text/javascript";
			mimetypes[extensions[TXT]]		= "text/plain";
			mimetypes[extensions[XML]]		= "application/xml";
			mimetypes[extensions[PHP]]		= "text/plain";
			mimetypes[extensions[JSON]]		= "text/json";
			mimetypes[extensions[MD]]		= "text/plain";


			// FICHEROS BINARIOS
			mimetypes[extensions[JAR]]		= "application/java-archive";
			mimetypes[extensions[JPEG]]		= "image/jpeg";
			mimetypes[extensions[JPG]]		= "image/jpeg";
			mimetypes[extensions[PNG]]		= "image/png";
			mimetypes[extensions[GIF]]		= "image/gif";
			mimetypes[extensions[PSD]]		= "image/psd";

		}

		FUNCTION PRIVATE DEFINITION static void loadEvents
			(
				void
			)
		{
			webFiles["/"] = Session->toHtml();
			webFiles["utils.js"] = Session->toJavaScript();
			webFiles["dynamic.js"] = "void";
			webFiles["acction.do"] = "<html><head></head><body>ACCTION RESPONSE</body></html>";
			webFiles["grid.txt"] = "void";
			webFiles["getfiles.xml"] = "void";
			webFiles["fileexplorer.css"] = hwt::ccss::fexp_css;
		}

		FUNCTION PRIVATE DEFINITION static void loadJsLibs
			(
				void
			)
		{
			Vfile_t files;
			utl::listAllFilesOnFolder(Session->WebLibraryPath, files);
			Str_t content;
			for(Vfile_t::iterator it = files.begin(); it != files.end(); it++)
			{
				if (isBinaryFile(it->name))
				{
					if (utl::ReadFullBinFile(it->ruta, content) == false)
                    {
						errorln
							(
								utl::Stringf
								(
									"Fail Loading file : %s", it->ruta.c_str()
								)
							);
                    }
				}
				else
				{
					if (utl::ReadFullTxFile(it->ruta, content) == false)
                    {
						errorln
							(
							utl::Stringf
							(
								"Fail Loading file : %s",
								it->ruta.c_str()
							)
							);
                    }

				}
				webFiles[it->name] = content;
				content.clear();
			}
		}


		FUNCTION PRIVATE DEFINITION static bool isBinaryFile
			(
				const Str_t file
			)
		{
			size_t index = file.find_last_of(".");
			Str_t ext = file.substr(index);
			std::map<Str_t, bool>::iterator it;
			if ( ( it = binaryFiles.find(ext) ) == binaryFiles.end() )
            {
				errorln
				(
				utl::Stringf
				(
					"Unknown file extension : %s",
					file.c_str()
					)
				);
            }

			return it->second;
		}

		FUNCTION PRIVATE DEFINITION static int on_client_connect
			(
				void* , 
				const sockaddr* , 
				socklen_t  
			)
		{		
			return MHD_YES;
		}    
	        


		FUNCTION PRIVATE DEFINITION static int answer_to_connection
			(
				void *, 
				struct MHD_Connection * connection, 
				const Char_t *url,
				const Char_t *method,
				const Char_t *version, 
				const Char_t *upload_data, 
				size_t *upload_data_size, 
				void **con_cls
			)
		{
			#ifdef VERBOSE
				infoln(utl::Stringf(" %s %s %s", version, method, url));
			#endif        
			Str_t smethod(method);    
			if (*con_cls == NULL)
			{
				Connection_info_struct_t* con_info =
                    new (Connection_info_struct_t);
                
				if (con_info == NULL) return MHD_NO;
				con_info->answerstring.clear();

				if (smethod.compare(sendHtmlMethods[POST]) == 0)
				{
					con_info->postprocessor = MHD_create_post_processor
                                                (
                                                    connection,
                                                    POST_BUFFER_SIZE,
                                                    iterate_post,
                                                    (void *) con_info
                                                );
                    
					if (con_info->postprocessor == NULL)
					{
						#ifdef VERBOSE                    
							std::printf("[WARNING]"
                                        "[deploy::answer_to_connection]"
                                        "POST Request not datas to process.\n");
						#endif                    
						delete con_info;
						return MHD_NO;
					}
					con_info->connectiontype = POST;
				}
				else if (smethod.compare(sendHtmlMethods[GET]) == 0)	
				{
					con_info->connectiontype = GET;			
				}

				*con_cls = (void *) con_info;

				return MHD_YES;
			}

			if (smethod.compare(sendHtmlMethods[GET]) == 0)
			{
                Str_t page, mimetype;
                page_dispatcher(Str_t(url), page, mimetype);
                return send_page (connection, page, mimetype, url);
                
			}

			if (smethod.compare(sendHtmlMethods[POST]) == 0)
			{
				Connection_info_struct_t* con_info =
                    reinterpret_cast<Connection_info_struct_t*>(*con_cls);

				if (*upload_data_size != 0)
				{
					MHD_post_process
                        (
                            con_info->postprocessor,
                            upload_data, *upload_data_size
                         );
                    
					*upload_data_size = 0;
		            
					return MHD_YES;
				}
				else if (!con_info->post_key_value.empty())
				{     				
					handleEvents(con_info->post_key_value);
				}
			}
			Str_t page, mimetype;
            if (Session->turnON == true)
            {
                deployapp::page_dispatcher(Str_t(url), page, mimetype);
                return send_page (connection, page, mimetype, url);
            }
            else
            {
                return send_page
                    (
                        connection,
                        Str_t("<html><h1>Aplication ShutDown</h1></html>"),
                        mimetypes[extensions[HTML]],
						url
                    );
            }
		}

		FUNCTION PRIVATE DEFINITION static int send_page 
			(
				struct MHD_Connection* connection, 
				const Str_t& page, 
				const Str_t& mimetype,
				const Str_t& url
			)
		{
			int ret = 0;
			struct MHD_Response* response = NULL;
			
			response = MHD_create_response_from_data
                        (
                            page.size(),
                            (void*) page.c_str(),
                            MHD_NO, MHD_YES
                        );
            
			if (!response) return MHD_NO;
			MHD_add_response_header(response, "Content-Type", mimetype.c_str());            
			ret = MHD_queue_response (connection, MHD_HTTP_OK, response);
			MHD_destroy_response (response);

			infoln(utl::Stringf("%s , sent.", url.c_str()));
			return ret;    
		}    
	    
		FUNCTION PRIVATE DEFINITION static void request_completed
			(
				void* , 
				struct MHD_Connection* , 
				void** con_cls, 
				enum MHD_RequestTerminationCode 
			)
		{
			Connection_info_struct_t* con_info =
                reinterpret_cast<Connection_info_struct_t*>(*con_cls);

			if (con_info->connectiontype == POST && con_info != NULL)
			{
				MHD_destroy_post_processor(con_info->postprocessor);
				con_info->answerstring.clear();            
			}

			delete con_info;
			*con_cls = NULL;
		}    

		FUNCTION PRIVATE DEFINITION static int iterate_post 
			(	
				void *cls, 
				enum MHD_ValueKind, 
				const Char_t *key,
				const Char_t *, 
				const Char_t *,
				const Char_t *, 
				const Char_t *data,  
				uint64_t,
                size_t
			)
		{
	        
			Connection_info_struct_t* con_info =
                reinterpret_cast<Connection_info_struct_t*>(cls);
			#ifdef VERBOSE
			infoln(utl::Stringf(" Request [ %s, %s ]", key, data));
			#endif
			Str_t asciiKey, asciiData;
			utl::UTFToAny(data, Session->AianaEncoding.c_str(), asciiData);
			utl::UTFToAny(key, Session->AianaEncoding.c_str(), asciiKey);
			if (asciiKey.compare("json") == 0)
				utl::Json2Params(asciiData, con_info->post_key_value);
			else
				con_info->post_key_value[asciiKey] = asciiData;
			return (asciiKey.compare("endTransmision") == 0 && asciiData.compare("true") == 0)
                    ? MHD_NO : MHD_YES;

		}    
	    
		FUNCTION PRIVATE DEFINITION static void page_dispatcher
			(
				const Str_t& page_request, 
				Str_t& page_return, 
				Str_t& mimetype
			)
		{				
			if (page_request.empty())
            {
				errorln("Page request is empty");
            }

			Str_t tok = page_request.substr(0, 1);
			Str_t substr;
			
			if ( page_request.size() == 1 && (tok.compare("/") == 0) )
				substr = page_request.substr(0, 1);
			else if ( page_request.size() > 1 && (tok.compare("/") == 0) )
			{
				if (page_request.find("_grid.txt") != Str_t::npos)
					substr = page_request.substr(page_request.find_last_of("_") + 1);
				else
					substr = page_request.substr(page_request.find_last_of("/") + 1);
			}
			std::map<Str_t, Str_t>::iterator itm = webFiles.find(substr);
			if ( itm == webFiles.end())
            {
				errorln
				(
                 utl::Stringf
                 (
					"Page not found: %s",
                    page_request.c_str()
                  )
				);
            }
			else if (itm->first.compare("dynamic.js") == 0)
			{
				if (callsStack.empty())
					errorln("The Stack is empty.");
				page_return = createDynamicJS( callsStack.top() );
				mimetype = mimetypes[extensions[JS]];
				callsStack.pop();
			}
			else if (itm->first.find("grid.txt") != Str_t::npos)
			{
				page_return = getSerialGrid( page_request.substr(page_request.find_last_of("/") + 1) );
				mimetype = mimetypes[extensions[TXT]];
			}
			else if (itm->first.find("getfiles.xml") != Str_t::npos)
			{
				page_return = getFileList();
				mimetype = mimetypes[extensions[XML]];
			}
			else
			{
				if ( page_request.size() == 1)				
					mimetype = mimetypes[extensions[HTML]];
				else if ( page_request.size() > 1)				
					mimetype = mimetypes
					[
						page_request.substr(
							page_request.find_last_of(".") + 1)				
					];

				page_return = webFiles[substr];
			}
		}    

	}
}


