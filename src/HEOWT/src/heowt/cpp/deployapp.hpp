// SOURCE CHARSET ENCODING FOR WINDOWS   : US-ASCII
// SOURCE CHARSET ENCODING FOR LINUX, OS : ASCII

#ifndef _DEPLOYAPP_HPP
#define _DEPLOYAPP_HPP

#include "types.hpp" 
#include "session.hpp"
#include "utils.hpp"
#include "tools.hpp"
#include "createFileListXml.hpp"

extern "C"
{
	#include "microhttpd.h"
}

namespace hwt
{
	namespace deployapp
	{
		FUNCTION DECLARATION void Init
			(
				session* ss,
                sessionWidgets_t* mapofwidgets
			);
		
		FUNCTION DECLARATION void Destroy
            (
                void
            );
        
        
		FUNCTION DECLARATION void Run
			(
				void
			);
        
	}

}


#endif //_DEPLOYAPP_HPP

