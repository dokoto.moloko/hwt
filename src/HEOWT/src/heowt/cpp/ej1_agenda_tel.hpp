// SOURCE CHARSET ENCODING FOR WINDOWS   : Western European (Windows) - Codepage 1252
// SOURCE CHARSET ENCODING FOR LINUX, OS : 

#ifndef _EJ1_AGENDA_TEL_HPP
#define _EJ1_AGENDA_TEL_HPP 

#include <iostream>
#include <vector>
#include "hwt.hpp"


typedef struct
{
	hwt::Str_t	 nombre;
	hwt::Str_t	 apellidos;
	hwt::Str_t	 telFijo;
	hwt::Str_t	 telMovil;
	hwt::Str_t	 email;
	hwt::Str_t	 web;
	hwt::Str_t	 dir;
	hwt::Str_t	 fechaAlta;
} Contact_t;



extern const char* libPath;
extern std::vector<Contact_t> contatcs;

int ej1_agenda_tel(void);

void abrirVentana(const hwt::params_t& params, hwt::session& Session);
void gestionAccion(const hwt::params_t& params, hwt::session& Session);
void buscarContacto(const hwt::params_t& params, hwt::session& Session);
void cerraVentana(const hwt::params_t& params, hwt::session& Session);
void cerraVentanaAUTO(const hwt::params_t& params, hwt::session& Session);
void scrMenu(hwt::session& app);
void scrVisual(hwt::session& app);
void scrBuscar(hwt::session& app);
void srcListado(hwt::session& app);

std::vector<Contact_t>::iterator searchContact(const hwt::Str_t& name, std::vector<Contact_t>& contacts);
Contact_t loadContacts(const hwt::params_t& params, const hwt::session& ss);
void setContacts(const Contact_t& cc, hwt::session& ss);
void loadContactsFromFile(std::vector<Contact_t>& vContacts);
void convStr2vContacts(const hwt::Str_t& text, std::vector<Contact_t>& vContacts);
void convContacts2Str(const std::vector<Contact_t>& vContacts, hwt::Str_t& out);
void saveContactsInFile(const std::vector<Contact_t>& vContacts);
void DisableIputs(const bool disable, hwt::session& ss);
void DisableMain(const bool disable, hwt::session& ss);

void ClearInputValues(hwt::session& ss);

#endif // _EJ1_AGENDA_TEL_HPP
