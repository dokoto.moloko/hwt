// SOURCE CHARSET ENCODING FOR WINDOWS   : Western European (Windows) - Codepage 1252
// SOURCE CHARSET ENCODING FOR LINUX, OS : 

#include "main.hpp"
 
/*	
	TARGETS BETA 0.1:
	- Everything works in LINUX, OS X, Windows.
	- Widgets: Input(X), button(X), label(X), checkbox(X), listbox(X), grid(X), fileSelector(X)
	- Events: Button click(X), Close window(X)

	
	TARGETS BETA 0.2:
	- Establecer un ambito para widgets tl::clickEventParamsForWidget
	- Establecer tamaño maximo o algun tipo de validacion para los fileExplorers	
	- Cambiar todos los atributos a Setter() o Getter()
	- Cambiar line::modButton, modInput, modLabe por modWidget()
	- Pasar cosas como getDocbyId a tools.hpp y todas las herramientas de ese tipo encontradas en 
	  session, document, etc...
	- Contemplar la posibilidad de dividir class Widget en class Widget y class Event
	- File content transmision from browser to lib with Json
	- Deep tune to improve performance :
		* Str_t to Char_t*

*/


int main(int argc, char* argv[])
{
	return ej1_agenda_tel();
    //return eje2(argc, argv);
}




