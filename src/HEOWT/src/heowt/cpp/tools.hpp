// SOURCE CHARSET ENCODING FOR WINDOWS   : US-ASCII
// SOURCE CHARSET ENCODING FOR LINUX, OS : ASCII
#ifndef _TOOLS_HPP
#define _TOOLS_HPP

#include "types.hpp"
#include "document.hpp" 

namespace hwt
{
	namespace tl
	{
		FUNCTION PUBLIC DECLARATION void PRE_updateWidgetValues
			(
				const widget* w, 
				Buffer_t& buf,
                bool no_events = false
			);

		FUNCTION PUBLIC DECLARATION void POST_updateWidgetValues
			(
				const widget* w, 
				const sessionWidgets_t* ss,
				Buffer_t& buf,
				bool no_events = false
			);

		#define GETINDEX(key, secuence)\
		tl::getINDEX(key, (const char**)&secuence, sizeof(secuence)/sizeof(secuence[0]))
		FUNCTION PUBLIC DECLARATION int getINDEX
			(
				const Char_t* key,
				const char* const secuence[],
				const size_t secuenceLen
			);

	}
}

#endif // _TOOLS_HPP

