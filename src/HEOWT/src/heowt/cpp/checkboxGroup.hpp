// SOURCE CHARSET ENCODING FOR WINDOWS   : US-ASCII
// SOURCE CHARSET ENCODING FOR LINUX, OS : ASCII

#ifndef _CHECKBOXGROUP_HPP
#define _CHECKBOXGROUP_HPP

#include "types.hpp"
#include "widget.hpp"
#include "utils.hpp"
#include "webTags.hpp"
#include "tools.hpp"
#include "checkbox.hpp"

namespace hwt
{
	class checkboxGroup : public widget
	{
        private:
            COPY_CONSTRUCTOR PRIVATE DEFINITION checkboxGroup
                (
                    const checkboxGroup& b
                )
                : widget( b.Father, b.UniqueName, b.Labeltx, b.Width, b.Height, CHECKBOXGROUP, b.Visible, b.Disable, b.AianaEncoding.c_str() ),
                SessionWidgets(b.SessionWidgets){}
        
            OPERATOR PRIVATE DEFINITION checkboxGroup& operator=
                (
                    const checkboxGroup&
                ) { return *this; }
        
        private:
            sessionWidgets_t* SessionWidgets;
            std::vector<checkbox*> Checkbox_list;
        
        
        public:
            CONSTRUCTOR PUBLIC DECLARATION checkboxGroup
                (
                    sessionWidgets_t* sessionwidgets,
                    const Char_t* aianaencoding,
                    const uInt_t father,
                    const Str_t& uniquename = "",
                    const Str_t& labeltx = "",
                    const uInt_t width = 0,
                    const uInt_t height = 0,
                    const bool visible = true,
                    const bool disable = false
                 );
        
            DESTRUCTOR PUBLIC DECLARATION ~checkboxGroup
                (
                    void
                );
        
            METHOD PUBLIC DECLARATION Str_t toHtml
                (
                    bool no_event = false
                ) const;
        
			
            METHOD PUBLIC DECLARATION const Str_t toJavaScript
                (
                    bool no_events = false
                ) const;
			
			/*
            METHOD PUBLIC DECLARATION void toJScbValues
                (
                    Buffer_t& buf
                ) const;
			*/
            METHOD PUBLIC DECLARATION checkbox* Add
                (
                    const Str_t& uniquename = "",
                    const Str_t& labeltx = "",
                    const bool checked = false,
                    const uInt_t width = 0,
                    const uInt_t height = 0,
                    const bool visible = true,
                    const bool disable = false                    
                );
    };
}
#endif // _CHECKBOXGROUP_HPP


