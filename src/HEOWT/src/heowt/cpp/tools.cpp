// SOURCE CHARSET ENCODING FOR WINDOWS   : US-ASCII
// SOURCE CHARSET ENCODING FOR LINUX, OS : ASCII

#include "tools.hpp"

namespace hwt 
{
namespace tl
{

	FUNCTION PUBLIC DECLARATION static void update_inputs
		(
			const widget* w, 
			Buffer_t& buf,
			bool no_events
		);
    
	FUNCTION PUBLIC DECLARATION static void update_textArea
        (
            const widget* w,
            Buffer_t& buf,
            bool no_events
        );

	FUNCTION PUBLIC DECLARATION static void update_buttons
		(
			const widget* w, 
			Buffer_t& buf,
			bool no_events
		);

	FUNCTION PUBLIC DECLARATION static void update_listbox
		(
			const widget* w, 
			Buffer_t& buf		
		);

	FUNCTION PUBLIC DECLARATION static void update_checkboxGroup
		(
			const widget* w, 
			Buffer_t& buf		
		);

	FUNCTION PUBLIC DECLARATION static void update_grid
		(
			const widget* w, 
			Buffer_t& buf,
			bool no_events
		);

	FUNCTION PUBLIC DECLARATION static void update_fileexplorer
		(
			const widget* w, 
			Buffer_t& buf,
			bool no_events
		);

	FUNCTION PUBLIC DEFINITION static void update_inputs
		(
			const widget* w, 
			Buffer_t& buf,
			bool
		)
	{		
		buf << utl::Stringf(cjs::set_value,
							w->Uuid,
							static_cast<const input*>(w)->getconstUtf8Value().c_str());

		if (w->Width > 0 && w->Height > 0)
		{
			buf << utl::Stringf(cjs::widget_width,
								w->Uuid,
								w->Width);
		}
		if (w->Type == INPUT)
		{
			const input* i = static_cast<const input*>(w);
			buf << utl::Stringf(cjs::input_maxlen, w->Uuid, i->getconstMaxLen());			
		}
		if (w->Type == INPUT_DATE)
		{
			buf << utl::Stringf(cjs::imput_date_help, w->Uuid);
			buf << utl::Stringf(cjs::set_input_date, w->Uuid);			
		}
		buf << utl::Stringf(cjs::event_keypress, w->Uuid, w->Uuid, objets[w->Type]);
	}
    
	FUNCTION PUBLIC DEFINITION static void update_textArea
        (
            const widget* w,
            Buffer_t& buf,
            bool
        )
	{
		buf << utl::Stringf(cjs::set_value,
							w->Uuid,
							static_cast<const textArea*>(w)->getconstUtf8Value().c_str());
        
		if (w->Width > 0 && w->Height > 0)
		{
			buf << utl::Stringf(cjs::widget_width,
								w->Uuid,
								w->Width);
            
			buf << utl::Stringf(cjs::widget_height,
								w->Uuid,
								w->Height);
		}
        const textArea* i = static_cast<const textArea*>(w);
        buf << utl::Stringf(cjs::input_maxlen, w->Uuid, i->getconstMaxLen());
		buf << utl::Stringf(cjs::event_keypress, w->Uuid, w->Uuid, objets[w->Type]);
	}

	FUNCTION PUBLIC DEFINITION static void update_buttons
		(
			const widget* w, 
			Buffer_t& buf,
			bool no_events
		)
	{
		const button* bt = static_cast<const button*>(w);
		buf << bt->toJavaScript(no_events);
		buf << utl::Stringf(cjs::set_button, bt->Uuid);
	}

	FUNCTION PUBLIC DEFINITION static void update_listbox
		(
			const widget* w, 
			Buffer_t& buf
		)
	{
		const listbox* lb = static_cast<const listbox*>(w);
		buf << lb->toJavaScript();
	}

	FUNCTION PUBLIC DEFINITION static void update_checkboxGroup
		(
			const widget* w, 
			Buffer_t& buf
		)
	{
		const checkboxGroup* cbg = static_cast<const checkboxGroup*>(w);
		buf << cbg->toJavaScript();
	}

	FUNCTION PUBLIC DEFINITION static void update_grid
		(
			const widget* w, 
			Buffer_t& buf,
			bool no_events
		)
	{
		const grid* gd = static_cast<const grid*>(w);
		buf << gd->toJavaScript(no_events);
	}

	FUNCTION PUBLIC DEFINITION static void update_fileexplorer
		(
			const widget* w, 
			Buffer_t& buf,
			bool
		)
	{
		const fileexplorer* fe = static_cast<const fileexplorer*>(w);
		buf << fe->toJavaScript(false);
	}

	FUNCTION PUBLIC DEFINITION void PRE_updateWidgetValues
		(
			const widget* w, 
			Buffer_t& buf,
			bool no_events
		)
	{
		// Se inicializan sus valores por defecto		
		if (w->Type == INPUT || w->Type == INPUT_DATE)
		{			
			update_inputs(w, buf, no_events);
		}
		else if (w->Type == BUTTON)
		{					
			update_buttons(w, buf, no_events);
		}
		else if (w->Type == FILEEXPLORER)
		{
			update_fileexplorer(w, buf, no_events);
		}
		else if (w->Type == LISTBOX)
		{
			update_listbox(w, buf);
		}
		else if (w->Type == CHECKBOXGROUP)
		{
			update_checkboxGroup(w, buf);
		}
		else if (w->Type == TEXTAREA)
		{
			update_textArea(w, buf, no_events);
		}

		if (w->Visible == true)
			buf << utl::Stringf(cjs::set_show, w->Uuid);
		else if (w->Visible == false)
			buf << utl::Stringf(cjs::set_hide, w->Uuid);

		if (w->Disable == true)
			buf << utl::Stringf(cjs::set_disable, w->Uuid);
		else if (w->Disable == false)
			buf << utl::Stringf(cjs::set_enable, w->Uuid);
	}


	FUNCTION PUBLIC DEFINITION void POST_updateWidgetValues
		(
			const widget* w, 
			const sessionWidgets_t*,
			Buffer_t& buf,
            bool no_events
		)
	{
		Str_t utf8;
		if (w->Type == INPUT || w->Type == INPUT_DATE)
		{			
			update_inputs(w, buf, no_events);
		}
		else if (w->Type == BUTTON)
		{					
			update_buttons(w, buf, no_events);
		}
		else if (w->Type == GRID)
		{
				update_grid(w, buf, no_events);
		}
		else if (w->Type == FILEEXPLORER)
		{
			update_fileexplorer(w, buf, no_events);
		}
		else if (w->Type == LISTBOX)
		{
			update_listbox(w, buf);
		}
		else if (w->Type == CHECKBOXGROUP)
		{
			update_checkboxGroup(w, buf);
		}
		else if (w->Type == TEXTAREA)
		{
			update_textArea(w, buf, no_events);
		}

		if (w->Visible == true)
			buf << utl::Stringf(cjs::set_show, w->Uuid);
		else if (w->Visible == false)
			buf << utl::Stringf(cjs::set_hide, w->Uuid);

		if (w->Disable == true)
			buf << utl::Stringf(cjs::set_disable, w->Uuid);
		else if (w->Disable == false)
			buf << utl::Stringf(cjs::set_enable, w->Uuid);

	}

	
	FUNCTION PUBLIC DEFINITION int getINDEX
		(
			const Char_t* key,
			const char* const secuence[],
			const size_t secuenceLen
		)
	{
		for(int i = 0; i < static_cast<int>(secuenceLen); i++)
		{
			if (strncmp(key, secuence[i], strlen(secuence[i])) == 0)
				return i;
		}
		return -1;		
	}

}
}
