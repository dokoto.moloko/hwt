// SOURCE CHARSET ENCODING FOR WINDOWS   : US-ASCII
// SOURCE CHARSET ENCODING FOR LINUX, OS : ASCII

#include "label.hpp" 

namespace hwt
{
	CONSTRUCTOR PUBLIC DEFINITION label::label
		(
			sessionWidgets_t* sessionwidgets,
            const Char_t* aianaencoding,
			const uInt_t father,
			const Str_t& uniquename,
			const Str_t& labeltx, 			
			const uInt_t width, 
			const uInt_t height,
			const bool visible,
			const bool disable            
		) 
		
	:	widget(father, uniquename, labeltx, width, height, LABEL, visible, disable, aianaencoding )
	{
	}

	DESTRUCTOR PUBLIC DEFINITION label::~label
		(
			void
		)
	{
	}

	METHOD PUBLIC DEFINITION Str_t label::toHtml
		(
			bool
		) const
	{		
		return getconstUtf8Labeltx();					
	}
}


