// SOURCE CHARSET ENCODING FOR WINDOWS   : US-ASCII
// SOURCE CHARSET ENCODING FOR LINUX, OS : ASCII

#ifndef _LAYOUT_DEFS_HPP
#define _LAYOUT_DEFS_HPP

#define FUNCTION 
#define DECLARATION
#define DEFINITION
#define METHOD
#define PRIVATE
#define PROTECTED
#define PUBLIC
#define DESTRUCTOR
#define CONSTRUCTOR
#define COPY_CONSTRUCTOR
#define OPERATOR
#define FRIEND
#define VIRTUAL virtual
#define INLINE inline

#endif // _LAYOUT_DEFS_HPP

