// SOURCE CHARSET ENCODING FOR WINDOWS   : US-ASCII
// SOURCE CHARSET ENCODING FOR LINUX, OS : ASCII

#ifndef _LINE_HPP
#define	_LINE_HPP 

#include "types.hpp"
#include "button.hpp"
#include "input.hpp"
#include "label.hpp"
#include "grid.hpp"
#include "checkboxGroup.hpp"
#include "listbox.hpp"
#include "fileexplorer.hpp"
#include "textArea.hpp"

namespace hwt
{
    class session;
	class document;

    class line 
    {
	private:
        COPY_CONSTRUCTOR PRIVATE DEFINITION line
			(
				const line& l
             ) : SessionWidgets(l.SessionWidgets),
				 DocumentWidgets(l.DocumentWidgets),
				 Father(l.Father){}

		OPERATOR PRIVATE DEFINITION line& operator=
			(
				const line&
			) { return *this; }        
                
		METHOD PRIVATE DECLARATION widget* createWidget
			(
				enum objets_t type,
				const Str_t& uniquename,
				const Str_t& labeltx, 
				const uInt_t width, 
				const uInt_t height	
			);

		METHOD PRIVATE DECLARATION widget* advanceWidget
			(
				const size_t index
			);

        template<class type_widget>
        METHOD PRIVATE DEFINITION type_widget* modWidget
            (
                objets_t type,
                widget* w
            )
        {
            if (type != w->Type)
                errorln(utl::Stringf("This is not a %s, is a %s ", type, objets[w->Type]));
            
            return static_cast<type_widget*>(w); 		
        }

		METHOD PRIVATE DECLARATION void RemoveAllDepOfWidget
			(
				const uInt_t uuid,
				const Str_t& uniquename
			);

		METHOD PRIVATE DECLARATION void RemoveSessionDepOfWidget
			(
				const uInt_t uuid,
				const Str_t& uniquename
			);

		METHOD PRIVATE DECLARATION void RemoveDocumentDepOfWidget
			(
				const uInt_t uuid,
				const Str_t& uniquename
			);

		METHOD PRIVATE DECLARATION void RemoveLineDepOfWidget
			(
				const uInt_t uuid,
				const Str_t& uniquename
			);


	private:		
		lineWidgets_t LineWidgets;
		sessionWidgets_t* SessionWidgets;
		documentWidgets_t* DocumentWidgets;
		const uInt_t Father;
        Str_t AianaEncoding;
                
    public:
        friend class session;
        friend class document;

	public:		
		CONSTRUCTOR PUBLIC DECLARATION line
			(				
				sessionWidgets_t* sessionwidgets,
				documentWidgets_t* documentwidgets,
				const uInt_t father,
                const Char_t* aianaencoding
			); 

		DESTRUCTOR PUBLIC DECLARATION ~line
			(
				void
			); 

		METHOD PUBLIC DEFINITION const lineWidgets_t& getconstAllWidgets
			(
				void
			) const;

		METHOD PUBLIC DEFINITION lineWidgets_t& getAllWidgets
			(
				void
			);

		METHOD PUBLIC DECLARATION size_t AddWidget
			(				
				enum objets_t type,				
				const Str_t& uniquename = "",
				const Str_t& labeltx = "", 
				const uInt_t width = 200, 
				const uInt_t height = 100				
			);

		METHOD PUBLIC DECLARATION void DelWidget
			(
				const size_t index
			);

		METHOD PUBLIC DECLARATION void DelWidget
			(
				const Str_t& uniquename
			);

        template<typename T>
        METHOD PUBLIC DEFINITION T* ModWidget
            (
                const size_t index,
                objets_t type
            )
        {
            return modWidget<T>(type, advanceWidget(index));
        };
        
        
        template<typename T>
        METHOD PUBLIC DEFINITION T* ModWidget
            (
                const Str_t& uniquename,
                objets_t type
            )
        {
            byName_t::const_iterator it = LineWidgets.byName.find(uniquename);
            if (it == LineWidgets.byName.end())
                errorln("unKnown UniqueName.");
            return modWidget<T>(type, it->second);
        };
        
		METHOD PUBLIC DECLARATION button* ModButton
			(
				const size_t index
			);

		METHOD PUBLIC DECLARATION label* ModLabel
			(
				const size_t index
			);

		METHOD PUBLIC DECLARATION input* ModInput
			(
				const size_t index
			);

		METHOD PUBLIC DECLARATION button* ModButton
			(
				const Str_t& uniquename
			);

		METHOD PUBLIC DECLARATION label* ModLabel
			(
				const Str_t& uniquename
			);

		METHOD PUBLIC DECLARATION input* ModInput
			(
				const Str_t& uniquename
			);
    };
}
#endif	// _LINE_HPP

