// SOURCE CHARSET ENCODING FOR WINDOWS   : US-ASCII
// SOURCE CHARSET ENCODING FOR LINUX, OS : ASCII

#ifndef _WEBTAGS_HPP
#define _WEBTAGS_HPP
 
#include "types.hpp"

namespace hwt
{
	namespace chtml
	{
		extern const Char_t* session_title;
		extern const Char_t* dependencies;
		extern const Char_t* header_html;
		extern const Char_t* meta_header_html;
		extern const Char_t* container_minimiz;
		extern const Char_t* doc_div;
		extern const Char_t* doc_h4;
		extern const Char_t* input;
		extern const Char_t* button;	
        extern const Char_t* textArea;
		extern const Char_t* title;				
		extern const Char_t* html_open;		
		extern const Char_t* html_close;
		extern const Char_t* td_id_open;		
		extern const Char_t* td_open;
		extern const Char_t* tr_open;
		extern const Char_t* head_open;
		extern const Char_t* head_close;
		extern const Char_t* table_open;
		extern const Char_t* div_close;
		extern const Char_t* table_close;
		extern const Char_t* tr_close;
		extern const Char_t* td_close;
		extern const Char_t* body_open;
		extern const Char_t* body_close;
		extern const Char_t* void_line;
        extern const Char_t* popup_warn_close;
		extern const Char_t* error_page;
		extern const Char_t* dataTable_open;
		extern const Char_t* dataTable_header;
		extern const Char_t* dataTable_close;
		extern const Char_t* fexp_Html_div;
		extern const Char_t* checkbox;
		extern const Char_t* checkbox_checked;
		extern const Char_t* listbox_open;
		extern const Char_t* listbox_close;
		extern const Char_t* listbox_option;
		extern const Char_t* listbox_option_default;
	}

	namespace cjs
	{
		extern const Char_t* min_max;
		extern const Char_t* initDoc;
		extern const Char_t* intCheckBox;
		extern const Char_t* initDateInput;
		extern const Char_t* initDocument;
		extern const Char_t* initDocumentAuto;
        extern const Char_t* initMainDocumentAuto;
		extern const Char_t* open_ready;
		extern const Char_t* close_ready;
		extern const Char_t* set_value;
        extern const Char_t* widget_width;
        extern const Char_t* widget_height;
        extern const Char_t* imput_date_help;
        extern const Char_t* close_app;
		extern const Char_t* button_click_begin;		
		extern const Char_t* button_click_end;
		extern const Char_t* dynmic_func_call;
		extern const Char_t* dynmic_func_open;
		extern const Char_t* dynmic_func_close;
		extern const Char_t* div_empty;
		extern const Char_t* div_append;
		extern const Char_t* div_to_top;
		extern const Char_t* div_to_open;		
		extern const Char_t* div_to_close;
		extern const Char_t* div_to_enable;
		extern const Char_t* div_to_disable;
		extern const Char_t* div_to_destroy;
		extern const Char_t* key_value_pair;
		extern const Char_t* var;
		extern const Char_t* div_title;
		extern const Char_t* set_button;
		extern const Char_t* set_input;
		extern const Char_t* set_label;
		extern const Char_t* set_input_date;
		extern const Char_t* set_disable;
		extern const Char_t* set_enable;
		extern const Char_t* set_hide;
		extern const Char_t* set_show;
		extern const Char_t* set_win_disable;
		extern const Char_t* set_win_enable;
		extern const Char_t* alert;
		extern const Char_t* close_win_begin;
		extern const Char_t* close_win_end;
		extern const Char_t* dataTable_open;
		extern const Char_t* grid_var;
		extern const Char_t* grid_select_click;
		extern const Char_t* grid_get_properties;
		extern const Char_t* grid_func_get_properties;
		extern const Char_t* grid_key_value_pair;
		extern const Char_t* input_maxlen;
		extern const Char_t* checkbox_key_value_pair;
		extern const Char_t* fexp_OnClickButton;
		extern const Char_t* fexp_StringToXML;
		extern const Char_t* fexp_OnClickLink;
		extern const Char_t* fexp_fixWinPath;
		extern const Char_t* fexp_getFileList;
		extern const Char_t* arrayToJson;
		extern const Char_t* var_Map;
		extern const Char_t* event_keypress;
		extern const Char_t* event_click;
		extern const Char_t* event_change;
		extern const Char_t* event_focus;
	}

	namespace ccss
	{
		extern const Char_t* fexp_css;
	}

}

#endif // _WEBTAGS_HPP


