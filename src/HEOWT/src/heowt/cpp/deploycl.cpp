// SOURCE CHARSET ENCODING FOR WINDOWS   : US-ASCII
// SOURCE CHARSET ENCODING FOR LINUX, OS : ASCII

#include "deploycl.hpp" 

namespace hwt
{
	CONSTRUCTOR PUBLIC DECLARATION deploy::deploy
		(
			session* ss,
            sessionWidgets_t* mapofwidgets
		)
		: Session(ss),
          mapOfWidgets(mapofwidgets)
    
	{
        hwt::deployapp::Init(Session, mapOfWidgets);
        hwt::deployapp::Run();
	}

	DESTRUCTOR PUBLIC DECLARATION deploy::~deploy
		(
			void
		)
	{
		hwt::deployapp::Destroy();
	}
}

