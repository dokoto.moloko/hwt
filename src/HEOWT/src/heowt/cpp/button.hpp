// SOURCE CHARSET ENCODING FOR WINDOWS   : US-ASCII
// SOURCE CHARSET ENCODING FOR LINUX, OS : ASCII

#ifndef _BUTTON_HPP
#define _BUTTON_HPP 

#include "types.hpp"
#include "widget.hpp"
#include "utils.hpp"
#include "webTags.hpp"
#include "tools.hpp" 

namespace hwt
{
	class button : public widget
	{
		private:
			COPY_CONSTRUCTOR PRIVATE DEFINITION button
				(
					const button& b 
				)  
				:	widget( b.Father, b.UniqueName, b.Labeltx, b.Width, b.Height, BUTTON, b.Visible, b.Disable, b.AianaEncoding.c_str() ),
					SessionWidgets(b.SessionWidgets){}

			OPERATOR PRIVATE DEFINITION button& operator=
				(
					const button&
				) { return *this; }

		private:				
			sessionWidgets_t* SessionWidgets;

		public:
			CONSTRUCTOR PUBLIC DECLARATION button
				(	
					sessionWidgets_t* sessionwidgets,
                    const Char_t* aianaencoding,
					const uInt_t father,	
					const Str_t& uniquename = "",
					const Str_t& labeltx = "", 					
					const uInt_t width = 0,
					const uInt_t height = 0,	
					const bool visible = true,
					const bool disable = false
				);

			DESTRUCTOR PUBLIC DECLARATION ~button
				(
					void
				);

			METHOD PUBLIC DECLARATION Str_t toHtml
				(
					bool no_event = false
				) const;

			METHOD PUBLIC DECLARATION const Str_t toJavaScript
				(
					bool no_events = false
				) const;

	};
}

#endif //_BUTTON_HPP

