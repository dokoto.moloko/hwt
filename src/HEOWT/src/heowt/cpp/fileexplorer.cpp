// SOURCE CHARSET ENCODING FOR WINDOWS   : US-ASCII
// SOURCE CHARSET ENCODING FOR LINUX, OS : ASCII
#include "fileexplorer.hpp"

namespace hwt
{
	CONSTRUCTOR PUBLIC DEFINITION fileexplorer::fileexplorer
		(	
			sessionWidgets_t* sessionwidgets,
            const Char_t* aianaencoding,
			const uInt_t father,
			const Str_t& uniquename,
			const Str_t& labeltx, 			
			const uInt_t width, 
			const uInt_t height,
			const bool visible,
			const bool disable            
		) 
		:	widget(father, uniquename, labeltx, width, height, FILEEXPLORER, visible, disable, aianaencoding ),
            SessionWidgets(sessionwidgets),
			UuidLabelPtr(NULL),
			UuidInputPtr(NULL),
			UuidButtonPtr(NULL),
			UuidLabel(0),
			UuidInput(0),
			UuidButton(0)
			
	{		
		UuidLabelPtr = new uInt_t;
		UuidInputPtr = new uInt_t;
		UuidButtonPtr = new uInt_t;
		UuidLabel = reinterpret_cast<uInt_t>(UuidLabelPtr);
		UuidInput = reinterpret_cast<uInt_t>(UuidInputPtr);
		UuidButton = reinterpret_cast<uInt_t>(UuidButtonPtr);
	}

	DESTRUCTOR PUBLIC DEFINITION fileexplorer::~fileexplorer
		(
			void
		)
	{
		if (UuidLabelPtr != NULL)
		{
			delete UuidLabelPtr;
			UuidLabelPtr = NULL;
			UuidLabel = 0;
		}
		if (UuidInputPtr != NULL)
		{
			delete UuidInputPtr;
			UuidInputPtr = NULL;
			UuidInput = 0;
		}
		if (UuidButtonPtr != NULL)
		{
			delete UuidButtonPtr;
			UuidButtonPtr = NULL;
			UuidButton = 0;
		}
	}

	METHOD PUBLIC DEFINITION uInt_t fileexplorer::getconstUuidLabel
		(
			void
		) const { return UuidLabel; }

	METHOD PUBLIC DEFINITION uInt_t fileexplorer::getconstUuidInput
		(
			void
		) const { return UuidInput; }

	METHOD PUBLIC DEFINITION uInt_t fileexplorer::getconstUuidButton
		(
			void
		) const { return UuidButton; }

	METHOD PUBLIC DEFINITION const Str_t fileexplorer::getconstInputValue
		(
			void
		) const { return Value.value;}

	METHOD PUBLIC DEFINITION void fileexplorer::setInputValue
		(
			const value_t& value
		) { Value = value; }

	METHOD PUBLIC DEFINITION Str_t fileexplorer::toHtml
		(
			bool
		) const
	{
		Buffer_t buf;
		buf << chtml::td_open << Labeltx << chtml::td_close;		
		buf << chtml::td_open << utl::Stringf(chtml::input, UuidInput) << chtml::td_close;
		buf << chtml::td_open << utl::Stringf(chtml::button, UuidButton, "..") << chtml::td_close;
		return buf.str();
	}

	METHOD PUBLIC DEFINITION const Str_t fileexplorer::toJavaScript
		(
			bool
		) const
	{		
		Buffer_t buf;			
		buf << utl::Stringf(cjs::fexp_OnClickButton, UuidButton , Father, Uuid, 'D', "null", utl::FixWinPath(utl::CurrentDir()).c_str(), UuidInput, Uuid, Father);
		buf << utl::Stringf(cjs::event_keypress, UuidInput, UuidInput, objets[Type]);
		buf << utl::Stringf(cjs::event_click, UuidButton, UuidInput, objets[Type]);
		buf << utl::Stringf(cjs::set_button, UuidButton);	
		buf << utl::Stringf(cjs::widget_width, UuidInput, 200);
		buf << utl::Stringf(cjs::widget_height, UuidButton, 28);		
		buf << utl::Stringf(cjs::set_show, UuidInput);
		buf << utl::Stringf(cjs::set_enable, UuidInput);
		
		return buf.str();
	}

}


