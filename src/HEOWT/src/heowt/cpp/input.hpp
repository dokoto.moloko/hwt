// SOURCE CHARSET ENCODING FOR WINDOWS   : US-ASCII
// SOURCE CHARSET ENCODING FOR LINUX, OS : ASCII

#ifndef _INPUT_HPP 
#define _INPUT_HPP

#include "types.hpp"
#include "widget.hpp"
#include "utils.hpp"
#include "webTags.hpp"

namespace hwt
{
	class input : public widget
	{
		private:
			COPY_CONSTRUCTOR PRIVATE DEFINITION input
				(
					const input& i 
				)  :	widget(	i.Father, i.UniqueName, i.Labeltx, i.Width, i.Height, i.Type, i.Visible, i.Disable, i.AianaEncoding.c_str() ),
						SessionWidgets(i.SessionWidgets),
						MaxLen(i.MaxLen){}

			OPERATOR PRIVATE DEFINITION input& operator=
				(
					const input&
				) { return *this; }

			METHOD PRIVATE DECLARATION types_t checkType
				(
					void
				);

		private:
			value_t Value;
			sessionWidgets_t* SessionWidgets;
			uInt_t MaxLen;
		
		public:
			CONSTRUCTOR PUBLIC DECLARATION input
				(	
					sessionWidgets_t* sessionwidgets,
                    const Char_t* aianaencoding,
					const uInt_t father,	
					const Str_t& uniquename = "",
					const Str_t& labeltx = "", 					
					const uInt_t width = 0,
					const uInt_t height = 0,
                    enum objets_t type = INPUT,
					const bool visible = true,
					const bool disable = false,
					const uInt_t maxlen = 50
				);

			DESTRUCTOR PUBLIC DECLARATION ~input
				(
				);
        
			METHOD PUBLIC DECLARATION Str_t toHtml
				(
					bool no_event = false
				) const;

            METHOD PUBLIC DECLARATION const value_t getconstValue
                (
                    void
                 ) const;

			METHOD PUBLIC DECLARATION const Str_t getconstUtf8Value
				(
					void
				) const;

			METHOD PUBLIC DECLARATION void setMaxLen
				(
					uInt_t len
				);

			METHOD PUBLIC DECLARATION uInt_t getconstMaxLen
				(
					void
				) const;
        
			METHOD PUBLIC DECLARATION const Str_t toJavaScript
				(
					bool 
				) const;

			METHOD PUBLIC DEFINITION void setValue
				(
					int v
				) { Value = hwt::utl::Any2Value(v); }

			METHOD PUBLIC DEFINITION void setValue
				(
					char v
				) { Value = hwt::utl::Any2Value(v); }

			METHOD PUBLIC DEFINITION void setValue
				(
					Str_t v
				) { Value = hwt::utl::Any2Value(v); }

			METHOD PUBLIC DEFINITION void setValue
				(
					wchar_t* v
				) { Value = hwt::utl::Any2Value(v); }

			METHOD PUBLIC DEFINITION void setValue
				(
					float v
				) { Value = hwt::utl::Any2Value(FLOAT(v)); }

			METHOD PUBLIC DEFINITION void setValue
				(
					double v
				) { Value = hwt::utl::Any2Value(v); }

			METHOD PUBLIC DEFINITION void setValue
				(
					long v
				) { Value = hwt::utl::Any2Value(LONG(v)); }
			
	};
}

#endif //_INPUT_HPP

