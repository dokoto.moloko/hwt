// SOURCE CHARSET ENCODING FOR WINDOWS   : US-ASCII
// SOURCE CHARSET ENCODING FOR LINUX, OS : ASCII

#include "button.hpp"

namespace hwt
{
	CONSTRUCTOR PUBLIC DEFINITION button::button
		(
			sessionWidgets_t* sessionwidgets,
            const Char_t* aianaencoding,
			const uInt_t father,
			const Str_t& uniquename,
			const Str_t& labeltx, 			
			const uInt_t width, 
			const uInt_t height,
			const bool visible,
			const bool disable           
		) 
		
	:	widget(father, uniquename, labeltx, width, height, BUTTON, visible, disable, aianaencoding ),
		SessionWidgets(sessionwidgets)
	{		
	}

	DESTRUCTOR PUBLIC DEFINITION button::~button
		(
			void
		)
	{
	}

	METHOD PUBLIC DEFINITION Str_t button::toHtml
		(
			bool
		) const
	{
		return hwt::utl::Stringf
            (
                hwt::chtml::button,
                this->Uuid,
                this->getconstUtf8Labeltx().c_str()
            );       
	}

	METHOD PUBLIC DECLARATION const Str_t button::toJavaScript
		(
			bool no_events
		) const
	{
		Buffer_t buf;
		// Inicializacion
		buf << utl::Stringf
			(
				cjs::widget_width,
				Uuid,
				Width,
				Height
			);
	
        if (no_events == false)
        {
			scope_t scopeClick = getconstEventScopeFull(ONCLICK);
			if (scopeClick.scope != NO_SCOPE)
			{
				// Init click event
				buf << utl::Stringf(cjs::button_click_begin, Uuid, Father, Uuid, utl::Stringf(cjs::dynmic_func_call, Father).c_str());
				/*
				// Params			
				switch (scopeClick.scope)
				{
					case APPLICATION:
						tl::clickEventParamsForApplication(SessionWidgets, buf);
						break;
					case WINDOW:
						tl::clickEventParamsForWindow(SessionWidgets, Father, buf);
						break;
					case WIDGET:
						tl::clickEventParamsForWidget(scopeClick, buf);
						break;
					default:
						errorln("Unknown event button Scope.");
				}
				// End of click event
				buf << utl::Stringf(cjs::button_click_end, utl::Stringf(cjs::dynmic_func_call, Father).c_str());
				*/
			}
        }
		return buf.str();
	}

}

