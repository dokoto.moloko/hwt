// SOURCE CHARSET ENCODING FOR WINDOWS   : US-ASCII
// SOURCE CHARSET ENCODING FOR LINUX, OS : ASCII

#ifndef _TEXTAREA_HPP
#define _TEXTAREA_HPP

#include "types.hpp"
#include "widget.hpp"
#include "utils.hpp"
#include "webTags.hpp"

namespace hwt
{
	class textArea : public widget
	{
        private:
            COPY_CONSTRUCTOR PRIVATE DEFINITION textArea
                (
                    const textArea& i
                )  :	widget(	i.Father, i.UniqueName, i.Labeltx, i.Width, i.Height, i.Type, i.Visible, i.Disable, i.AianaEncoding.c_str() ),
                    SessionWidgets(i.SessionWidgets),
                    MaxLen(i.MaxLen){}
        
            OPERATOR PRIVATE DEFINITION textArea& operator=
                (
                    const textArea&
                ) { return *this; }
        
            METHOD PRIVATE DECLARATION types_t checkType
                (
                    void
                );
        
        private:
            Buffer_t Value;
            sessionWidgets_t* SessionWidgets;
            uInt_t MaxLen;
		
        public:
            CONSTRUCTOR PUBLIC DECLARATION textArea
                (
                    sessionWidgets_t* sessionwidgets,
                    const Char_t* aianaencoding,
                    const uInt_t father,
                    const Str_t& uniquename = "",
                    const Str_t& labeltx = "",
                    const uInt_t width = 0,
                    const uInt_t height = 0,
                    enum objets_t type = TEXTAREA,
                    const bool visible = true,
                    const bool disable = false,
                    const uInt_t maxlen = 2048
                 );
        
            DESTRUCTOR PUBLIC DECLARATION ~textArea
                (
                );
        
            METHOD PUBLIC DECLARATION Str_t toHtml
                (
                    bool no_event = false
                ) const;
        
            METHOD PUBLIC DECLARATION const Str_t getconstValue
                (
                    void
                ) const;
        
            METHOD PUBLIC DECLARATION const Str_t getconstUtf8Value
                (
                    void
                ) const;
        
            METHOD PUBLIC DECLARATION void setMaxLen
                (
                    uInt_t len
                );
        
            METHOD PUBLIC DECLARATION uInt_t getconstMaxLen
                (
                    void
                ) const;
        
            METHOD PUBLIC DEFINITION void setValue
                (
                    const Str_t& value
                ) { Value << value; }
        
        
	};
}

#endif //_TEXTAREA_HPP