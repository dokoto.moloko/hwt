// SOURCE CHARSET ENCODING FOR WINDOWS   : US-ASCII
// SOURCE CHARSET ENCODING FOR LINUX, OS : ASCII

#include "webTags.hpp"
 
namespace hwt
{

namespace chtml
{

	const Char_t* html_open = "<html>\n";		
	const Char_t* html_close = "</html>\n";
	const Char_t* td_id_open= "<td id =\"%l\">\n";
	const Char_t* div_close = "</div>\n";
	const Char_t* table_close = "</table>\n";
	const Char_t* td_open = "<td>\n";
	const Char_t* tr_open = "<tr>\n";
	const Char_t* tr_close = "</tr>\n";
	const Char_t* td_close = "</td>\n";
	const Char_t* head_open = "<head>\n";
	const Char_t* head_close = "</head>\n";
	const Char_t* body_open = "<body>\n";
	const Char_t* body_close = "</body>\n";
	const Char_t* void_line = "<td><br></td>\n";

	const Char_t* error_page = "\
	<html>\n\
		<head>\n\
			<title>webserver derived class</title>\n"
			"<meta http-equiv=\"Content-Type\" content=\"text/html\"; charset=\"utf-8\" />\n"
		"</head>\n\
		<body  bgcolor=\"White\">\n\
			<h4>Upps! algo ha salido mal, revisa los logs.</h4><br><br>\n\
			<h1>Salud ;)</h1>\n\
		</body>\n\
	</html>\n\
	";

	/*
	 * <title>HWT PROTO - SESSION TITLE o APPLICATION TITLE</title>
	 */
	const Char_t* title = "<title>%s</title>\n";

	/*
	 * <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd"
	 */

	const Char_t* header_html = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01//EN\" \"http://www.w3.org/TR/html4/strict.dtd\"\n";

	/*
	 * <meta meta name="App-server" content="text/html;" http-equiv="content-type" charset="utf-8" />
	 */

	const Char_t* meta_header_html = "<meta meta name=\"App-server\"  content=\"text/html;\" http-equiv=\"content-type\" charset=\"utf-8\" />\n";

	const Char_t* dependencies = "\
	<link rel=\"icon\" type=\"image/png\" href=\"minicon.png\" />\n\
	<link type=\"text/css\" href=\"jquery-ui-1.9.0.custom.css\" rel=\"Stylesheet\" />\n\
	<script type=\"text/javascript\" src=\"jquery-1.8.2.js\"></script>\n\
	<script type=\"text/javascript\" src=\"jquery-ui-1.9.0.custom.js\"></script>\n\
	<script type=\"text/javascript\" src=\"jquery.dataTables.min.js\"></script>\n\
	<style type=\"text/css\">\n\
	@import \"demo_page.css\";\n\
	@import \"demo_table.css\";\n\
	</style>\n\
	<script type=\"text/javascript\" src=\"utils.js\"></script>\n\
	<link type=\"text/css\" href=\"fileexplorer.css\" rel=\"Stylesheet\" />\n\
	";


	const Char_t* container_minimiz = "<div id=\"dialog_window_minimized_container\"></div>\n";

	/*
	 * @param %l uuid
	 * @param %s div name
	 * @param %s div Title
	 */
	const Char_t* doc_div ="<div id=\"%l\" class=\"%s\" title=\"%s\">\n";

	/*
	 * @param %s Text in H4 size
	 */
	const Char_t* doc_h4="<h4>%s</h4>\n";
	const Char_t* table_open = "<table class=\"dialog_form\">\n";

	/*
	 * @param %l uuid
	 */
	const Char_t* input = "<input type=\"text\" id=\"%l\" />\n";

	/*
	 * @param %l uuid
	 * @param %s Text of button
	 */
	const Char_t* button = "<button id=\"%l\">%s</button>\n";

	/*
	 * @param %l uuid
     * @param %d Heith in Rows
	 * @param %d Width in cols
     * @param %s Text content
	 */
    const Char_t* textArea = "<textarea id=\"%l\">%s</textarea>";
    
	/**
	 * @param %s title
	 */
	const Char_t* session_title = "<title>%s</title>\n";
	    
	    
	const Char_t* popup_warn_close = "\
		<div id=\"dialog-confirm\" title=\"Do you realy want to close the application ?\">\n\
			<p><span class=\"ui-icon ui-icon-alert\"></span>This acction will finalize the aplication. Are you sure?</p>\n\
		</div>\n\
	";

	/*
	 * @param %l uuid
	 */
	const Char_t* dataTable_open = "<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" class=\"display\" id=\"%l\"><thead><tr>\n";

	/*
	 * @param %l Header Name
	 */
	const Char_t* dataTable_header = "<th>%s</th>\n";


	const Char_t* dataTable_close = "</tr></thead><tbody></tbody></table>\n";


	/**
	 * @param %l uuid
	 */
	const Char_t* fexp_Html_div = "<div id=\"fileExplorer\"></div>\n";

	/**
	 * @param %l listbox uuid
	 */
	const Char_t* listbox_open = "<select id=\"%l\">\n";

	const Char_t* listbox_close = "</select>\n";

	/**
	 * @param %s option listbox key
	 * @param %s option listbox value
	 */
	const Char_t* listbox_option = "<option value=\"%s\">%s</option>\n";

	/**
	* @param %s option listbox key
	* @param %s option listbox value
	*/
	const Char_t* listbox_option_default = "<option value=\"%s\" selected=\"selected\">%s</option>\n";

	/**
	* @param %s checkbox uuid
	* @param %s checkbox uuid
	* @param %s option listbox text
	*/
	const Char_t* checkbox  = "<input type=\"checkbox\" id=\"%l\" /><label for=\"%l\">%s</label>\n";

/**
	* @param %s checkbox uuid
	* @param %s checkbox uuid
	* @param %s option listbox text
	*/
	const Char_t* checkbox_checked  = "<input type=\"checkbox\" id=\"%l\" checked/><label for=\"%l\">%s</label>\n";
    
}

namespace cjs
{    	
	const Char_t* initDoc = "\
	function initDoc(doc)\n\
	{\n\
		$(doc).dialog\n\
			(\n\
				{\n\
					width: 'auto',\n\
					height: 'auto'\n\
				}\n\
			);\n\
	}\n\
";
	/*
	 * @param %l uuid
	 */
	const Char_t* intCheckBox = "$('#%l').buttonset();\n";
	
	/*
	 * @param %l uuid
	 */
	const Char_t* initDateInput = "$('#%l').datepicker();\n";

	/**
	 * @param %l uuid	 	 
	 */
	const Char_t* initMainDocumentAuto="\
	$('#%l').dialog\n\
	(\n\
		{\n\
			width: 'auto',\n\
			height: 'auto'\n\
		}\n\
	);\n\
";
        
	/**
	 * @param %l uuid	 
	 * @param %l width	 
	 * @param %l height	 
	 */
    const Char_t* initDocument="\
    $('#%l').dialog\n\
    (\n\
        {\n\
            width: '%l',\n\
            height: '%l'\n\
        }\n\
     );\n\
";

	const Char_t* initDocumentAuto="\
	$('#%l').dialog\n\
	(\n\
		{\n\
			width: 'auto',\n\
			height: 'auto'\n\
		}\n\
	);\n\
";
        
	const Char_t* open_ready = "\
	// INICIALIZACION\n\
	$(document).ready\n\
	(\n\
		function()\n\
			{\n\
";
	const Char_t* close_ready = "\
			}\n\
	);\n\
";
    
	/**
	 * @param %l uuid	 
	 * @param %s value
	 */
    const Char_t* set_value = "$('#%l').val('%s');\n";

	/**
	 * @param %l uuid	 
	 * @param %l width
	 */
    const Char_t* widget_width = "$('#%l').width(%l);\n";

	/**
	 * @param %l uuid	 
	 * @param %l height
	 */
    const Char_t* widget_height = "$('#%l').height(%l);\n";

	/**
	 * @param %l uuid	 
	 */
    const Char_t* imput_date_help = "$('#%l').datepicker();\n";
    
	/**
	 * @param %l uuid	 
	 * @param %l uuid
	 * @param %s event
     * @param %s javascript code for close all windows opened     
	 * @param %l uuid from caller window
	 */
    const Char_t* close_app = "\
    $('#%l').bind('dialogclose',\n\
    function(event)\n\
    {\n\
	\n\
        /*if (minimize_event == true)\n\
        {\n\
            minimize_event = false;\n\
        }\n\
        else if(minimize_event == false)\n\
        {*/\n\
            $( \"#dialog-confirm\" ).dialog({\n\
            resizable: false,\n\
            width: 'auto',\n\
            height: 'auto',\n\
            modal: true,\n\
            buttons: {\n\
                \"Continue\": function() {\n\
					$( this ).dialog( \"close\" );\n\
					$.post\n\
					(\n\
                     \"acction.do\", { caller: \"%l\", event: \"%s\", turnON: \"false\", endTransmision: \"true\" },\n\
                     function(data)\n\
                     {\n\
                         %s\n\
                         alert('Application finalized');\n\
                     }\n\
                     );\n\
                },\n\
            Cancel: function() {\n\
				$('#%l').dialog(\"open\");\n\
                //minimize_event = false;\n\
                $( this ).dialog( \"close\" );\n\
            }\n\
            }\n\
            });\n\
       // }\n\
    });\n\
";


	/**
	 * @param %l Window uuid 
	 * @param %l Father Document uuid
	 * @param %s Comandos Javascript
	 */
    const Char_t* close_win_begin = "\
    $('#%l').bind('dialogclose',\n\
    function(event)\n\
    {\n\
			$.ajax\n\
			({\n\
				type: \"POST\",\n\
				url: \"acction.do\",\n\
				async: false,\n\
				data:\n\
					{\n\
						caller: \"%l\",\n\
						event: \"CLOSE_WIN\",\n\
						json: JSON.stringify(arrayToJson(mapParams)),\n\
						endTransmision: \"true\"\n\
					}\n\
			}).done\n\
			(\n\
				function(data)\n\
				{\n\
					mapParams = {};\n\
					%s\n\
				}\n\
			).fail\n\
			(\n\
				function(jqXHR, textStatus)\n\
					{\n\
						mapParams = {};\n\
						alert( \"Check traces, request failed: \" + textStatus );\n\
					}\n\
			);\n\
    });\n\
";
	
	/**
	 * @param %l Button uuid
	 * @param %l Father Document uuid
	 * @param %l Button uuid
	 * @param %s Comandos Javascript
	 */
	const Char_t* button_click_begin = "\
	$('#%l').button().click\n\
	(\n\
	   function()\n\
		{\n\
			$(\"#%l\").dialog(\"disable\");\n\
			$.ajax\n\
			({\n\
				type: \"POST\",\n\
				url: \"acction.do\",\n\
				async: false,\n\
				data:\n\
					{\n\
						caller: \"%l\",\n\
						event: \"ONCLICK\",\n\
						json: JSON.stringify(arrayToJson(mapParams)),\n\
						endTransmision: \"true\"\n\
					}\n\
			}).done\n\
			(\n\
			function(data)\n\
				{\n\
					mapParams = {};\n\
					%s\n\
				}\n\
			).fail\n\
			(\n\
			function(jqXHR, textStatus)\n\
				{\n\
					mapParams = {};\n\
					alert( \"Check traces, request failed: \" + textStatus );\n\
				}\n\
			);\n\
		}\n\
	);\n\
";

	/**
	 * @param %l uuid	 
	 */
	const Char_t* div_empty  = "$('#%l').empty();\n";

	/**
	 * @param %l uuid	 
	 * @param %s Contenido HTML de la ventana
	 */
	const Char_t* div_append = "$('#%l').append('%s');\n";

	/**
	 * @param %l uuid	 
	 */
	const Char_t* div_to_top = "$('#%l').dialog(\"moveToTop\");\n";
	const Char_t* div_to_open = "$('#%l').dialog(\"open\");\n";
	const Char_t* div_to_close = "$('#%l').dialog(\"close\");\n";
	const Char_t* div_to_enable = "$('#%l').dialog(\"enable\");\n";
	const Char_t* div_to_disable = "$('#%l').dialog(\"disable\");\n";
	const Char_t* div_to_destroy = "$('#%l').dialog(\"destroy\");\n";

	/**
	 * @param %l Father Document uuid
	 */
	const Char_t* dynmic_func_call ="\
	$.getScript(\n\
	\"dynamic.js\",\n\
	function()\n\
	{\n\
		exec_post_actions();\n\
		$(\"#%l\").dialog(\"enable\");\n\
	}\n\
	).fail(function(jqXHR, textStatus)\n\
	{\n\
		alert( \"Check traces, request failed: \" + textStatus );\n\
	});";

	const Char_t* dynmic_func_open = "function exec_post_actions()\n{\n";
	const Char_t* dynmic_func_close = "}\n";

	/**
	 * @param %l uuid	 
	 * @param %l uuid(param 1)
	 */
	const Char_t* key_value_pair = "\
						\"%l\" : $('#%l').val(),\n\
";

	/**
	 * @param %l uuid
	 * @param %l uuid
	 */
	const Char_t* grid_key_value_pair = "\
						\"%l\" : getRowSelect(o%l),\n\
";

	/**
	 * @param %l uuid
	 * @param %l uuid	 
	 */
	const Char_t* checkbox_key_value_pair = "\"%l\" :  $('#%l').attr('checked') ? \"on\" : \"off\";\n";

	/**
	 * @param %l uuid	 
	 * @param %l New title
	 */
	const Char_t* div_title = "$('#%l').dialog('option', 'title', '%s');\n";

	/**
	 * @param %l uuid	 	
	 */
	const Char_t* set_button = "$('#%l').button();\n";

	/**
	 * @param %l uuid	 	
	 */
	const Char_t* set_input_date = "$('#%l').datepicker();\n";

	
	/**
	 * @param %l uuid	 	
	 */
	const Char_t* set_disable = "$('#%l').attr(\"disabled\", true);\n";

	/**
	 * @param %l uuid	 	
	 */
	const Char_t* set_enable = "$('#%l').attr(\"disabled\", false);\n";

	/*
	 * @param %l uuid
	 */
	const Char_t* set_hide = "$('#%l').hide();\n";

	/*
	 * @param %l uuid
	 */
	const Char_t* set_show = "$('#%l').show();\n";

	/**
	 * @param %l uuid	 	
	 */
	const Char_t* set_win_disable = "$(\"#%l\").dialog(\"disable\");\n";

	/**
	 * @param %l uuid	 	
	 */
	const Char_t* set_win_enable = "$(\"#%l\").dialog(\"enable\");\n";

	/**
	 * @param %l text
	 */
	const Char_t* alert = "alert(\"%s\");\n";

	/**
	 * @param %l uuid 
	 * @param %l uuid 
	 * @param %d Number of lines per Page
	 * @param %l uuid
	 */
	const Char_t* dataTable_open ="\
	o%l = $('#%l').dataTable( {\n\
		\"bServerSide\": false,\n\
		\"bProcessing\": true,\n\
		\"iDisplayLength\": %d,\n\
		\"sAjaxSource\": \"%l_grid.txt\",\n\
		\"fnServerData\": function ( sSource, aoData, fnCallback )\n\
		{\n\
			$.getJSON( sSource, aoData, function (json)\n\
			{\n\
				fnCallback(json)\n\
			} );\n\
		}\n\
	} );\n\
";

	/**
	 * @param %l uuid	 	
	 */
	const Char_t* grid_var = "var o%l;\n";

	/**
	 * @param %l uuid	 
	 * @param %l uuid	
	 * @param %l uuid
	 */
	const Char_t* grid_select_click ="\
	$(\"#%l tbody\").delegate(\"tr\", \"click\", function(e)\n\
	{\n\
		mapParams['%l'] = 'GRID';\n\
		if ( $(this).hasClass('row_selected') ) {\n\
			$(this).removeClass('row_selected');\n\
		}\n\
		else {\n\
			o%l.$('tr.row_selected').removeClass('row_selected');\n\
			$(this).addClass('row_selected');\n\
		}\n\
	});\n\
";

	const Char_t* grid_func_get_properties ="\
	function getRowSelect(oGrid){\n\
		if (typeof oGrid === \"undefined\") return -1;\n\
		var anSelected = oGrid.$('tr.row_selected');\n\
		if (typeof anSelected === \"undefined\") return -1;\n\
		if (anSelected.length == 0) return -1;\n\
		var oSettings = oGrid.fnSettings();\n\
		if (typeof oSettings === \"undefined\") return -1;\n\
		return oSettings._iDisplayStart+anSelected[0].rowIndex;\n\
	}\n\
";	
	const Char_t* grid_row_selected = "oSettings._iDisplayStart+anSelected[0].rowIndex";


	/**
	 * @param %l fileExplorer Button uuid
	 * @param %l Father Document uuid
	 * @param %l fileExplorer uuid
	 * @param %c Type (Folder: 'D' or File : 'F' )
	 * @param %s File or Folder name or null
	 * @param %s Path 
	 * @param %l Input target Uuid
	 * @param %l fileExplorer uuid
	 * @param %l window caller
	 * 
	 */
	const Char_t* fexp_OnClickButton = "\
	$('#%l').button().click\n\
	(\n\
	   function()\n\
		{\n\
			$(\"#%l\").dialog(\"disable\");\n\
			$.ajax\n\
			({\n\
				type: \"POST\",\n\
				url: \"getfiles.xml\",\n\
				async: false,\n\
				dataType: ($.browser.msie) ? 'text' : 'xml',\n\
				data:\n\
					{\n\
						caller: \"%l\",\n\
						event: \"GETLISTOFFILES\",\n\
						tipo: \"%c\",\n\
						nombre: \"%s\",\n\
						ruta: fixWinPath(\"%s\"),\n\
						endTransmision: \"true\"\n\
					}\n\
			}).success\n\
			(\n\
			function(data)\n\
				{\n\
					printFileList(data, %l, %l, %l);\n\
				}\n\
			).fail\n\
			(\n\
			function(jqXHR, textStatus)\n\
				{\n\
					alert( \"Check traces, request failed: \" + textStatus );\n\
				}\n\
			);\n\
		}\n\
	);\n\
";

	const Char_t* fexp_StringToXML ="\
function StringtoXML(text)\n\
	{\n\
		if (text == null)\n\
			return null;\n\
		if (text.length == 0)\n\
			return null;\n\
		if (window.ActiveXObject)\n\
		{\n\
			var doc=new ActiveXObject('Microsoft.XMLDOM');\n\
			doc.async='false';\n\
			doc.loadXML(text);\n\
			if (doc.errorCode != 0)\n\
			{\n\
				document.write(\"Error code: \" + doc.parseError.errorCode);\n\
				document.write(\"Error reason: \" + doc.parseError.reason);\n\
				document.write(\"Error line: \" + doc.parseError.line); \n\
				document.write(\"Error source: \" + doc.parseError.srcText);\n\
				return null;\n\
			}\n\
		}\n\
		else\n\
		{\n\
			var parser=new DOMParser();\n\
			var doc=parser.parseFromString(text,'text/xml');\n\
			if (doc.documentElement.nodeName==\"parsererror\")\n\
			{\n\
				var errStr = doc.documentElement.childNodes[0].nodeValue;\n\
				errStr = errStr.replace(/</g, \"&lt;\");\n\
				document.write(\"Error code: \" + errStr);\n\
				return null;\n\
			}\n\
		}\n\
		return doc;\n\
	}\n\
";

	const Char_t* fexp_OnClickLink ="\
	function printFileList(XmlDoc, InputID, caller, WinCaller)\n\
	{\n\
		var div_links = document.getElementById('fileExplorer');\n\
		div_links.innerHTML = \"\";\n\
		if (XmlDoc != null)\n\
		{\n\
			var links_tag = XmlDoc.getElementsByTagName(\"lista\")[0].getElementsByTagName(\"item\");\n\
			for (var i = 0; i < links_tag.length; i++)\n\
			{\n\
				var tipo = links_tag[i].getElementsByTagName(\"tipo\")[0].childNodes[0].nodeValue;\n\
				var nombre = links_tag[i].getElementsByTagName(\"nombre\")[0].childNodes[0].nodeValue;\n\
				var ruta = links_tag[i].getElementsByTagName(\"ruta\")[0].childNodes[0].nodeValue;\n\
				ruta = fixWinPath(ruta);\n\
				div_links.innerHTML += \"<p id ='fileLine'  onclick='javascript:getFileList(\\\"\" + InputID + \"\\\",\\\"\" + caller + \"\\\",\\\"\" + tipo + \"\\\",\\\"\" + nombre + \"\\\",\\\"\" + ruta + \"\\\",\\\"\" + WinCaller + \"\\\")'>\" + nombre + \"</p>\";\n\
			}\n\
			document.getElementById('fileExplorer').style.zIndex='1000000';\n\
			document.getElementById('fileExplorer').style.visibility='visible';\n\
		} else alert('[ERROR] No se han recuperado datos del servidor.');\n\
	}\n\
";

	const Char_t* fexp_getFileList="\
	function getFileList(InputID, caller, tipo, nombre, ruta, WinCaller)\n\
	{\n\
		if (tipo == 'F')\n\
		{\n\
			$(\"#\"+InputID).val(ruta);\n\
			document.getElementById('fileExplorer').style.visibility='hidden';\n\
			$(\"#\"+WinCaller).dialog(\"enable\");\n\
			return;\n\
		}\n\
		$.ajax\n\
			({\n\
				type: \"POST\",\n\
				url: \"getfiles.xml\",\n\
				async: false,\n\
				dataType: ($.browser.msie) ? 'text' : 'xml',\n\
				data:\n\
					{\n\
						caller: caller,\n\
						event: \"GETLISTOFFILES\",\n\
						tipo: tipo,\n\
						nombre: nombre,\n\
						ruta: ruta,\n\
						endTransmision: \"true\"\n\
					}\n\
			}).success\n\
			(\n\
				function(data)\n\
				{\n\
					printFileList(data, InputID, caller, WinCaller);\n\
				}\n\
			).fail\n\
			(\n\
				function(jqXHR, textStatus)\n\
				{\n\
					alert( \"Check traces, request failed: \" + textStatus );\n\
				}\n\
			);\n\
	}\n\
";

	const Char_t* fexp_fixWinPath = "\
function fixWinPath(path)\n\
{\n\
	if (path.substring(1,3) == ':\\\\')\n\
	{\n\
		return path.replace(/\\\\/g,'\\\\\\\\');\n\
	}\n\
	else\n\
	{\n\
		return path;\n\
	}\n\
}\n";

	/**
	 * @param %l input uuid
	 * @param %d Length in Chars
	 */
	const Char_t* input_maxlen = "$(\"#%l\").attr('maxlength', %d);\n";

	/**
	 * @param %l input uuid
	 * @param %l input uuid
	 * @param %s widget type
	 */
	const Char_t* event_keypress = "\n\
	$('#%l').keypress(function(event)\n\
	{\n\
		mapParams['%l'] = '%s';\n\
	});\n";

	/**
	 * @param %l input uuid
	 * @param %l input uuid
	 * @param %s widget type
	 */
	const Char_t* event_click = "\n\
	$('#%l').click(function(event)\n\
	{\n\
		mapParams['%l'] = '%s';\n\
	});\n";

	/**
	 * @param %l input uuid
	 * @param %l input uuid
	 * @param %s widget type
	 */
	const Char_t* event_change = "\n\
	$('#%l').change(function(event)\n\
	{\n\
		mapParams['%l'] = '%s';\n\
	});\n";

	/**
	 * @param %l input uuid
	 * @param %l input uuid
	 * @param %s widget type
	 */
	const Char_t* event_focus = "\n\
	$('#%l').focus(function(event)\n\
	{\n\
		mapParams['%l'] = '%s';\n\
	});\n";

	const Char_t* arrayToJson = "\
	function arrayToJson(arr)\n\
	{\n\
		var Params = {};\n\
		for(var key in arr)\n\
		{\n\
			if ( arr[key] == \"GRID\" )\n\
			{\n\
				var ogrid = eval('o' + key);\n\
				Params[key] = getRowSelect(ogrid);\n\
			}\n\
			else if ( arr[key] == \"INPUT\" )\n\
			{\n\
				Params[key] = $('#' + key).val();\n\
			}\n\
			else if ( arr[key] == \"CHECKBOX\" )\n\
			{\n\
				Params[key] = $('#' + key).attr('checked') ? \"on\" : \"off\";\n\
			}\n\
			else if ( arr[key] == \"LISTBOX\" )\n\
			{\n\
				Params[key] = $('#' + key).val();\n\
			}\n\
			else if ( arr[key] == \"FILEEXPLORER\" )\n\
			{\n\
				Params[key] = $('#' + key).val();\n\
			}\n\
		}\n\
		return Params;\n\
	}\n\
";

	const Char_t* var_Map = "var mapParams = {};\n";
}



namespace ccss
{
	const Char_t* fexp_css ="\
@charset 'UTF-8';\n\
\n\
#fileLine {\n\
	color: rgb(0,0,0);\n\
	margin: 5px;\n\
	cursor: pointer;\n\
}\n\
\n\
#fileLine:hover {\n\
	background: rgb(176, 196, 222);\n\
}\n\
\n\
#fileExplorer{\n\
	font-family: Geneva, Arial, Helvetica, sans-serif;\n\
	position:absolute;\n\
	z-index: 100;\n\
	overflow: auto;\n\
	height: 200px;\n\
	width: 600px;\n\
	border: 1px solid rgb(0,0,0);\n\
	background-color: rgb(255,255,255);\n\
	padding: 8px;\n\
	top: 50px;\n\
	margin-left: auto;\n\
	margin-right: auto;\n\
	left:0;\n\
	right:0;\n\
	visibility:hidden;\n\
}\n\
";
}
    
}



