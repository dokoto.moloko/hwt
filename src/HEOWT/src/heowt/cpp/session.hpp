// SOURCE CHARSET ENCODING FOR WINDOWS   : US-ASCII
// SOURCE CHARSET ENCODING FOR LINUX, OS : ASCII

#ifndef _SESSION_HPP
#define _SESSION_HPP

#include "types.hpp" 
#include "document.hpp"
#include "header.hpp"
#include "webTags.hpp"
#include "deploycl.hpp"
#include "tools.hpp"

namespace hwt
{
	class session
	{
		private:
			COPY_CONSTRUCTOR PRIVATE DEFINITION session
				(
					const session& s
				) : WebLibraryPath(s.WebLibraryPath),		
					LogReportingMode(s.LogReportingMode),
					Port(s.Port),
					TimeOutSeg(s.TimeOutSeg),	
					Uuid(reinterpret_cast<uInt_t>(&s)),
					turnON(s.turnON),
                    AianaEncoding(s.AianaEncoding){}
		
			OPERATOR PRIVATE DEFINITION session& operator=
				(
					const session&
				) { return *this; }
        
            METHOD PRIVATE DECLARATION Str_t CloseAllWindows
                (
                    void
                ) const;

		public:

			FRIEND FUNCTION PUBLIC DECLARATION friend void Init
				(
					session& ss
				);

		public:			
			const Char_t* WebLibraryPath;
			mode_t LogReportingMode;			
			const uInt_t Port;
			const uInt_t TimeOutSeg;
			const uInt_t Uuid;
            bool turnON;
            Str_t AianaEncoding;
			header* Header;
            friend class line;
			

		private:
			sessionWidgets_t SessionWidgets;
			uInt_t main;

		public:
			CONSTRUCTOR PUBLIC DEFINITION session
				(
					const Char_t* weblibrarypath,
					mode_t logreportingmode,
                    const Char_t* aianaencoding = NULL,
					uInt_t port = 4444, 
					uInt_t timeoutseg = 10
				);

			DESTRUCTOR PUBLIC DECLARATION ~session
				(
					void
				);

		public: 		
			METHOD PUBLIC DECLARATION const documents_t& getconstDocuments
				(
					void
				) const;

			METHOD PUBLIC DECLARATION documents_t& getDocuments
				(
					void
				);

			// Se usa esta accion desde Session y no desde Document
			// porque cuando ponemos un documento al frente, automaticamente
			// se pone el resto detras.
			METHOD PUBLIC DECLARATION bool isDocOnTop
				(
					const uInt_t uuid
				) const;

			METHOD PUBLIC DECLARATION void setDocOnTopAndUnTopRest
				(
					const uInt_t uuid
				);

			METHOD PUBLIC DECLARATION void setDocOnTopAndUnTopRest
				(
					const Str_t& uniquename
				);

			METHOD PUBLIC DECLARATION void setDocUnTop
				(
					const uInt_t uuid
				);

			METHOD PUBLIC DECLARATION void setDocUnTop
				(
					const Str_t& uniquename
				);

			METHOD PUBLIC DECLARATION const document* getconstDocById
				(
					const uInt_t uuid
				) const;

			METHOD PUBLIC DECLARATION document* getDocById
				(
					const uInt_t uuid
				);

			METHOD PUBLIC DECLARATION const document* getconstDocByInd
				(
					const size_t index
				) const;

			METHOD PUBLIC DECLARATION document* getDocByInd
				(
					const size_t index
				) ;


			METHOD PUBLIC DECLARATION const document* getconstDocByName
				(
					const Str_t& uniquename
				) const;	
			
			METHOD PUBLIC DECLARATION document* getDocByName
				(
					const Str_t& uniquename
				);


			METHOD PUBLIC DECLARATION const widget* getconstWidgetByName
				(
					const Str_t& uniquename
				) const;

			METHOD PUBLIC DECLARATION widget* getWidgetByName
				(
					const Str_t& uniquename
				);

			METHOD PUBLIC DECLARATION const widget* getconstWidgetById
				(
					const uInt_t uuid
				) const;

			METHOD PUBLIC DECLARATION widget* getWidgetById
				(
					const uInt_t uuid
				);

			METHOD PUBLIC DECLARATION const sessionWidgets_t& getconstSessionWidgets
				(
					void
				) const;

			METHOD PUBLIC DECLARATION sessionWidgets_t& getSessionWidgets
				(
					void
				);

			METHOD PUBLIC DECLARATION uInt_t AddDocument
				(
					const Str_t& uniquename,
					const Str_t& title = "", 
					const Str_t& description = "", 
					const uInt_t width = 0,
					const uInt_t height = 0, 
					const bool visible = true
				);

			METHOD PUBLIC DECLARATION void DelDocument
				(
					const uInt_t uuid
				);

			METHOD PUBLIC DECLARATION void DelDocument
				(
					const Str_t& uniquename
				);

			METHOD PUBLIC DECLARATION  uInt_t getUuidOfMainDoc
				(
					void
				)const;

            METHOD PUBLIC DECLARATION  bool isTurnON
                (
                    void
                )const;
        
			METHOD PUBLIC DECLARATION const document* getconstMainDoc
				(
					void
				) const;

			METHOD PUBLIC DECLARATION document* getMainDoc
				(
					void
				);

			METHOD PUBLIC DECLARATION uInt_t NumOfDocuments
				(
					void
				) const;

			METHOD PUBLIC DECLARATION void DoDeploy
				(
					void
				);

			METHOD PUBLIC DECLARATION const Str_t toHtml
				(
					void
				) const;

			METHOD PUBLIC DECLARATION const Str_t toJavaScript
				(
					void
				) const;
	};
}

#endif //_SESSION_HPP

