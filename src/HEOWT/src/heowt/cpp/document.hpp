// SOURCE CHARSET ENCODING FOR WINDOWS   : US-ASCII
// SOURCE CHARSET ENCODING FOR LINUX, OS : ASCII

#ifndef _DOCUMENT_HPP 
#define _DOCUMENT_HPP

#include "types.hpp"
#include "widget.hpp"
#include "line.hpp"
#include "webTags.hpp"
#include "session.hpp"

namespace hwt
{
	class line;
	class document : public widget
	{
		private:
			COPY_CONSTRUCTOR PRIVATE DEFINITION document
				(
					const document& d
				) :
                    widget(	d.Father, d.UniqueName, d.Labeltx, d.Width, d.Height, DOCUMENT, d.Visible, d.Disable, d.AianaEncoding.c_str() ),
                    SessionWidgets(d.SessionWidgets),						
					onTop(d.onTop),
                    Description(d.Description),
                    Priority(d.Priority)                    
					{}

			OPERATOR PRIVATE DEFINITION document& operator=
				(
					const document&
				) { return *this; }

			METHOD PRIVATE DECLARATION Str_t wToHtml
				(
					widget* w, 
					const objets_t objType
				) const;

			METHOD PRIVATE DECLARATION void setOnTop
				(
					const bool ontop
				);

		private:
			static uInt_t count;
            std::vector<line*>  vLine;
            sessionWidgets_t* SessionWidgets;
			documentWidgets_t DocumentWidgets;
			bool onTop;
			std::vector<Str_t> alerts;
			
		public:
			Str_t Description;
			uInt_t Priority;
					
		public:			
			friend class session;

		public:
			CONSTRUCTOR PUBLIC DECLARATION document
				(
                    sessionWidgets_t* sessionwidgets,					
					const uInt_t father,		
					const Char_t* aianaencoding = NULL,
					const Str_t& uniquename = "",
					const Str_t& title = "",
					const Str_t& description = "", 					
					const uInt_t width = 0,
					const uInt_t height = 0, 
					const bool visible = true,
					const bool ontop = false,
					const bool disable = false                    
				);
			
			DESTRUCTOR PUBLIC DECLARATION ~document
				(
                    void
                );
        
			METHOD PUBLIC DECLARATION const std::vector<line*>& getconstAllLines
				( 
					void 
				)const;

			METHOD PUBLIC DECLARATION std::vector<line*>& getAllLines
				( 
					void 
				);

			METHOD PUBLIC DECLARATION const Str_t getconstTitle
				(
					void
				) const;

			METHOD PUBLIC DECLARATION const Str_t getconstUtf8Title
				(
					void
				) const;

			METHOD PUBLIC DECLARATION void setTitle
				(
					const Str_t title
				);


			METHOD PUBLIC DECLARATION bool isOnTop
				(
					void
				) const;


            METHOD PUBLIC DECLARATION size_t AddLine
                (
                    void
                );
        
            METHOD PUBLIC DECLARATION line* ModLine
                (
                    size_t index
                );
        
            METHOD PUBLIC DECLARATION const line* ModLine
                (
                    size_t index
                ) const;
        
            METHOD PUBLIC DECLARATION void DelLine
                (
                    size_t index
                );

			METHOD PUBLIC DECLARATION void AddAlert
				(
					const Str_t& text
				);

			METHOD PUBLIC DECLARATION const std::vector<Str_t>& getconstAlerts
				(
					void
				) const;

			METHOD PUBLIC DECLARATION void clearAlerts
				(
					void
				);

			METHOD PUBLIC DECLARATION Str_t toHtml
				(
					bool no_event = false
				) const;

			METHOD PUBLIC DECLARATION Str_t toHtmlWithDiv
				(
					void
				);


	};
}

#endif //_DOCUMENT_HPP

