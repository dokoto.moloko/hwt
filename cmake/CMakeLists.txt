#  cmake -DCMAKE_BUILD_TYPE=Debug ../../../.
#  cmake -DCMAKE_BUILD_TYPE=Release ../../../.
#  cmake -DCMAKE_BUILD_TYPE=Debug ../../../. -G Xcode
#  cmake -DCMAKE_BUILD_TYPE=Release ../../../. -G Xcode

SET(CMAKE_LEGACY_CYGWIN_WIN32 0)

project(HEOWT_PROC)
cmake_minimum_required( VERSION 2.6 FATAL_ERROR )

SET(TARGET "heowtProc")
 
MESSAGE( "SCAN DE SISTEMA" )
MESSAGE( "==========================================================================" )
MESSAGE( STATUS "SISTEMA ACTUAL              : "${CMAKE_SYSTEM_NAME} )
MESSAGE( STATUS "MODO                        : "${CMAKE_BUILD_TYPE} )
MESSAGE( STATUS "CMAKE_COMPILER_IS_GNUCXX    : "${CMAKE_COMPILER_IS_GNUCXX} )
MESSAGE( STATUS "UNIX                        : "${UNIX} )
MESSAGE( STATUS "WIN32                       : "${WIN32} )
MESSAGE( STATUS "APPLE                       : "${APPLE} )
MESSAGE( STATUS "MINGW                       : "${MINGW} )
MESSAGE( STATUS "MSYS                        : "${MSYS} )
MESSAGE( STATUS "CYGWIN                      : "${CYGWIN} )
MESSAGE( STATUS "BORLAND                     : "${BORLAND} )
MESSAGE( STATUS "WATCOM                      : "${WATCOM} )
MESSAGE( "==========================================================================" )

add_executable(${TARGET}
../src/HEOWT/src//heowt/cpp//button.hpp
../src/HEOWT/src//heowt/cpp/checkboxGroup.hpp
../src/HEOWT/src//heowt/cpp//checkbox.hpp
../src/HEOWT/src//heowt/cpp//createFileListXml.hpp
../src/HEOWT/src//heowt/cpp//deployapp.hpp
../src/HEOWT/src//heowt/cpp//deploycl.hpp
../src/HEOWT/src//heowt/cpp//document.hpp
../src/HEOWT/src//heowt/cpp//emsg.hpp
../src/HEOWT/src//heowt/cpp//fileexplorer.hpp
../src/HEOWT/src//heowt/cpp//grid.hpp
../src/HEOWT/src//heowt/cpp//header.hpp
../src/HEOWT/src//heowt/cpp//hwt.hpp
../src/HEOWT/src//heowt/cpp//input.hpp
../src/HEOWT/src//heowt/cpp/textArea.hpp
../src/HEOWT/src//heowt/cpp//label.hpp
../src/HEOWT/src//heowt/cpp//layout_defs.hpp
../src/HEOWT/src//heowt/cpp//line.hpp
../src/HEOWT/src//heowt/cpp//listbox.hpp
../src/HEOWT/src//heowt/cpp//pugiconfig.hpp
../src/HEOWT/src//heowt/cpp//pugixml.hpp
../src/HEOWT/src//heowt/cpp//session.hpp
../src/HEOWT/src//heowt/cpp//tools.hpp
../src/HEOWT/src//heowt/cpp//types.hpp
../src/HEOWT/src//heowt/cpp//utils.hpp
../src/HEOWT/src//heowt/cpp//webTags.hpp
../src/HEOWT/src//heowt/cpp//widget.hpp

../src/HEOWT/src//heowt/cpp//main.hpp
../src/HEOWT/src//heowt/cpp//test2.hpp
../src/HEOWT/src//heowt/cpp//ej1_agenda_tel.hpp

../src/HEOWT/src//heowt/cpp//button.cpp
../src/HEOWT/src//heowt/cpp/checkboxGroup.cpp
../src/HEOWT/src//heowt/cpp//checkbox.cpp
../src/HEOWT/src//heowt/cpp//createFileListXml.cpp
../src/HEOWT/src//heowt/cpp//deployapp.cpp
../src/HEOWT/src//heowt/cpp//deploycl.cpp
../src/HEOWT/src//heowt/cpp//document.cpp
../src/HEOWT/src//heowt/cpp//emsg.cpp
../src/HEOWT/src//heowt/cpp//fileexplorer.cpp
../src/HEOWT/src//heowt/cpp//grid.cpp
../src/HEOWT/src//heowt/cpp//header.cpp
../src/HEOWT/src//heowt/cpp//input.cpp
../src/HEOWT/src//heowt/cpp/textArea.cpp
../src/HEOWT/src//heowt/cpp//label.cpp
../src/HEOWT/src//heowt/cpp//line.cpp
../src/HEOWT/src//heowt/cpp//listbox.cpp

../src/HEOWT/src//heowt/cpp//pugixml.cpp
../src/HEOWT/src//heowt/cpp//session.cpp
../src/HEOWT/src//heowt/cpp//tools.cpp
../src/HEOWT/src//heowt/cpp//utils.cpp
../src/HEOWT/src//heowt/cpp//webTags.cpp
../src/HEOWT/src//heowt/cpp//widget.cpp

../src/HEOWT/src//heowt/cpp//main.cpp
../src/HEOWT/src//heowt/cpp//test2.cpp
../src/HEOWT/src//heowt/cpp//ej1_agenda_tel.cpp)
								
IF(${CMAKE_SYSTEM_NAME} MATCHES "Windows")
	MESSAGE( "SISTEMA: MSVC" )
	MESSAGE( "==========================================================================" )
	SET(CMAKE_CXX_FLAGS_DEBUG "/DWIN32 /D_WINDOWS /EHsc /WX /wd4355 /wd4251 /wd4250 /wd4996" CACHE STRING "Debug compiler flags" FORCE )   
	FIND_PATH(MICROHTTPD_LIBRARY_INCLUDE_DIRECTORIES microhttpd.h "D:/DATOS/PERSONALES/DESARROLLOS/C++/HWT/trunk/tests/HWT_PROTOTIPO_MSVC9/libs/win32/include")
	FIND_LIBRARY( MICROHTTPD_LIBRARY NAMES microhttpd libmicrohttpd PATHS" D:/DATOS/PERSONALES/DESARROLLOS/C++/HWT/trunk/tests/HWT_PROTOTIPO_MSVC9/libs/win32/lib")
	INCLUDE_DIRECTORIES ( ${MICROHTTPD_LIBRARY_INCLUDE_DIRECTORIES} )
	LINK_DIRECTORIES ( ${MICROHTTPD_LIBRARY} )            
	TARGET_LINK_LIBRARIES ( ${TARGET} ${MICROHTTPD_LIBRARY})
ELSE()	
  IF(${CMAKE_SYSTEM_NAME} MATCHES "Darwin")
	  MESSAGE( "SISTEMA: Apple OS X" )
	  MESSAGE( "==========================================================================" )	
	  IF(${CMAKE_BUILD_TYPE} MATCHES "Release")
		  MESSAGE( "Compilando en MODO RELEASE" )
		  add_definitions("-std=c++98 -DVERBOSE -DAPPLE -O3 -Wall -Wextra -pedantic -W -Wconversion -Wcast-qual -Wwrite-strings -Wstrict-aliasing  -Wpointer-arith -Wcast-align -fno-common") 
	  ELSE()
		  MESSAGE( "Compilando en MODO DEBUG" )
		  add_definitions("-std=c++98 -DVERBOSE -O0 -Wall -Wextra -pedantic -W -Wconversion -Wcast-qual -Wwrite-strings -Wstrict-aliasing  -Wpointer-arith -Wcast-align -fno-common") 
	  ENDIF()        

	  FIND_PATH(MICROHTTPD_LIBRARY_INCLUDE_DIRECTORIES microhttpd.h "/opt/local/include")
	  FIND_PATH(ICU_LIBRARY_INCLUDE_DIRECTORIES ucnv.h "/opt/local/include/unicode")
	  
	  FIND_LIBRARY( MICROHTTPD_LIBRARY NAMES microhttpd libmicrohttpd PATHS /opt/local/lib)
	  FIND_LIBRARY( ICU_LIBRARY NAMES icuuc libicuuc PATHS /opt/local/lib)


	  include_directories ( ${MICROHTTPD_LIBRARY_INCLUDE_DIRECTORIES} )
	  include_directories ( ${ICU_LIBRARY_INCLUDE_DIRECTORIES} )

	  link_directories ( ${MICROHTTPD_LIBRARY} )     
	  link_directories ( ${ICU_LIBRARY} )  

	  target_link_libraries ( ${TARGET} ${MICROHTTPD_LIBRARY} ${ICU_LIBRARY}) 

  ELSE()
	  IF(${CYGWIN})  
		  MESSAGE( "SISTEMA: Cygwin/Windows" )
		  MESSAGE( "==========================================================================" )
		  IF(${CMAKE_BUILD_TYPE} MATCHES "Release")
			  MESSAGE( "Compilando en MODO RELEASE" )
			  add_definitions("-std=c++98 -DVERBOSE -O3 -Wall -Wextra -pedantic -W -Wconversion -Wcast-qual -Wwrite-strings -Wstrict-aliasing  -Wpointer-arith -Wcast-align -fno-common") 
		  ELSE()
			  MESSAGE( "Compilando en MODO DEBUG" )
			  add_definitions("-std=c++98 -DVERBOSE -O0 -Wall -Wextra -pedantic -W -Wconversion -Wcast-qual -Wwrite-strings -Wstrict-aliasing  -Wpointer-arith -Wcast-align -fno-common") 
		  ENDIF()
		  target_link_libraries(${TARGET} /usr/local/lib/libmicrohttpd.dll.a )
	  ELSE()
		  IF(${CMAKE_SYSTEM_NAME} MATCHES "Linux")
			  MESSAGE( "SISTEMA: Linux" )
			  MESSAGE( "==========================================================================" )
			  IF(${CMAKE_BUILD_TYPE} MATCHES "Release")
				  MESSAGE( "Compilando en MODO RELEASE" )
				  add_definitions("-std=c++98 -DVERBOSE -O3 -Wall -Wextra -pedantic -W -Wconversion -Wcast-qual -Wwrite-strings -Wstrict-aliasing  -Wpointer-arith -Wcast-align -fno-common") 
			  ELSE()
				  MESSAGE( "Compilando en MODO DEBUG" )
				  add_definitions("-std=c++98 -DVERBOSE -O0 -Wall -Wextra -pedantic -W -Wconversion -Wcast-qual -Wwrite-strings -Wstrict-aliasing  -Wpointer-arith -Wcast-align -fno-common") 
			  ENDIF()                
			  FIND_PATH(MICROHTTPD_LIBRARY_INCLUDE_DIRECTORIES microhttpd.h "/usr/include")
			  FIND_PATH(ICU_LIBRARY_INCLUDE_DIRECTORIES ucnv.h "/usr/include/unicode")

			  FIND_LIBRARY( MICROHTTPD_LIBRARY NAMES microhttpd libmicrohttpd PATHS /usr/lib)
			  FIND_LIBRARY( ICU_LIBRARY NAMES icuuc libicuuc PATHS /usr/lib)

			  include_directories ( ${MICROHTTPD_LIBRARY_INCLUDE_DIRECTORIES} )
			  include_directories ( ${ICU_LIBRARY_INCLUDE_DIRECTORIES} )

			  link_directories ( ${MICROHTTPD_LIBRARY} )
			  link_directories ( ${ICU_LIBRARY} )            
            
			  target_link_libraries ( ${TARGET} ${MICROHTTPD_LIBRARY} ${ICU_LIBRARY})                
		  ELSE()
			  MESSAGE( STATUS "ATENCION ALGO VA MAL !!! -> NO SE HA DETECTADO EL SISTEMA : " ${CMAKE_SYSTEM_NAME})
			  MESSAGE( "==========================================================================" )
		  ENDIF()
	  ENDIF()        
  ENDIF()
ENDIF()
