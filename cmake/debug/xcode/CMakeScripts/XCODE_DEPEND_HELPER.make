# DO NOT EDIT
# This makefile makes sure all linkable targets are
# up-to-date with anything they link to
default:
	echo "Do not invoke directly"

# For each target create a dummy rule so the target does not have to exist
/opt/local/lib/libmicrohttpd.dylib:
/opt/local/lib/libicuuc.dylib:


# Rules to remove targets that are older than anything to which they
# link.  This forces Xcode to relink the targets from scratch.  It
# does not seem to check these dependencies itself.
PostBuild.heowtProc.Debug:
/Users/dokoto/Developments/CPP/hwt/cmake/debug/xcode/Debug/heowtProc:\
	/opt/local/lib/libmicrohttpd.dylib\
	/opt/local/lib/libicuuc.dylib
	/bin/rm -f /Users/dokoto/Developments/CPP/hwt/cmake/debug/xcode/Debug/heowtProc


PostBuild.heowtProc.Release:
/Users/dokoto/Developments/CPP/hwt/cmake/debug/xcode/Release/heowtProc:\
	/opt/local/lib/libmicrohttpd.dylib\
	/opt/local/lib/libicuuc.dylib
	/bin/rm -f /Users/dokoto/Developments/CPP/hwt/cmake/debug/xcode/Release/heowtProc


PostBuild.heowtProc.MinSizeRel:
/Users/dokoto/Developments/CPP/hwt/cmake/debug/xcode/MinSizeRel/heowtProc:\
	/opt/local/lib/libmicrohttpd.dylib\
	/opt/local/lib/libicuuc.dylib
	/bin/rm -f /Users/dokoto/Developments/CPP/hwt/cmake/debug/xcode/MinSizeRel/heowtProc


PostBuild.heowtProc.RelWithDebInfo:
/Users/dokoto/Developments/CPP/hwt/cmake/debug/xcode/RelWithDebInfo/heowtProc:\
	/opt/local/lib/libmicrohttpd.dylib\
	/opt/local/lib/libicuuc.dylib
	/bin/rm -f /Users/dokoto/Developments/CPP/hwt/cmake/debug/xcode/RelWithDebInfo/heowtProc


